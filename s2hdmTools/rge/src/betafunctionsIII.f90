module betafunctions

implicit none

integer, parameter :: dp = selected_real_kind(15,307)
real(dp), parameter :: pi = 4.E0_dp*atan(1.E0_dp)

! Define cutoff for landau poles
real(dp), parameter :: cutoff = 1.0E3_dp

public :: calc_betas, calc_betas_oneloop,  &
  calc_betas_twoloop

contains

subroutine calc_betas(y, t, yprime)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp), intent(in) :: y(18)
  real(dp), intent(in) :: t
  real(dp), intent(out) :: yprime(18)

  real(dp) :: yprimeOL(18), yprimeTL(18)

  !f2py intent(in) :: y
  !f2py intent(in) :: t
  !f2py intent(out) :: yprime

  ! One-loop beta functions
  call calc_betas_oneloop(y, t, yprimeOL)

  ! Two-loop beta funcions
  call calc_betas_twoloop(y, t, yprimeTL)

  yprime = yprimeOL + yprimeTL

end subroutine calc_betas


subroutine calc_betas_oneloop(y, t, yprime)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp), intent(in) :: y(18)
  real(dp), intent(in) :: t
  real(dp), intent(out) :: yprime(18)

  real(dp) :: yt, yb, g1, g2, g3
  real(dp) :: l1, l2, l3, l4, l5, l6, l7, l8
  real(dp) :: m11sq, m22sq, m12sq, mssq, mxsq

  integer :: i, landau

  !f2py intent(in) :: y
  !f2py intent(in) :: t
  !f2py intent(out) :: yprime

  landau = -1
  do i=1,13
    if (abs(y(i)).gt.cutoff) then
      landau = 1
    endif
  enddo

  if (landau.gt.0) then

    yprime = 0.0E0_dp

  else

    yt = y(1)
    yb = y(2)
    g1 = y(3)
    g2 = y(4)
    g3 = y(5)
    l1 = y(6)
    l2 = y(7)
    l3 = y(8)
    l4 = y(9)
    l5 = y(10)
    l6 = y(11)
    l7 = y(12)
    l8 = y(13)
    m11sq = y(14)
    m22sq = y(15)
    m12sq = y(16)
    mssq = y(17)
    mxsq = y(18)

    yprime(1) = (-17*g1**2*yt)/12.0E0_dp-(9*g2**2*yt)/4.0E0_dp-  &
      8*g3**2*yt+(3*yb**2*yt)/2.0E0_dp+(9*yt**3)/2.0E0_dp

    yprime(2) = (-5*g1**2*yb)/12.0E0_dp-(9*g2**2*yb)/4.0E0_dp-  &
      8*g3**2*yb+(9*yb**3)/2.0E0_dp+(3*yb*yt**2)/2.0E0_dp

    yprime(3) = 7*g1**3

    yprime(4) = -3*g2**3

    yprime(5) = -7*g3**3

    yprime(6) = (3*g1**4)/4.0E0_dp+(3*g1**2*g2**2)/2.0E0_dp+  &
      (9*g2**4)/4.0E0_dp-3*g1**2*l1-9*g2**2*l1+  &
      12*l1**2+4*l3**2+4*l3*l4+2*l4**2+2*l5**2  &
      +2*l7**2

    yprime(7) = (3*g1**4)/4.0E0_dp+(3*g1**2*g2**2)/2.0E0_dp+  &
      (9*g2**4)/4.0E0_dp-3*g1**2*l2-9*g2**2*l2+  &
      12*l2**2+4*l3**2+4*l3*l4+2*l4**2+2*l5**2  &
      +2*l8**2+12*l2*yb**2-12*yb**4+  &
      12*l2*yt**2-12*yt**4

    yprime(8) = (3*g1**4)/4.0E0_dp-(3*g1**2*g2**2)/2.0E0_dp+  &
      (9*g2**4)/4.0E0_dp-3*g1**2*l3-9*g2**2*l3+  &
      6*l1*l3+6*l2*l3+4*l3**2+2*l1*l4+2*l2*l4+  &
      2*l4**2+2*l5**2+2*l7*l8+6*l3*yb**2+  &
      6*l3*yt**2

    yprime(9) = 3*g1**2*g2**2-3*g1**2*l4-9*g2**2*l4+  &
      2*l1*l4+2*l2*l4+8*l3*l4+4*l4**2+8*l5**2+  &
      6*l4*yb**2+6*l4*yt**2

    yprime(10) = -3*g1**2*l5-9*g2**2*l5+2*l1*l5+2*l2*l5+  &
      8*l3*l5+12*l4*l5+6*l5*yb**2+6*l5*yt**2

    yprime(11) = 10*l6**2+4*(l7**2+l8**2)

    yprime(12) = (-3*g1**2*l7)/2.0E0_dp-(9*g2**2*l7)/2.0E0_dp+6*l1*l7  &
      +4*l6*l7+4*l7**2+4*l3*l8+2*l4*l8

    yprime(13) = 4*l3*l7+2*l4*l7-(3*g1**2*l8)/2.0E0_dp-  &
      (9*g2**2*l8)/2.0E0_dp+6*l2*l8+4*l6*l8+4*l8**2+  &
      6*l8*yb**2+6*l8*yt**2

    yprime(14) = (-3*g1**2*m11sq)/2.0E0_dp-(9*g2**2*m11sq)/2.0E0_dp+  &
      6*l1*m11sq+4*l3*m22sq+2*l4*m22sq+l7*mssq

    yprime(15) = 4*l3*m11sq+2*l4*m11sq-(3*g1**2*m22sq)/2.0E0_dp-  &
      (9*g2**2*m22sq)/2.0E0_dp+6*l2*m22sq+l8*mssq+  &
      6*m22sq*yb**2+6*m22sq*yt**2

    yprime(16) = (-3*g1**2*m12sq)/2.0E0_dp-(9*g2**2*m12sq)/2.0E0_dp+  &
      2*l3*m12sq+4*l4*m12sq+6*l5*m12sq+3*m12sq*yb**2+  &
      3*m12sq*yt**2

    yprime(17) = 8*l7*m11sq+8*l8*m22sq+4*l6*mssq

    yprime(18) = 2*l6*mxsq

    yprime = yprime / (16.0E0_dp * pi**2)

  endif

end subroutine calc_betas_oneloop


subroutine calc_betas_twoloop(y, t, yprime)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp), intent(in) :: y(18)
  real(dp), intent(in) :: t
  real(dp), intent(out) :: yprime(18)

  real(dp) :: yt, yb, g1, g2, g3
  real(dp) :: l1, l2, l3, l4, l5, l6, l7, l8
  real(dp) :: m11sq, m22sq, m12sq, mssq, mxsq

  integer :: i, landau

  !f2py intent(in) :: y
  !f2py intent(in) :: t
  !f2py intent(out) :: yprime

  landau = -1
  do i=1,13
    if (abs(y(i)).gt.cutoff) then
      landau = 1
    endif
  enddo

  if (landau.gt.0) then

    yprime = 0.0E0_dp

  else

    yt = y(1)
    yb = y(2)
    g1 = y(3)
    g2 = y(4)
    g3 = y(5)
    l1 = y(6)
    l2 = y(7)
    l3 = y(8)
    l4 = y(9)
    l5 = y(10)
    l6 = y(11)
    l7 = y(12)
    l8 = y(13)
    m11sq = y(14)
    m22sq = y(15)
    m12sq = y(16)
    mssq = y(17)
    mxsq = y(18)

    yprime(1) = (1267*g1**4*yt)/216.0E0_dp-  &
      (3*g1**2*g2**2*yt)/4.0E0_dp-(21*g2**4*yt)/4.0E0_dp+  &
      (19*g1**2*g3**2*yt)/9.0E0_dp+9*g2**2*g3**2*yt-  &
      108*g3**4*yt+(3*l2**2*yt)/2.0E0_dp+l3**2*yt+  &
      l3*l4*yt+l4**2*yt+(3*l5**2*yt)/2.0E0_dp+  &
      (l8**2*yt)/2.0E0_dp-(7*g1**2*yb**2*yt)/24.0E0_dp+  &
      (45*g2**2*yb**2*yt)/8.0E0_dp+28*g3**2*yb**2*yt  &
      +(((35*g1**2)/3.0E0_dp+45*g2**2-  &
      520*g3**2)*yb**2*yt)/24.0E0_dp-  &
      (7*((5*g1**2)/3.0E0_dp+45*g2**2+  &
      80*g3**2)*yb**2*yt)/240.0E0_dp-(yb**4*yt)/4.0E0_dp+  &
      (131*g1**2*yt**3)/16.0E0_dp+  &
      (225*g2**2*yt**3)/16.0E0_dp+36*g3**2*yt**3-  &
      6*l2*yt**3-(11*yb**2*yt**3)/4.0E0_dp-12*yt**5

    yprime(2) = (-113*g1**4*yb)/216.0E0_dp-  &
      (9*g1**2*g2**2*yb)/4.0E0_dp-(21*g2**4*yb)/4.0E0_dp+  &
      (31*g1**2*g3**2*yb)/9.0E0_dp+9*g2**2*g3**2*yb-  &
      108*g3**4*yb+(3*l2**2*yb)/2.0E0_dp+l3**2*yb+  &
      l3*l4*yb+l4**2*yb+(3*l5**2*yb)/2.0E0_dp+  &
      (l8**2*yb)/2.0E0_dp+(119*g1**2*yb**3)/72.0E0_dp+  &
      (45*g2**2*yb**3)/8.0E0_dp+(118*g3**2*yb**3)/3.0E0_dp  &
      +((473*g1**2)/144.0E0_dp+(135*g2**2)/16.0E0_dp-  &
      (10*g3**2)/3.0E0_dp-6*l2)*yb**3-12*yb**5+  &
      (53*g1**2*yb*yt**2)/24.0E0_dp+  &
      (45*g2**2*yb*yt**2)/8.0E0_dp+28*g3**2*yb*yt**2  &
      -(3*((5*g1**2)/3.0E0_dp-3*g2**2+  &
      128*g3**2)*yb*yt**2)/16.0E0_dp-  &
      (11*yb**3*yt**2)/4.0E0_dp-(yb*yt**4)/4.0E0_dp

    yprime(3) = (g1**3*((1040*g1**2)/3.0E0_dp+180*g2**2+  &
      440*g3**2-25*yb**2-85*yt**2))/30.0E0_dp

    yprime(4) = (g2**3*(20*g1**2+80*g2**2+120*g3**2-  &
      15*yb**2-15*yt**2))/10.0E0_dp

    yprime(5) = -(g3**3*((-55*g1**2)/3.0E0_dp-45*g2**2+  &
      260*g3**2+20*yb**2+20*yt**2))/10.0E0_dp

    yprime(6) = (-131*g1**6)/8.0E0_dp-(191*g1**4*g2**2)/8.0E0_dp-  &
      (101*g1**2*g2**4)/8.0E0_dp+(291*g2**6)/8.0E0_dp+  &
      (217*g1**4*l1)/8.0E0_dp+(39*g1**2*g2**2*l1)/4.0E0_dp  &
      -(51*g2**4*l1)/8.0E0_dp+18*g1**2*l1**2+  &
      54*g2**2*l1**2-78*l1**3+5*g1**4*l3+  &
      15*g2**4*l3+8*g1**2*l3**2+24*g2**2*l3**2  &
      -20*l1*l3**2-16*l3**3+(5*g1**4*l4)/2.0E0_dp+  &
      5*g1**2*g2**2*l4+(15*g2**4*l4)/2.0E0_dp+  &
      8*g1**2*l3*l4+24*g2**2*l3*l4-20*l1*l3*l4  &
      -24*l3**2*l4+4*g1**2*l4**2+6*g2**2*l4**2  &
      -12*l1*l4**2-32*l3*l4**2-12*l4**3-  &
      2*g1**2*l5**2-14*l1*l5**2-40*l3*l5**2-  &
      44*l4*l5**2-10*l1*l7**2-8*l7**3-  &
      12*(2*l3**2+2*l3*l4+l4**2+l5**2)*yb**2-  &
      24*l3**2*yt**2-24*l3*l4*yt**2-  &
      12*l4**2*yt**2-12*l5**2*yt**2

    yprime(7) = (-131*g1**6)/8.0E0_dp-(191*g1**4*g2**2)/8.0E0_dp-  &
      (101*g1**2*g2**4)/8.0E0_dp+(291*g2**6)/8.0E0_dp+  &
      (217*g1**4*l2)/8.0E0_dp+(39*g1**2*g2**2*l2)/4.0E0_dp  &
      -(51*g2**4*l2)/8.0E0_dp+18*g1**2*l2**2+  &
      54*g2**2*l2**2-78*l2**3+5*g1**4*l3+  &
      15*g2**4*l3+8*g1**2*l3**2+24*g2**2*l3**2  &
      -20*l2*l3**2-16*l3**3+(5*g1**4*l4)/2.0E0_dp+  &
      5*g1**2*g2**2*l4+(15*g2**4*l4)/2.0E0_dp+  &
      8*g1**2*l3*l4+24*g2**2*l3*l4-20*l2*l3*l4  &
      -24*l3**2*l4+4*g1**2*l4**2+6*g2**2*l4**2  &
      -12*l2*l4**2-32*l3*l4**2-12*l4**3-  &
      2*g1**2*l5**2-14*l2*l5**2-40*l3*l5**2-  &
      44*l4*l5**2-10*l2*l8**2-8*l8**3+  &
      (5*g1**4*yb**2)/2.0E0_dp+9*g1**2*g2**2*yb**2-  &
      (9*g2**4*yb**2)/2.0E0_dp+(((25*g1**2)/3.0E0_dp+  &
      45*g2**2+160*g3**2-144*l2)*l2*yb**2)/2.0E0_dp+  &
      (8*g1**2*yb**4)/3.0E0_dp-64*g3**2*yb**4-  &
      3*l2*yb**4+60*yb**6-(19*g1**4*yt**2)/2.0E0_dp+  &
      21*g1**2*g2**2*yt**2-(9*g2**4*yt**2)/2.0E0_dp+  &
      (85*g1**2*l2*yt**2)/6.0E0_dp+  &
      (45*g2**2*l2*yt**2)/2.0E0_dp+80*g3**2*l2*yt**2  &
      -72*l2**2*yt**2-42*l2*yb**2*yt**2-  &
      12*yb**4*yt**2-(16*g1**2*yt**4)/3.0E0_dp-  &
      64*g3**2*yt**4-3*l2*yt**4-12*yb**2*yt**4  &
      +60*yt**6

    yprime(8) = (-131*g1**6)/8.0E0_dp+(101*g1**4*g2**2)/8.0E0_dp+  &
      (11*g1**2*g2**4)/8.0E0_dp+(291*g2**6)/8.0E0_dp+  &
      (15*g1**4*l1)/4.0E0_dp-(5*g1**2*g2**2*l1)/2.0E0_dp+  &
      (45*g2**4*l1)/4.0E0_dp+(15*g1**4*l2)/4.0E0_dp-  &
      (5*g1**2*g2**2*l2)/2.0E0_dp+(45*g2**4*l2)/4.0E0_dp+  &
      (197*g1**4*l3)/8.0E0_dp+(11*g1**2*g2**2*l3)/4.0E0_dp  &
      -(111*g2**4*l3)/8.0E0_dp+12*g1**2*l1*l3+  &
      36*g2**2*l1*l3-15*l1**2*l3+  &
      12*g1**2*l2*l3+36*g2**2*l2*l3-  &
      15*l2**2*l3+2*g1**2*l3**2+6*g2**2*l3**2-  &
      36*l1*l3**2-36*l2*l3**2-12*l3**3+  &
      (5*g1**4*l4)/2.0E0_dp-3*g1**2*g2**2*l4+  &
      (15*g2**4*l4)/2.0E0_dp+4*g1**2*l1*l4+  &
      18*g2**2*l1*l4-4*l1**2*l4+4*g1**2*l2*l4+  &
      18*g2**2*l2*l4-4*l2**2*l4-12*g2**2*l3*l4  &
      -16*l1*l3*l4-16*l2*l3*l4-4*l3**2*l4-  &
      2*g1**2*l4**2+6*g2**2*l4**2-14*l1*l4**2-  &
      14*l2*l4**2-16*l3*l4**2-12*l4**3+  &
      4*g1**2*l5**2-18*l1*l5**2-18*l2*l5**2-  &
      18*l3*l5**2-44*l4*l5**2-l3*l7**2-  &
      8*l3*l7*l8-4*l7**2*l8-l3*l8**2-  &
      4*l7*l8**2+(5*g1**4*yb**2)/4.0E0_dp-  &
      (9*g1**2*g2**2*yb**2)/2.0E0_dp-  &
      (9*g2**4*yb**2)/4.0E0_dp+(((25*g1**2*l3)/3.0E0_dp+  &
      45*g2**2*l3+160*g3**2*l3-24*(2*l3**2+  &
      l4**2+2*l2*(3*l3+l4)+l5**2))*yb**2)/4.0E0_dp-  &
      (27*l3*yb**4)/2.0E0_dp-(19*g1**4*yt**2)/4.0E0_dp-  &
      (21*g1**2*g2**2*yt**2)/2.0E0_dp-  &
      (9*g2**4*yt**2)/4.0E0_dp+  &
      (85*g1**2*l3*yt**2)/12.0E0_dp+  &
      (45*g2**2*l3*yt**2)/4.0E0_dp+40*g3**2*l3*yt**2  &
      -36*l2*l3*yt**2-12*l3**2*yt**2-  &
      12*l2*l4*yt**2-6*l4**2*yt**2-  &
      6*l5**2*yt**2-21*l3*yb**2*yt**2-  &
      24*l4*yb**2*yt**2-(27*l3*yt**4)/2.0E0_dp

    yprime(9) = (-73*g1**4*g2**2)/2.0E0_dp-14*g1**2*g2**4+  &
      5*g1**2*g2**2*l1+5*g1**2*g2**2*l2+  &
      2*g1**2*g2**2*l3+(157*g1**4*l4)/8.0E0_dp+  &
      (51*g1**2*g2**2*l4)/4.0E0_dp-(231*g2**4*l4)/8.0E0_dp  &
      +4*g1**2*l1*l4-7*l1**2*l4+4*g1**2*l2*l4-  &
      7*l2**2*l4+4*g1**2*l3*l4+36*g2**2*l3*l4-  &
      40*l1*l3*l4-40*l2*l3*l4-28*l3**2*l4+  &
      8*g1**2*l4**2+18*g2**2*l4**2-20*l1*l4**2  &
      -20*l2*l4**2-28*l3*l4**2+16*g1**2*l5**2+  &
      54*g2**2*l5**2-24*l1*l5**2-24*l2*l5**2-  &
      48*l3*l5**2-26*l4*l5**2-l4*l7**2-  &
      8*l4*l7*l8-l4*l8**2+9*g1**2*g2**2*yb**2+  &
      (((25*g1**2*l4)/3.0E0_dp+45*g2**2*l4+  &
      160*g3**2*l4-48*(l2*l4+2*l3*l4+l4**2+  &
      2*l5**2))*yb**2)/4.0E0_dp-(27*l4*yb**4)/2.0E0_dp+  &
      21*g1**2*g2**2*yt**2+  &
      (85*g1**2*l4*yt**2)/12.0E0_dp+  &
      (45*g2**2*l4*yt**2)/4.0E0_dp+40*g3**2*l4*yt**2  &
      -12*l2*l4*yt**2-24*l3*l4*yt**2-  &
      12*l4**2*yt**2-24*l5**2*yt**2+  &
      27*l4*yb**2*yt**2-(27*l4*yt**4)/2.0E0_dp

    yprime(10) = (157*g1**4*l5)/8.0E0_dp+(19*g1**2*g2**2*l5)/4.0E0_dp  &
      -(231*g2**4*l5)/8.0E0_dp-2*g1**2*l1*l5-  &
      7*l1**2*l5-2*g1**2*l2*l5-7*l2**2*l5+  &
      16*g1**2*l3*l5+36*g2**2*l3*l5-  &
      40*l1*l3*l5-40*l2*l3*l5-28*l3**2*l5+  &
      24*g1**2*l4*l5+72*g2**2*l4*l5-  &
      44*l1*l4*l5-44*l2*l4*l5-76*l3*l4*l5-  &
      32*l4**2*l5+6*l5**3-l5*l7**2-8*l5*l7*l8-  &
      l5*l8**2+(((25*g1**2)/3.0E0_dp+45*g2**2+  &
      160*g3**2-48*(l2+2*l3+  &
      3*l4))*l5*yb**2)/4.0E0_dp-(3*l5*yb**4)/2.0E0_dp+  &
      (85*g1**2*l5*yt**2)/12.0E0_dp+  &
      (45*g2**2*l5*yt**2)/4.0E0_dp+40*g3**2*l5*yt**2  &
      -12*l2*l5*yt**2-24*l3*l5*yt**2-  &
      36*l4*l5*yt**2+3*l5*yb**2*yt**2-  &
      (3*l5*yt**4)/2.0E0_dp

    yprime(11) = (-4*(75*l6**3-10*g1**2*l7**2-  &
      30*g2**2*l7**2+25*l6*l7**2+20*l7**3-  &
      10*g1**2*l8**2-30*g2**2*l8**2+  &
      25*l6*l8**2+20*l8**3+30*l8**2*yb**2+  &
      30*l8**2*yt**2))/5.0E0_dp

    yprime(12) = (193*g1**4*l7)/16.0E0_dp+  &
      (15*g1**2*g2**2*l7)/8.0E0_dp-  &
      (123*g2**4*l7)/16.0E0_dp+12*g1**2*l1*l7+  &
      36*g2**2*l1*l7-15*l1**2*l7-2*l3**2*l7-  &
      2*l3*l4*l7-2*l4**2*l7-3*l5**2*l7-  &
      10*l6**2*l7+g1**2*l7**2+3*g2**2*l7**2-  &
      36*l1*l7**2-24*l6*l7**2-11*l7**3+  &
      (5*g1**4*l8)/2.0E0_dp+(15*g2**4*l8)/2.0E0_dp+  &
      8*g1**2*l3*l8+24*g2**2*l3*l8-8*l3**2*l8+  &
      4*g1**2*l4*l8+12*g2**2*l4*l8-8*l3*l4*l8-  &
      8*l4**2*l8-12*l5**2*l8-16*l3*l7*l8-  &
      8*l4*l7*l8-8*l3*l8**2-4*l4*l8**2-  &
      2*l7*l8**2-12*(2*l3+l4)*l8*yb**2-  &
      24*l3*l8*yt**2-12*l4*l8*yt**2

    yprime(13) = (5*g1**4*l7)/2.0E0_dp+(15*g2**4*l7)/2.0E0_dp+  &
      8*g1**2*l3*l7+24*g2**2*l3*l7-8*l3**2*l7+  &
      4*g1**2*l4*l7+12*g2**2*l4*l7-8*l3*l4*l7-  &
      8*l4**2*l7-12*l5**2*l7-8*l3*l7**2-  &
      4*l4*l7**2+(193*g1**4*l8)/16.0E0_dp+  &
      (15*g1**2*g2**2*l8)/8.0E0_dp-  &
      (123*g2**4*l8)/16.0E0_dp+12*g1**2*l2*l8+  &
      36*g2**2*l2*l8-15*l2**2*l8-2*l3**2*l8-  &
      2*l3*l4*l8-2*l4**2*l8-3*l5**2*l8-  &
      10*l6**2*l8-16*l3*l7*l8-8*l4*l7*l8-  &
      2*l7**2*l8+g1**2*l8**2+3*g2**2*l8**2-  &
      36*l2*l8**2-24*l6*l8**2-11*l8**3+  &
      (((25*g1**2)/3.0E0_dp+45*g2**2+160*g3**2-  &
      144*l2-48*l8)*l8*yb**2)/4.0E0_dp-  &
      (27*l8*yb**4)/2.0E0_dp+(85*g1**2*l8*yt**2)/12.0E0_dp  &
      +(45*g2**2*l8*yt**2)/4.0E0_dp+  &
      40*g3**2*l8*yt**2-36*l2*l8*yt**2-  &
      12*l8**2*yt**2-21*l8*yb**2*yt**2-  &
      (27*l8*yt**4)/2.0E0_dp

    yprime(14) = (193*g1**4*m11sq)/16.0E0_dp+  &
      (15*g1**2*g2**2*m11sq)/8.0E0_dp-  &
      (123*g2**4*m11sq)/16.0E0_dp+12*g1**2*l1*m11sq+  &
      36*g2**2*l1*m11sq-15*l1**2*m11sq-  &
      2*l3**2*m11sq-2*l3*l4*m11sq-2*l4**2*m11sq-  &
      3*l5**2*m11sq-l7**2*m11sq+  &
      (5*g1**4*m22sq)/2.0E0_dp+(15*g2**4*m22sq)/2.0E0_dp+  &
      8*g1**2*l3*m22sq+24*g2**2*l3*m22sq-  &
      8*l3**2*m22sq+4*g1**2*l4*m22sq+  &
      12*g2**2*l4*m22sq-8*l3*l4*m22sq-  &
      8*l4**2*m22sq-12*l5**2*m22sq-2*l7**2*mssq-  &
      12*(2*l3+l4)*m22sq*yb**2-24*l3*m22sq*yt**2  &
      -12*l4*m22sq*yt**2

    yprime(15) = (5*g1**4*m11sq)/2.0E0_dp+(15*g2**4*m11sq)/2.0E0_dp+  &
      8*g1**2*l3*m11sq+24*g2**2*l3*m11sq-  &
      8*l3**2*m11sq+4*g1**2*l4*m11sq+  &
      12*g2**2*l4*m11sq-8*l3*l4*m11sq-  &
      8*l4**2*m11sq-12*l5**2*m11sq+  &
      (193*g1**4*m22sq)/16.0E0_dp+  &
      (15*g1**2*g2**2*m22sq)/8.0E0_dp-  &
      (123*g2**4*m22sq)/16.0E0_dp+12*g1**2*l2*m22sq+  &
      36*g2**2*l2*m22sq-15*l2**2*m22sq-  &
      2*l3**2*m22sq-2*l3*l4*m22sq-2*l4**2*m22sq-  &
      3*l5**2*m22sq-l8**2*m22sq-2*l8**2*mssq+  &
      (((25*g1**2)/3.0E0_dp+45*g2**2+160*g3**2-  &
      144*l2)*m22sq*yb**2)/4.0E0_dp-  &
      (27*m22sq*yb**4)/2.0E0_dp+  &
      (85*g1**2*m22sq*yt**2)/12.0E0_dp+  &
      (45*g2**2*m22sq*yt**2)/4.0E0_dp+  &
      40*g3**2*m22sq*yt**2-36*l2*m22sq*yt**2-  &
      21*m22sq*yb**2*yt**2-(27*m22sq*yt**4)/2.0E0_dp

    yprime(16) = (153*g1**4*m12sq)/16.0E0_dp+  &
      (15*g1**2*g2**2*m12sq)/8.0E0_dp-  &
      (243*g2**4*m12sq)/16.0E0_dp+(3*l1**2*m12sq)/2.0E0_dp+  &
      (3*l2**2*m12sq)/2.0E0_dp+4*g1**2*l3*m12sq+  &
      12*g2**2*l3*m12sq-6*l1*l3*m12sq-6*l2*l3*m12sq+  &
      8*g1**2*l4*m12sq+24*g2**2*l4*m12sq-  &
      6*l1*l4*m12sq-6*l2*l4*m12sq-6*l3*l4*m12sq+  &
      12*g1**2*l5*m12sq+36*g2**2*l5*m12sq-  &
      6*l1*l5*m12sq-6*l2*l5*m12sq-12*l3*l5*m12sq-  &
      12*l4*l5*m12sq+3*l5**2*m12sq+(l7**2*m12sq)/2.0E0_dp-  &
      2*l7*l8*m12sq+(l8**2*m12sq)/2.0E0_dp+  &
      (((25*g1**2)/3.0E0_dp+45*g2**2+160*g3**2-  &
      48*(l3+2*l4+3*l5))*m12sq*yb**2)/8.0E0_dp-  &
      (27*m12sq*yb**4)/4.0E0_dp+  &
      (85*g1**2*m12sq*yt**2)/24.0E0_dp+  &
      (45*g2**2*m12sq*yt**2)/8.0E0_dp+  &
      20*g3**2*m12sq*yt**2-6*l3*m12sq*yt**2-  &
      12*l4*m12sq*yt**2-18*l5*m12sq*yt**2+  &
      (3*m12sq*yb**2*yt**2)/2.0E0_dp-(27*m12sq*yt**4)/4.0E0_dp

    yprime(17) = 16*g1**2*l7*m11sq+48*g2**2*l7*m11sq-  &
      16*l7**2*m11sq+16*g1**2*l8*m22sq+  &
      48*g2**2*l8*m22sq-16*l8**2*m22sq-  &
      10*l6**2*mssq-2*l7**2*mssq-2*l8**2*mssq-  &
      48*l8*m22sq*yb**2-48*l8*m22sq*yt**2

    yprime(18) = -2*(3*l6**2+l7**2+l8**2)*mxsq

    yprime = yprime / (16.0E0_dp**2 * pi**4)

  endif

end subroutine calc_betas_twoloop


end module betafunctions
