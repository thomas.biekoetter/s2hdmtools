import numpy as np


def calc_mw(mz, Gf, alew):

    mw = np.sqrt(
        mz**2 / 2. + np.sqrt(
            mz**4 / 4. - np.pi / np.sqrt(2.) * alew / Gf * mz**2))

    return mw


def get_paras_mass_ordered(p):

    if p['mH1'] <= p['mH2'] <= p['mH3']:
        res = {key: value for key, value in p.items()}
        return res

    if p['mH2'] <= p['mH1'] <= p['mH3']:
        order = [2, 1, 3]
    elif p['mH1'] <= p['mH3'] <= p['mH2']:
        order = [1, 3, 2]
    elif p['mH3'] <= p['mH1'] <= p['mH2']:
        order = [3, 1, 2]
    elif p['mH2'] <= p['mH3'] <= p['mH1']:
        order = [2, 3, 1]
    elif p['mH3'] <= p['mH2'] <= p['mH1']:
        order = [3, 2, 1]
    else:
        raise NotImplementedError(
            'Permutation of masses not implemented.')

    masses = [p['mH1'], p['mH2'], p['mH3']]
    mHa = masses[order[0] - 1]
    mHb = masses[order[1] - 1]
    mHc = masses[order[2] - 1]

    R = np.zeros(shape=(3, 3))
    S = np.zeros(shape=(3, 3))
    T = np.zeros(shape=(3, 3))

    c = np.cos
    s = np.sin

    R[0, 0] = c(p['al1']) * c(p['al2'])
    R[1, 0] = -(c(p['al1']) * s(p['al2']) * s(p['al3']) +
        s(p['al1']) * c(p['al3']))
    R[2, 0] = -c(p['al1']) * s(p['al2']) * c(p['al3']) + \
        s(p['al1']) * s(p['al3'])
    R[0, 1] = s(p['al1']) * c(p['al2'])
    R[1, 1] = c(p['al1']) * c(p['al3']) - \
        s(p['al1']) * s(p['al2']) * s(p['al3'])
    R[2, 1] = -(c(p['al1']) * s(p['al3']) +
        s(p['al1']) * s(p['al2']) * c(p['al3']))
    R[0, 2] = s(p['al2'])
    R[1, 2] = c(p['al2']) * s(p['al3'])
    R[2, 2] = c(p['al2']) * c(p['al3'])

    found = False

    for sgn in [1., -1.]:

        for x, i in enumerate(order):
            if i == 1:
                S[x, ...] = sgn * R[0, ...]
            elif i == 2:
                S[x, ...] = sgn * R[1, ...]
            elif i == 3:
                S[x, ...] = sgn * R[2, ...]

        be2 = np.arcsin(S[0, 2])
        be3 = np.arccos(S[2, 2] / c(be2))
        be1 = np.arccos(S[0, 0] / c(be2))

        signpos = [
            [1, 1, 1],
            [-1, 1, 1],
            [1, -1, 1],
            [1, 1, -1],
            [-1, -1, 1],
            [-1, 1, -1],
            [1, -1, -1],
            [-1, -1, -1]]

        for pos in signpos:
            be1c = pos[0] * be1
            be2c = pos[1] * be2
            be3c = pos[2] * be3
            T[0, 0] = c(be1c) * c(be2c)
            T[1, 0] = -(c(be1c) * s(be2c) * s(be3c) +
               s(be1c) * c(be3c))
            T[2, 0] = -c(be1c) * s(be2c) * c(be3c) + \
               s(be1c) * s(be3c)
            T[0, 1] = s(be1c) * c(be2c)
            T[1, 1] = c(be1c) * c(be3c) - \
               s(be1c) * s(be2c) * s(be3c)
            T[2, 1] = -(c(be1c) * s(be3c) +
               s(be1c) * s(be2c) * c(be3c))
            T[0, 2] = s(be2c)
            T[1, 2] = c(be2c) * s(be3c)
            T[2, 2] = c(be2c) * c(be3c)
            if np.allclose(S, T, atol=1e-6):
                be1 = be1c
                be2 = be2c
                be3 = be3c
                found = True

    if not found:
        raise RuntimeError(
            'Could not find correct signs of mixing angles.')

    res = {key: value for key, value in p.items()}
    res['mH1'] = mHa
    res['mH2'] = mHb
    res['mH3'] = mHc
    res['al1'] = be1
    res['al2'] = be2
    res['al3'] = be3

    return res
