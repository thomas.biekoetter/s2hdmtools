import random
import numpy as np


def get_point_in_limits(dc):
    """
    Takes a dict as input, with keys
    as the input keys of ParamPoint,
    and the values are tuples with
    lower and upper limits. If a
    parameter has a fixed value, it
    can be given as float instead
    of tuple.

    Returns an input dict to initialize
    an instance of ParamPoint with parameter
    values in betweem the lower and upper
    limits of each parameter.
    """

    if 'lam1' in dc:
        raise NotImplementedError(
            'Lambda input not yet supported.')

    dd = {}

    for key in dc:

        try:

            dd[key] = random.uniform(*dc[key])

        except TypeError:

            dd[key] = dc[key]

    return dd
