from s2hdmTools.util import calc_mw


vSM = 246.221
mt = 172.0
mb = 4.18
ml = 1.777
mZ = 91.1876
Gf = 1.16637e-5
alew = 1. / 127.9
als = 0.1184
mW = calc_mw(mZ, Gf, alew)

# Conversion factor cm^3 / s to pb
# From: 1e-26cm**3/s = 0.8566e-9/GeV**2
#       1pb = 2.568e-9/GeV**2
conv_cm3ps_pb = 3.33564e25

Omegah2_Planck = 1.2e-1
Omegah2_Planck_Err = 1.2e-3
