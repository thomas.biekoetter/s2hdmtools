import numpy as np


def boundedness(
        lam1, lam2, lam3, lam4,
        lam5, lam6, lam7, lam8):

    BfB = True
    BfB1 = True

    D = min(lam4 - np.abs(lam5),0)
    cond = np.zeros(shape=(7, ))

    cond[0] = lam1
    cond[1] = lam2
    cond[2] = lam6

    for x in cond[0:3]:
        if x <= 0:
            BfB = False
            return BfB

    cond[3] = np.sqrt(lam1*lam6) + lam7
    cond[4] = np.sqrt(lam2*lam6) + lam8
    cond[5] = np.sqrt(lam1*lam2) + lam3 + D

    for x in cond[3:6]:
        if x <= 0:
            BfB1 = False

    cond[6] = lam7 + np.sqrt(lam1/lam2)*lam8

    if cond[6] < 0:
        BfB1 = False

    if BfB1 == True:

        return BfB

    else:

        if not (np.sqrt(lam2 * lam6) >= lam8 >
            -np.sqrt(lam2 * lam6)):

            BfB = False
            return BfB

        if not (np.sqrt(lam1 * lam6) > -lam7 >=
            np.sqrt((lam1 / lam2)) * lam8):

            BfB = False
            return BfB

        if not (np.sqrt((lam7**2-lam1*lam6)*(lam8**2-lam2*lam6)) >
            (lam7*lam8-(D+lam3)*lam6)):

            BfB = False
            return BfB

        else:

            BfB = True
            return BfB


_vec_boundedness = np.vectorize(boundedness)


def boundedness_to_scale(
        pt, lamsAtScale, scales):

    boundis = _vec_boundedness(
        lamsAtScale[..., 0],
        lamsAtScale[..., 1],
        lamsAtScale[..., 2],
        lamsAtScale[..., 3],
        lamsAtScale[..., 4],
        lamsAtScale[..., 5],
        lamsAtScale[..., 6],
        lamsAtScale[..., 7])

    for i, bd in enumerate(boundis):

        if not bd:

            cutoffP = scales[i]
            IsNonBounded = True
            iP = i
            break
    else:

            IsNonBounded = False

    dc = {'IsNonBounded': IsNonBounded}

    if IsNonBounded:

        dc['Bounded_scale'] = cutoffP
        dc['Bounded_index'] = iP

    return dc

