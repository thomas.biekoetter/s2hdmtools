import dds2hdm.interface


def calc_nucleon_scattering(pt):
    dc = {}
    dd = dds2hdm.interface.S2hdmTools(
        pt, nucleon_type="proton")
    xs = dd.get_cross_section()
    dc['sigma_proton'] = xs
    dc['amplitude_up'] = dd.Fuu
    dc['amplitude_down'] = dd.Fdd
    dd = dds2hdm.interface.S2hdmTools(
        pt, nucleon_type="neutron")
    xs = dd.get_cross_section()
    dc['sigma_neutron'] = xs
    pt.directDetection = dc
