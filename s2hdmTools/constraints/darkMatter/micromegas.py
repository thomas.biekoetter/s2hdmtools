import os
import warnings
import numpy as np
from decimal import Decimal
from random import randint, uniform
import time

from s2hdmTools.constraints.darkMatter.microPATH \
    import MO_EXEC_DIR_1, MO_EXEC_DIR_2, \
        MO_EXEC_DIR_3, MO_EXEC_DIR_4
from s2hdmTools.constraints.darkMatter.microPATH \
    import MO_EXEC_DIR_UNI_1, MO_EXEC_DIR_UNI_2, \
        MO_EXEC_DIR_UNI_3, MO_EXEC_DIR_UNI_4


BRCUT = 1.0e-6

# Values from Feynrules model file

CABI = 2.27736e-1

AEWM1 = 1.279e2
GF = 1.16637e-5
AS = 1.184e-1

YMDO = 5.04e-3
YMUP = 2.55e-3
YMS = 1.01e-1
YMC = 1.27
YMB = 4.7
YMT = 1.72e2
YME = 5.11e-4
YMM = 1.0566e-1
YMTAU = 1.777

MD = 5.03e-3
MU = 2.55e-3
MS = 1.01e-1
MC = 1.27
MB = 4.7
MT = 1.72e2
ME = 5.11e-4
MNUE = 0.0
MMU = 1.0566e-1
MNUM = 0.0
MTA = 1.777
MNUT = 0.0
MZ = 9.11876e1
MW = 7.9824359746e1

WZ = 2.4952
WW = 2.085


class Micromegas:

    def __init__(self, pt, gauge='unitary'):

        if not gauge in ['feynman', 'unitary']:
            raise ValueError(
                "Possible gauges: 'feynman', 'unitary'. " +
                "You chose: '" + str(gauge) + "'.")

        try:
            pt.b_H1
        except AttributeError:
            pt.calculate_branching_ratios()

        self.pt = pt

        if gauge=='feynman':
            if pt.yuktype == 1:
                self.exec_dir = MO_EXEC_DIR_1
            elif pt.yuktype == 2:
                self.exec_dir = MO_EXEC_DIR_2
            elif pt.yuktype == 3:
                self.exec_dir = MO_EXEC_DIR_3
            elif pt.yuktype == 4:
                self.exec_dir = MO_EXEC_DIR_4
        else:
            if pt.yuktype == 1:
                self.exec_dir = MO_EXEC_DIR_UNI_1
            elif pt.yuktype == 2:
                self.exec_dir = MO_EXEC_DIR_UNI_2
            elif pt.yuktype == 3:
                self.exec_dir = MO_EXEC_DIR_UNI_3
            elif pt.yuktype == 4:
                self.exec_dir = MO_EXEC_DIR_UNI_4

        hs = str(randint(1000000, 9999999))

        self.IN_NAME = 'vars1.lha'
        self.VER_NAME = hs + 'micro.log'

    def execute(self):

        self.create_inputfile()
        self.run()
        self.read_outputfile()

        dc = {}
        dc['indirect'] = self.indirect
        dc['monoject'] = self.monojet
        dc['relic'] = self.relic
        self.pt.Micromegas = dc

    def create_inputfile(self):

        pt = self.pt

        with open(self.IN_NAME, 'w') as f:

            def fw(x): f.write(x + '\n')

            def nstr(x):
                y = '{0:.10E}'.format(Decimal(str(x)))
                if y[-3] == 'E':
                    y = y.replace('E+', 'E+0')
                    y = y.replace('E-', 'E-0')
                if y == '0.0000000000E+09':
                    y = '0.0000000000E+00'
                return y

            fw(
                '# This file has been generated' +
                ' by s2hdmTools')

            fw('Block CKMBLOCK')
            fw('  1  ' + nstr(CABI) + '  # cabi')

            fw('BLOCK S2HDMBLOCK')
            fw('  1  ' + nstr(pt.vs) + '  # vS')
            fw('  2  ' + nstr(pt.al1) + '  # al1')
            fw('  3  ' + nstr(pt.al2) + '  # al2')
            fw('  4  ' + nstr(pt.al3) + '  # al3')
            fw('  5  ' + nstr(pt.tb) + '  # TB')
            fw('  6  ' + nstr(pt.m12sq) + '  # m12sq')

            fw('BLOCK SMINPUTS')
            fw('  1  ' + nstr(AEWM1) + '  # aEWM1')
            fw('  2  ' + nstr(GF) + '  # Gf')
            fw('  3  ' + nstr(AS) + '  # aS')

            fw('Block YUKAWA')
            fw('  1  ' + nstr(YMDO) + '  # ymdo')
            fw('  2  ' + nstr(YMUP) + '  # ymup')
            fw('  3  ' + nstr(YMS) + '  # yms')
            fw('  4  ' + nstr(YMC) + '  # ymc')
            fw('  5  ' + nstr(YMB) + '  # ymb')
            fw('  6  ' + nstr(YMT) + '  # ymt')
            fw('  11  ' + nstr(YME) + '  # yme')
            fw('  13  ' + nstr(YMM) + '  # ymm')
            fw('  15  ' + nstr(YMTAU) + '  # ymtau')

            fw('Block MASS')
            fw('  1  ' + nstr(MD) + '  # MD')
            fw('  2  ' + nstr(MU) + '  # MU')
            fw('  3  ' + nstr(MS) + '  # MS')
            fw('  4  ' + nstr(MC) + '  # MC')
            fw('  5  ' + nstr(MB) + '  # MB')
            fw('  6  ' + nstr(MT) + '  # MT')
            fw('  11  ' + nstr(ME) + '  # Me')
            fw('  12  ' + nstr(MNUE) + '  # Mnue')
            fw('  13  ' + nstr(MMU) + '  # MMU')
            fw('  14  ' + nstr(MNUM) + '  # Mnum')
            fw('  15  ' + nstr(MTA) + '  # MTA')
            fw('  16  ' + nstr(MNUT) + '  # Mnut')
            fw('  23  ' + nstr(MZ) + '  # MZ')
            fw('  24  ' + nstr(MW) + '  # MW')
            mH1 = pt.mH1
            mH2 = pt.mH2
            mH3 = pt.mH3
            if abs(mH2 - mH1) < 1e-2:
                mH2 += 1e-2
            if abs(mH3 - mH2) < 1e-2:
                mH3 += 1e-2
            fw('  25  ' + nstr(mH1) + '  # Mh1')
            fw('  29  ' + nstr(mH2) + '  # Mh2')
            fw('  30  ' + nstr(pt.mXi) + '  # MXi')
            fw('  36  ' + nstr(mH3) + '  # Mh3')
            fw('  37  ' + nstr(pt.mHp) + '  # MHp')
            fw('  38  ' + nstr(pt.mA) + '  # MA0')

            fw('DECAY  6  ' + nstr(pt.wTot['t']) + '  # WT')
            fw('DECAY  23  ' + nstr(WZ) + '  # WZ')
            fw('DECAY  24  ' + nstr(WW) + '  # WW')
            fw('DECAY  25  ' + nstr(pt.wTot['H1']) + '  # Wh1')
            fw('DECAY  29  ' + nstr(pt.wTot['H2']) + '  # Wh2')
            fw('DECAY  36  ' + nstr(pt.wTot['H3']) + '  # Wh3')
            fw('DECAY  37  ' + nstr(pt.wTot['Hp']) + '  # WHp')
            fw('DECAY  38  ' + nstr(pt.wTot['A']) + '  # WA0')

    def run(self):

        time.sleep(uniform(0.1, 0.3))

        CUR_DIR = os.getcwd() + '/'
        exec_dir = self.exec_dir + '/'

        # Wait if currently running
        if os.path.exists(exec_dir + self.IN_NAME):
            time.sleep(uniform(4.0, 10.0))

        os.rename(
            CUR_DIR + self.IN_NAME,
            exec_dir + self.IN_NAME)

        os.chdir(exec_dir)

        os.system(
            './main ' + self.IN_NAME + \
                ' > ' + self.VER_NAME)

        os.rename(
            exec_dir + self.VER_NAME,
            CUR_DIR + self.VER_NAME)

        os.remove(self.IN_NAME)

        os.chdir(CUR_DIR)

    def read_outputfile(self):

        pt = self.pt

        with open(self.VER_NAME, 'r') as f:
            lns = f.readlines()

        # Remove CALCHEP basura
        for i, ln in enumerate(lns):
            if 'Error return of slhaRead' in ln:
                start = i

        try:
            lns = lns[start:]
        except UnboundLocalError:
            slhaError()

        if int(lns[0].split()[-1]) != 0: slhaError()

        mXi = float(lns[3].split('mass=')[-1])
        if niseq(mXi, pt.mXi): slhaError()

        mH1, wH1 = self._get_mh_wh(lns[7])
        if niseq(mH1, pt.mH1): slhaError()
        if niseq(wH1, pt.wTot['H1']): slhaError()
        mH2, wH2 = self._get_mh_wh(lns[8])
        if niseq(mH2, pt.mH2): slhaError()
        if niseq(wH2, pt.wTot['H2']): slhaError()
        mH3, wH3 = self._get_mh_wh(lns[9])
        if niseq(mH3, pt.mH3): slhaError()
        if niseq(wH3, pt.wTot['H3']): slhaError()
        mA, wA = self._get_mh_wh(lns[10])
        if niseq(mA, pt.mA): slhaError()
        if niseq(wA, pt.wTot['A']): slhaError()
        mHp, wHp = self._get_mh_wh(lns[11])
        if niseq(mHp, pt.mHp): slhaError()
        if niseq(wHp, pt.wTot['Hp']): slhaError()

        self._read_monojet(lns[16:24])

        self._read_relic(lns)

        self._read_indirect(lns)

        os.remove(self.VER_NAME)

    def _read_monojet(self, lns):

        self.monojet = {}
        mj = self.monojet

        mj['Description'] = lns[0][0:-2]

        mj['METs [GeV]'] = [float(x) for x in lns[1].split('>')[1:]]

        mj['Signal events'] = [float(x) for x in lns[2].split()[2:]]

        mj['1-CLs expected'] = [float(x) for x in lns[3].split()[2:]]

        mj['Most sens. MET region'] = float(lns[4].split('>')[-1])

        mj['1-CLs'] = [float(x) for x in lns[5].split()[1:]]

        N = len(mj['METs [GeV]'])
        if len(mj['Signal events']) != N:
            slhaError()
        if len(mj['1-CLs expected']) != N:
            slhaError()
        if len(mj['1-CLs']) != N:
            slhaError()

        mj['Signal excl. CL'] = float(lns[6].split()[-1])

    def _read_relic(self, lns):

        # Find first line
        for i, ln in enumerate(lns):
            if 'Xf=' in ln:
                start = i

        # Find last line
        for i, ln in enumerate(lns):
            if '<sig v>_FO =' in ln:
                stop = i + 1

        try:
            lns = lns[start:stop]
        except UnboundLocalError:
            slhaError()

        self.relic = {}
        rl = self.relic

        rl['Xf'] = float(lns[0].split('=')[1])

        rl['Omegahsq'] = float(lns[2].split('=')[1])

        chs = [x[3:-2] for x in lns if '->' in x]
        rl['Channels'] = [
            [
                x.split('% ')[1].replace('->', '-> '),
                float(x.split('% ')[0]) / 100.] for x in chs]

        rl['<sig v>_FO [pb]'] = float(lns[-1].split('=')[1].split('[')[0])

    def _read_indirect(self, lns):

        # Find first line
        for i, ln in enumerate(lns):
            if 'Indirect detection' in ln:
                start = i

        # Find last line
        for i, ln in enumerate(lns):
            if 'Antiproton flux' in ln:
                stop = i + 1

        try:
            lns = lns[start:stop]
        except UnboundLocalError:
            slhaError()

        self.indirect = {}
        dc = self.indirect

        dc['sig v [cm^3/s]'] = float(lns[3].split()[3])

        chs = [x for x in lns if '->' in x]
        dc['Channels'] = [
            [
                x[0:20].strip(),
                float(x[21:])] for x in chs]

        for ln in lns:
            if 'angle of sight' in ln:
                dc['Angle of sight [rad]'] = float(ln.split('f=')[1].split('[')[0])
            if 'cone with angle' in ln:
                dc['Cone angle [rad]'] = float(ln.split('angle')[1].split('[')[0])
            if 'Photon flux =' in ln:
                dc['Photon flux [1 / (cm^2 s GeV)], E [GeV]'] = [
                    float(ln.split(' = ')[1].split('[')[0]),
                    float(ln.split('E=')[1].split('[')[0])]
            if 'Positron flux =' in ln:
                dc['Positron flux [1 / (cm^2 sr s GeV)], E [GeV]'] = [
                    float(ln.split(' = ')[1].split('[')[0]),
                    float(ln.split('E=')[1].split('[')[0])]

    def _get_mh_wh(self, ln):

        x  = ln.split(' ')
        x = [y for y in x if y != '']

        m = float(x[1])
        w = float(x[2])

        return m, w


def niseq(a, b):
    return not abs(2. * abs(a - b) / (a + b)) < 1.e-2

def slhaError():
    raise RuntimeError(
        'LHA input not read properly by MO.')
