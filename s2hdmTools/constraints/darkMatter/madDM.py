import os
import shutil
import numpy as np
from decimal import Decimal

try:
    from s2hdmTools.constraints.darkMatter.maddmPATH \
        import MA_EXEC_DIR
    from s2hdmTools.constraints.darkMatter.pythonTWO \
        import PYTHON_2_EXEC
    MA_INSTALLED = True
except ModuleNotFoundError:
    MA_INSTALLED = False
from s2hdmTools.constants import Omegah2_Planck


class MadDM:

    def __init__(self, pt, gauge='unitary', vrel=2e-5):

        if not MA_INSTALLED:
            raise RuntimeError(
                'You called the MadDM interface, but MadDM ' +
                'was not installed.')

        if not vrel in [1e-3, 2e-5]:
            raise ValueError(
                'Wrong relative DM velocity.')

        if not gauge in ['feynman', 'unitary']:
            raise ValueError(
                "Possible gauges: 'feynman', 'unitary'. " +
                "You chose: '" + str(gauge) + "'.")

        try:
            pt.b_H1
        except AttributeError:
            pt.calculate_branching_ratios()

        if pt.yuktype == 2:
            if vrel == 2e-5:
                self.relic_dir = MA_EXEC_DIR + '/s2hdmii_relic'
            if vrel == 1e-3:
                self.relic_dir = MA_EXEC_DIR + '/s2hdmii_anom'
        else:
            raise NotImplementedError

        self.pt = pt
        self.vrel = vrel

    def execute(self):

        if self.vrel == 2e-5:

            self.write_param_card()
            self.run()
            self.read_output()

            dc = {}
            dc['relic'] = self.relic
            dc['indirect'] = self.indirect
            self.pt.MadDM = dc

        if self.vrel == 1e-3:

            self.write_param_card()
            self.run_anom()
            self.read_output_anom()

            self.pt.CentOfGalax = self.anom

    def write_param_card(self):

        pt = self.pt

        def nstr(x):
            y = '{0:.6e}'.format(Decimal(str(x)))
            if y[-3] == 'e':
                y = y.replace('e+', 'e+0')
                y = y.replace('e-', 'e-0')
            if y == '0.000000e+09':
                y = '0.000000e+00'
            return y

        card_dir = self.relic_dir + '/Cards/param_card.dat'

        with open(card_dir, 'r') as f:
            lns = f.readlines()

        # block mass
        lns[43] = '   25 ' + nstr(pt.mH1) + ' # Mh1 \n'
        lns[44] = '   29 ' + nstr(pt.mH2) + ' # Mh2 \n'
        lns[45] = '   30 ' + nstr(pt.mXi) + ' # MXi \n'
        lns[46] = '   36 ' + nstr(pt.mH3) + ' # Mh3 \n'
        lns[47] = '   37 ' + nstr(pt.mHp) + ' # MHp \n'
        lns[48] = '   38 ' + nstr(pt.mA) + ' # MA0 \n'

        # block s2hdmblock
        lns[66] = '    1 ' + nstr(pt.vs) + ' # vS \n'
        lns[67] = '    2 ' + nstr(pt.al1) + ' # al1 \n'
        lns[68] = '    3 ' + nstr(pt.al2) + ' # al2 \n'
        lns[69] = '    4 ' + nstr(pt.al3) + ' # al3 \n'
        lns[70] = '    5 ' + nstr(pt.tb) + ' # TB \n'
        lns[71] = '    6 ' + nstr(pt.m12sq) + ' # m12sq \n'

        # block decay
        lns[98] = 'DECAY   6 ' + nstr(pt.wTot['t']) + \
            ' # WT \n'
        lns[101] = 'DECAY  25 ' + nstr(pt.wTot['H1']) + \
            ' # Wh1 \n'
        lns[102] = 'DECAY  29 ' + nstr(pt.wTot['H2']) + \
            ' # Wh2 \n'
        lns[103] = 'DECAY  36 ' + nstr(pt.wTot['H3']) + \
            ' # Wh3 \n'
        lns[104] = 'DECAY  37 ' + nstr(pt.wTot['Hp']) + \
            ' # WHp \n'
        lns[105] = 'DECAY  38 ' + nstr(pt.wTot['A']) + \
            ' # WA0 \n'

        with open(card_dir, 'w') as f:
            f.writelines(lns)


    def run(self):

        CUR_DIR = os.getcwd() + '/'

        os.chdir(MA_EXEC_DIR)
        self.out_dir = self.relic_dir + '/output'

        runs = os.listdir(self.out_dir)
        for rn  in runs:
            shutil.rmtree(self.out_dir + '/' + rn)

        os.system(
            PYTHON_2_EXEC +
                ' ./bin/maddm.py run_relic_2.dat' +
                    ' > maddmtemp.txt 2>&1')

        os.chdir(CUR_DIR)

    def run_anom(self):

        CUR_DIR = os.getcwd() + '/'

        os.chdir(MA_EXEC_DIR)
        self.out_dir = self.relic_dir + '/output'

        runs = os.listdir(self.out_dir)
        for rn  in runs:
            shutil.rmtree(self.out_dir + '/' + rn)

        os.system(
            PYTHON_2_EXEC +
                ' ./bin/maddm.py run_anom_2.dat' +
                    ' > maddmtemp.txt 2>&1')

        os.chdir(CUR_DIR)

    def read_output(self):

        out = self.out_dir + '/run_01/MadDM_results.txt'

        with open(out, 'r') as f:
            lns = f.readlines()

        self.relic = {}
        rl = self.relic

        rl['Omegahsq'] = float(lns[9].split('=')[1])
        rl['Xf'] = float(lns[12].split('=')[1])
        rl['<sig v>_FO [cm^3 / s]'] = \
            float(lns[13].split('=')[1].split('#')[0])

        lnsREL = [x for x in lns if '%_' in x]

        for ln in lnsREL:

            x = ln.split(' %')[0].split('%_')[1]
            k, v = x.split('=')
            k = k.strip()

            rl[k] = float(v) / 100.0

        self.indirect = {}
        rl = self.indirect

        lnsIND = [x for x in lns if ('= [' in x) and ('>' in x)]

        for ln in lnsIND:

            x = ln.replace('=', ',').split(',')
            k = x[0].strip()
            v1 = float(x[1].replace('[', '').strip())
            v2 = float(x[2].replace(']', '').strip())

            rl[k] = [v1, v2]

        # Try to add rescaled xs if Omegah2 from micro available
        try:

            xi = self.pt.Micromegas['relic']['Omegahsq'] / Omegah2_Planck

            for k, v in rl.items():

                v1, v2 = v
                rl[k] = [v1, xi**2 * v1, v2]

            rl['xi'] = xi

        except AttributeError:

            pass

    def read_output_anom(self):

        out = self.out_dir + '/run_01/MadDM_results.txt'

        with open(out, 'r') as f:
            lns = f.readlines()

        self.anom = {}
        rl = self.anom

        # Start index
        for i, ln in enumerate(lns):
            if ('xxixxi' in ln) and ('=' in ln):
                istart = i
                break

        # End index
        for i, ln in enumerate(lns):
            if i <= istart:
                continue
            else:
                if 'TotalSM_xsec' in ln:
                    iend = i

        lns = lns[istart:iend]

        for ln in lns:

            k, v = ln.split('=')
            k = k.strip()
            v = float(v.split(',')[0].split('[')[1])

            rl[k] = v

        # Try to add rescaled xs if Omegah2 from micro available
        try:

            xi = self.pt.Micromegas['relic']['Omegahsq'] / Omegah2_Planck

            for k, v in rl.items():

                rl[k] = [v, xi**2 * v]

            rl['xi'] = xi

        except AttributeError:

            pass
