# import os
import json
import numpy as np

from s2hdmTools.constraints.ewpo.gfitterPath import STU_DATA_PATH
from s2hdmTools.constraints.ewpo.theo import Theo


# this_dir, this_filename = os.path.split(__file__)
# STU_DATA_PATH = this_dir + '/data/gfitter.json'

CHISQ3D95CL = 7.81
CHISQ2D95CL = 5.99

with open(STU_DATA_PATH + '/gfitter.json') as f:
    STU = json.load(f)

S3 = STU['3D']['central']['S']
T3 = STU['3D']['central']['T']
U3 = STU['3D']['central']['U']

dS3 = STU['3D']['standardDev']['S']
dT3 = STU['3D']['standardDev']['T']
dU3 = STU['3D']['standardDev']['U']

cST3 = STU['3D']['correlation']['ST']
cSU3 = STU['3D']['correlation']['SU']
cTU3 = STU['3D']['correlation']['TU']

Cinv3 = np.linalg.inv(np.array([[
    dS3 * dS3,
    dS3 * cST3 * dT3,
    dS3 * cSU3 * dU3], [
    dT3 * cST3 * dS3,
    dT3 * dT3,
    dT3 * cTU3 * dU3], [
    dU3 * cSU3 * dS3,
    dU3 * cTU3 * dT3,
    dU3 * dU3]]))

S2 = STU['2D']['central']['S']
T2 = STU['2D']['central']['T']

dS2 = STU['2D']['standardDev']['S']
dT2 = STU['2D']['standardDev']['T']

cST2 = STU['2D']['correlation']['ST']

Cinv2 = np.linalg.inv(np.array([[
    dS2 * dS2,
    dS2 * cST2 * dT2], [
    dT2 * cST2 * dS2,
    dT2 * dT2]]))

def check_stu(pt, mode='STU'):

    if not mode in ['ST', 'STU']:
        raise ValueError(
            'Invalid mode argument. Choose ST or STU.')

    pt.EWPO = {}

    ScalarMix = np.zeros(shape=(6, 6))

    # G0 -- A block
    ScalarMix[0, 0] = pt.cb
    ScalarMix[0, 1] = pt.sb
    ScalarMix[1, 0] = -pt.sb
    ScalarMix[1, 1] = pt.cb

    # 3 x 3 block for h1, h2, h3
    ScalarMix[2:5, 2:5] = pt.R

    # Xi is decoupled
    ScalarMix[5, 5] = 1.0

    U = np.array([
        [pt.cb, -pt.sb],
        [pt.sb, pt.cb]])

    Vtil = ScalarMix.T

    masq = np.array([0., pt.mHp**2])

    mubsq = np.array([
        0., pt.mA**2, pt.mH1**2, pt.mH2**2, pt.mH3**2, pt.mXi])

    pred = Theo(2, 2, U, Vtil, masq, mubsq)

    pred.calc_T()
    pred.calc_S()
    pred.calc_U()

    pt.EWPO['T'] = pred.T
    pt.EWPO['S'] = pred.S
    pt.EWPO['U'] = pred.U

    if mode == 'STU':

        chisq = chisq_3d(
            pt.EWPO['S'],
            pt.EWPO['T'],
            pt.EWPO['U'])

        pt.EWPO['Chisq3D'] = chisq

        if chisq > CHISQ3D95CL:
            res = False
        else:
            res = True

    if mode == 'ST':

        chisq = chisq_2d(
            pt.EWPO['S'],
            pt.EWPO['T'])

        pt.EWPO['Chisq2D'] = chisq

        if chisq > CHISQ2D95CL:
            res = False
        else:
            res = True

    return res

def chisq_3d(S, T, U):

    v = np.array([
        S - S3,
        T - T3,
        U - U3])

    return v.dot(Cinv3.dot(v))

def chisq_2d(S, T):

    pass
    v = np.array([
        S - S2,
        T - T2])

    return v.dot(Cinv2.dot(v))
