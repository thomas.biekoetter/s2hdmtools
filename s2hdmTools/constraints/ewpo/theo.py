import numpy as np


MW = 80.379
MZ = 91.1876
MHSM = 125.09
MWsq = MW**2
MZsq = MZ**2
MHSMsq = MHSM**2
CTW = MW / MZ
STW = np.sqrt(1. - CTW**2)


class Theo:
    """ Based on 0802.4353 """

    def __init__(
            self, nd, nn, U, Vtil,
            masq, mubsq):

        m = 2 * nd + nn

        self.check_shapes(U, Vtil, nd, m, masq, mubsq)

        self.decomp_Vtil(Vtil, nd, m)

        self.U = U

        self.nd = nd
        self.nn = nn
        self.m = m

        self.masq = masq
        self.mubsq = mubsq

        V = self.V
        self.Uc = U.T
        Uc = self.Uc
        self.Vc = np.conjugate(V).T
        Vc = self.Vc

        # For T
        self.UcV = np.matmul(Uc, V)
        self.iVcV = np.matmul(Vc, V).imag
        self.UcU = np.matmul(Uc, U)

        # For S
        self.VcV = np.matmul(Vc, V)

    def check_shapes(self, U, Vtil, nd, m, masq, mubsq):

        if U.shape != (nd, nd):

            raise ValueError(
                'Shape of argument U does not match ' +
                'value of nd.')

        if Vtil.shape != (m, m):

            raise ValueError(
                'Shape of argument Vtil does not match ' +
                'values of nd and nn.')


        if masq.shape != (nd, ):

            raise ValueError(
                'Shape of argument masq does not match ' +
                'value of nd.')

        if mubsq.shape != (m, ):

            raise ValueError(
                'Shape of argument mubsq does not match ' +
                'values of nd and nn.')

    def decomp_Vtil(self, Vtil, nd, m):

        lim1 = nd
        lim2 = 2 * nd
        lim3 = m

        Vim = Vtil[0:lim1, ...]
        Vre = Vtil[lim1:lim2, ...]

        self.V = Vre + 1j * Vim

        self.R = Vtil[lim2:lim3, ...]

    def calc_S(self):

        U = self.U
        V = self.V

        masq = self.masq
        mubsq = self.mubsq

        UcV = self.UcV
        iVcV = self.iVcV
        UcU = self.UcU
        VcV = self.VcV

        n = self.nd
        m = self.m

        res = 0

        for a in range(1, n):
            res += (2.0 * STW**2 - UcU[a, a])**2 * G(masq[a], masq[a], MZsq)

        if n > 2:
            for a1 in range(1, n - 1):
                for a2 in range(a1 + 1, n):
                    res += 2.0 * abs(UcU[a1, a2])**2 * G(masq[a1], masq[a2], MZsq)

        if m > 2:
            for b1 in range(1, m - 1):
                for b2 in range(b1 + 1, m):
                    res += iVcV[b1, b2]**2 * G(mubsq[b1], mubsq[b2], MZsq)

        for a in range(1, n):
            res -= 2.0 * UcU[a, a] * np.log(masq[a])

        for b in range(1, m):
            # Diagonal elements of VcV are real
            res += VcV[b, b].real * np.log(mubsq[b])

        res -= np.log(MHSMsq)

        for b in range(1, m):
            res += iVcV[0, b]**2 * Gd(mubsq[b], MZsq)

        res -= Gd(MHSMsq, MZsq)

        res /= 24.0 * np.pi

        self.S = res

    def calc_T(self):

        masq = self.masq
        mubsq = self.mubsq

        UcV = self.UcV
        iVcV = self.iVcV
        UcU = self.UcU

        n = self.nd
        m = self.m

        res = 0

        for a in range(1, n):
            for b in range(1, m):
                res += abs(UcV[a, b])**2 * F(masq[a], mubsq[b])

        for b1 in range(1, m - 1):
            for b2 in range(b1 + 1, m):
                res -= iVcV[b1, b2]**2 * F(mubsq[b1], mubsq[b2])

        if n > 2:
            for a1 in range(1, n - 1):
                for a2 in range(a1 + 1, n):
                    res -= 2.0 * UcU[a1, a2]**2 * F(masq[a1], masq[a2])

        for b in range(1, m):
            res += 3.0 * iVcV[0, b]**2 * (
                F(MZsq, mubsq[b]) - F(MWsq, mubsq[b]))

        res -= 3.0 * (F(MZsq, MHSMsq) - F(MWsq, MHSMsq))

        res /= 16. * np.pi * MWsq * STW**2

        self.T = res

    def calc_U(self):

        masq = self.masq
        mubsq = self.mubsq

        UcV = self.UcV
        iVcV = self.iVcV
        UcU = self.UcU

        n = self.nd
        m = self.m

        res = 0

        for a in range(1, n):
            for b in range(1, m):
                res += abs(UcV[a, b])**2 * G(masq[a], mubsq[b], MWsq)

        for a in range(1, n):
            res -= (2.0 * STW**2 - UcU[a, a])**2 * G(masq[a], masq[a], MZsq)

        if n > 2:
            for a1 in range(1, n - 1):
                for a2 in range(a1 + 1, n):
                    res -= 2.0 * UcU[a1, a2]**2 * G(masq[a1], masq[a2], MZsq)

        for b1 in range(1, m - 1):
            for b2 in range(b1 + 1, m):
                res -= iVcV[b1, b2]**2 * G(mubsq[b1], mubsq[b2], MZsq)

        for b in range(1, m):
            res  += iVcV[0, b]**2 * (Gd(mubsq[b], MWsq) - Gd(mubsq[b], MZsq))

        res -= Gd(MHSMsq, MWsq)

        res += Gd(MHSMsq, MZsq)

        res /= 24.0 * np.pi

        self.U = res


def F(a, b):

    if abs(a - b) < 5.:

        res = (
            (a - b)**10 / (110. * b**9) -
            (a - b)**9 / (90. * b**8) +
            (a - b)**8 / (72. * b**7) -
            (a - b)**7 / (56. * b**6) +
            (a - b)**6 / (42. * b**5) -
            (a - b)**5 / (30. * b**4) +
            (a - b)**4 / (20. * b**3) -
            (a - b)**3 / (12. * b**2) +
            (a - b)**2 / (6. * b)
        )

    else:

        res = (
            (a + b) / 2. -
            (a * b * (
                np.log(a) -
                np.log(b))) / (a - b)
        )

    return res

def f(t, r):

    if r < 0.0:

        fac = np.sqrt(-r)

        res = 2. * fac * \
            np.arctan(fac / t)

    elif abs(r) < 1.e-2:

        res = 0.0

    else:

        fac = np.sqrt(r)

        res = fac * np.log(np.abs(
            (t - fac) / (t + fac)))

    return res

def G(a, b, c):

    t = a + b - c
    r = c**2 - 2.0 * c * (a + b) + (a - b)**2
    f0 = f(t, r)

    if abs(a - b) < 5.0:

        res = (
            -16. / 3.0 + (8.0 * (a - b)) / c -
            (a - b)**3 / (b**2 * c) + (16*b)/c +
            (2.0 * (a - b)**2 * (-4 * b + c)) / (b * c**2) +
            ((a - b)**4 * (
                10.0 * b**2 -
                5.0 * b * c +
                7 * c**2)) / (10. * b**3 * c**3) -
            ((a - b)**5 * (
                10.0 * b**2 -
                10.0 * b * c +
                11.0 * c**2)) / (20. * b**4 * c**3) -
            ((a - b)**7 * (
                35.0 * b**2 -
                56.0 * b * c +
                55.0 * c**2)) / (140. * b**6 * c**3) +
            ((a - b)**8 * (
                84.0 * b**2 -
                150 * b * c +
                145 * c**2)) / (420. * b**7 * c**3) +
            ((a - b)**6 * (
                140.0 * b**2 -
                189.0 * b * c +
                192.0 * c**2)) / (420. * b**5 * c**3) -
            ((a - b)**9 * (
                140.0 * b**2 -
                270.0 * b * c +
                259.0 * c**2)) / (840. * b**8 * c**3) +
            ((a - b)**10 * (
                1320.0 * b**2 -
                2695.0 * b * c +
                2576.0 * c**2)) / (9240. * b**9 * c**3) +
            (f0 *r ) / c**3
        )

    else:

        res = (
            -16.0 / 3.0 -
            (2.0 * (a - b)**2) / c**2 +
            (5.0 * (a + b)) / c +
            (f0 * r) / c**3 +
            (3.0 * (
                (a**2 + b**2) / (a - b) +
                (a - b)**3 / (3.0 * c**2) -
                (a**2 - b**2) / c) * np.log(a / b) ) / c
        )

    return res

def Gd(a, b):

    fin1 = a
    fin2 = a**2 - 4.0 * a * b
    f0 = f(fin1, fin2)

    if abs(a - b) < 5.0:

        res = (
            -112.0 / 3.0 +
            (1483.0 * (a - b)**10) / (3080. * b**10) -
            (493.0 * (a - b)**9) / (840. * b**9) +
            (103.0 * (a - b)**8) / (140. * b**8) -
            (67.0 * (a - b)**7) / (70. * b**7) +
            (551.0 * (a - b)**6) / (420. * b**6) -
            (39.0 * (a - b)**5) / (20. * b**5) +
            (17.0 * (a - b)**4) / (5. * b**4) -
            (5.0 * (a - b)**3) / b**3 +
            ((a - b) * (8.0 * b - 2.0 * f0)) / b**2 +
            (9.0 * f0) / b +
            ((a - b)**2 * (4.0 * b + f0)) / b**3
        )

    else:

        res = (
            -79.0 / 3.0 -
            (2.0 * a**2) / b**2 +
            (9.0 * a) / b +
            ((12.0 + a**2 / b**2 - (4.0 * a) / b) * f0) / b +
            (
                -10.0 +
                a**3 / b**3 -
                (6.0 * a**2) / b**2 +
                (18.0 * a) / b -
                (9.0 * (a + b)) / (a - b)) * np.log(a / b)
        )

    return res
