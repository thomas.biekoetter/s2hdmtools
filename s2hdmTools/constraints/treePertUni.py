import numpy as np


CUTOFF = 8. * np.pi
SKIPPER = 100

def pertuni(
        lam1, lam2, lam3, lam4,
        lam5, lam6, lam7, lam8):

    PertUni = True

    cond = np.zeros(shape=(16, ))

    cond[0] = np.abs(lam3 + lam4)
    under = (lam1 - lam2)**2 + 4*lam4**2
    cond[1] = np.abs(
        lam1 + lam2 - np.sqrt(under))/2.
    cond[2] = np.abs(
        lam1 + lam2 + np.sqrt(under))/2.
    cond[3] = np.abs(lam3 + 2*lam4 - 3*lam5)
    cond[4] = np.abs(lam3 - lam5)
    cond[5] = np.abs(lam3 + lam5)
    cond[6] = np.abs(lam3 + 2*lam4 + 3*lam5)
    under = (lam1 - lam2)**2 + 4*lam5**2
    cond[7] = np.abs(
        lam1 + lam2 - np.sqrt(under))/2.
    cond[8] = np.abs(
        lam1 + lam2 + np.sqrt(under))/2.
    cond[9] = np.abs(lam6)
    cond[10] = np.abs(lam7)
    cond[11] = np.abs(lam8)
    cond[12] = np.abs(lam3 - lam4)

    coeffs = [
        1,
        (
            -6*lam1 - 6*lam2 - 4*lam6
        ),
        (
            36*lam1*lam2 - 16*lam3**2 - 16*lam3*lam4 - 4*lam4**2
            + 24*lam1*lam6 + 24*lam2*lam6 - 8*lam7**2 - 8*lam8**2
        ),
        (
            -144*lam1*lam2*lam6 + 64*lam3**2*lam6
            + 64*lam3*lam4*lam6 + 16*lam4**2*lam6 + 48*lam2*lam7**2
            - 64*lam3*lam7*lam8 - 32*lam4*lam7*lam8 + 48*lam1*lam8**2
        )
    ]

    sol = np.abs(np.roots(coeffs)) / 2.

    cond[13] = sol[0]
    cond[14] = sol[1]
    cond[15] = sol[2]

    for cn in cond:
        if cn > CUTOFF:
            PertUni = False
            break

    return cond, PertUni


def _pertuni_tf(
        lam1, lam2, lam3, lam4,
        lam5, lam6, lam7, lam8):

    y =  pertuni(
        lam1, lam2, lam3, lam4,
        lam5, lam6, lam7, lam8)

    return y[1]


_vec_pertuni = np.vectorize(_pertuni_tf)


def pertuni_to_scale(
        pt, lamsAtScale, scales):

    # Check pert. uni. until landau pole

    lams = lamsAtScale[::SKIPPER, ...]

    pertis = _vec_pertuni(
        lams[..., 0],
        lams[..., 1],
        lams[..., 2],
        lams[..., 3],
        lams[..., 4],
        lams[..., 5],
        lams[..., 6],
        lams[..., 7])

    for i, pr in enumerate(pertis):

        if not pr:

            cutoffP = scales[i * SKIPPER]
            IsNonPert = True
            iP = i * SKIPPER
            break
    else:

        IsNonPert = False

    dc = {'IsNonPert': IsNonPert}

    if IsNonPert:

        dc['Unitarity_scale'] = cutoffP
        dc['Unitarity_index'] = iP

    return dc

