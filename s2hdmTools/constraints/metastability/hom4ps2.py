import os
import warnings
import numpy as np
from decimal import Decimal

from s2hdmTools.constraints.metastability.hom4ps2PATH \
    import PS_EXEC_DIR


class Hom4ps2:

    def __init__(
            self, pt,
            imag_eps=1.e-5,
            tadpoles_eps=1.e-3):

        self.pt = pt

        self.IN_NAME = 's2hdm.sys'
        self.OUT_NAME = 'data.roots'
        self.VER_NAME = 'hom4ps.log'

        self.imag_part_thresh = imag_eps
        self.tadpoles_thresh = tadpoles_eps

        self.num_fields = 6

        N = self.num_fields
        ## Coefs with field dim. 3
        # A: i1: Eq, i2: Field^3
        self.A = np.zeros(shape=(N, N))
        # B: i1: Eq, i2: Field^2, i3: Field
        self.B = np.zeros(shape=(N, N, N))
        # C: i1: Eq, i2: Field, i3: Field, i4: Field
        self.C = np.zeros(shape=(N, N, N, N))

        ## Coefs with field dim. 2
        # D: i1: Eq, i2: Field^2
        self.D = np.zeros(shape=(N, N))
        # E: i1: Eq, i2: Field, i3: Field
        self.E = np.zeros(shape=(N, N, N))

        ## Coefs with field dim. 1
        # F: i1: Eq, i2: Field
        self.F = np.zeros(shape=(N, N))

        self.calc_A()
        self.calc_B()
        self.calc_C()
        self.calc_D()
        self.calc_E()
        self.calc_F()

        if pt.debug:
            self.check_tadpoles()

    def calc_A(self):

        m11sq = self.pt.m11sq
        m12sq = self.pt.m12sq
        m22sq = self.pt.m22sq
        mSsq = self.pt.mSsq
        mXsq = self.pt.mXsq
        L1 = self.pt.lam1
        L2 = self.pt.lam2
        L6 = self.pt.lam6

        self.A[0, 0] = L1/2.
        self.A[1, 1] = L2/2.
        self.A[2, 2] = L2/2.
        self.A[3, 3] = L2/2.
        self.A[4, 4] = L6/2.
        self.A[5, 5] = L6/2.

    def calc_B(self):

        m11sq = self.pt.m11sq
        m12sq = self.pt.m12sq
        m22sq = self.pt.m22sq
        mSsq = self.pt.mSsq
        mXsq = self.pt.mXsq
        L1 = self.pt.lam1
        L2 = self.pt.lam2
        L3 = self.pt.lam3
        L4 = self.pt.lam4
        L5 = self.pt.lam5
        L6 = self.pt.lam6
        L7 = self.pt.lam7
        L8 = self.pt.lam8

        self.B[0, 1, 0] = L3/2.
        self.B[0, 2, 0] = L3/2. + L4/2. + L5/2.
        self.B[0, 3, 0] = L3/2. + L4/2. - L5/2.
        self.B[0, 4, 0] = L7/2.
        self.B[0, 5, 0] = L7/2.
        self.B[1, 0, 1] = L3/2.
        self.B[1, 2, 1] = L2/2.
        self.B[1, 3, 1] = L2/2.
        self.B[1, 4, 1] = L8/2.
        self.B[1, 5, 1] = L8/2.
        self.B[2, 0, 2] = L3/2. + L4/2. + L5/2.
        self.B[2, 1, 2] = L2/2.
        self.B[2, 3, 2] = L2/2.
        self.B[2, 4, 2] = L8/2.
        self.B[2, 5, 2] = L8/2.
        self.B[3, 0, 3] = L3/2. + L4/2. - L5/2.
        self.B[3, 1, 3] = L2/2.
        self.B[3, 2, 3] = L2/2.
        self.B[3, 4, 3] = L8/2.
        self.B[3, 5, 3] = L8/2.
        self.B[4, 0, 4] = L7/2.
        self.B[4, 1, 4] = L8/2.
        self.B[4, 2, 4] = L8/2.
        self.B[4, 3, 4] = L8/2.
        self.B[4, 5, 4] = L6/2.
        self.B[5, 0, 5] = L7/2.
        self.B[5, 1, 5] = L8/2.
        self.B[5, 2, 5] = L8/2.
        self.B[5, 3, 5] = L8/2.
        self.B[5, 4, 5] = L6/2.

    def calc_C(self):

        pass

    def calc_D(self):

        pass

    def calc_E(self):

        pass

    def calc_F(self):

        m11sq = self.pt.m11sq
        m12sq = self.pt.m12sq
        m22sq = self.pt.m22sq
        mSsq = self.pt.mSsq
        mXsq = self.pt.mXsq

        self.F[0, 0] = m11sq
        self.F[0, 2] = -m12sq
        self.F[1, 1] = m22sq
        self.F[2, 0] = -m12sq
        self.F[2, 2] = m22sq
        self.F[3, 3] = m22sq
        self.F[4, 4] = mSsq/2. - mXsq/2.
        self.F[5, 5] = mSsq/2. + mXsq/2.

    def calc_tadpoles(self, x):

        N = self.num_fields
        y = np.zeros(shape=(N, ))

        for i in range(0, N):
            for j in range(0, N):
                y[i] += x[j]**3 * self.A[i, j]

        for i in range(0, N):
            for j in range(0, N):
                for k in range(0, N):
                    y[i] += x[j]**2 * x[k] * self.B[i, j, k]

        for i in range(0, N):
            for j in range(0, N):
                for k in range(j + 1, N):
                    for l in range(k + 1, N):
                        y[i] += x[j] * x[k] * x[l] * self.C[i, j, k, l]

        for i in range(0, N):
            for j in range(0, N):
                y[i] += x[j]**2 * self.D[i, j]

        for i in range(0, N):
            for j in range(0, N):
                for k in range(j + 1, N):
                    y[i] += x[j] * x[k] * self.E[i, j, k]

        for i in range(0, N):
            for j in range(0, N):
                y[i] += x[j] * self.F[i, j]

        return y


    def check_tadpoles(self, x=None):

        if x is None:

            x = np.zeros(shape=(self.num_fields, ))
            x[0] = self.pt.v1
            x[2] = self.pt.v2
            x[4] = self.pt.vs

        if np.abs(np.sum(self.calc_tadpoles(x))) > \
            self.tadpoles_thresh:

            raise RuntimeError(
                'Tree-level tadpoles not zero for vevs' +
                ' of benchmark point.')

    def execute(self):

        self.create_inputfile()

        self.run()

        self.read_log()
        self.read_outputfile()

        self.check_solutions()

        self.extract_minima()

        self.check_deeper()

    def create_inputfile(self):

        N = self.num_fields

        eqs = [''] * N

        for i in range(0, N):
            for j in range(0, N):
                n = self.A[i, j]
                nstr = '{0:.12E}'.format(Decimal(str(n)))
                if n > 0:
                    expr = '+' + nstr + '*' + 'x' + str(j + 1) + '^3\n'
                elif n < 0:
                    expr = nstr + '*' + 'x' + str(j + 1) + '^3\n'
                else:
                    expr = ''
                eqs[i] += expr

        for i in range(0, N):
            for j in range(0, N):
                for k in range(0, N):
                    n = self.B[i, j, k]
                    nstr = '{0:.12E}'.format(Decimal(str(n)))
                    if n > 0:
                        expr = '+' + nstr + '*' + 'x' + \
                            str(j + 1) + '^2*x' + str(k + 1) + '\n'
                    elif n < 0:
                        expr = nstr + '*' + 'x' + str(j + 1) + \
                            '^2*x' + str(k + 1) + '\n'
                    else:
                        expr = ''
                    eqs[i] += expr

        for i in range(0, N):
            for j in range(0, N):
                for k in range(j + 1, N):
                    for l in range(k + 1, N):
                        n = self.C[i, j, k, l]
                        nstr = '{0:.12E}'.format(Decimal(str(n)))
                        if n > 0:
                            expr = '+' + nstr + '*' + 'x' + \
                                str(j + 1) + '*x' + str(k + 1) + \
                                    '*x' + str(l + 1) + '\n'
                        elif n < 0:
                            expr = nstr + '*' + 'x' + str(j + 1) + \
                                '*x' + str(k + 1) + \
                                    '*x' + str(l + 1) + '\n'
                        else:
                            expr = ''
                        eqs[i] += expr

        for i in range(0, N):
            for j in range(0, N):
                n = self.D[i, j]
                nstr = '{0:.12E}'.format(Decimal(str(n)))
                if n > 0:
                    expr = '+' + nstr + '*' + 'x' + str(j + 1) + '^2\n'
                elif n < 0:
                    expr = nstr + '*' + 'x' + str(j + 1) + '^2\n'
                else:
                    expr = ''
                eqs[i] += expr

        for i in range(0, N):
            for j in range(0, N):
                for k in range(j + 1, N):
                    n = self.E[i, j, k]
                    nstr = '{0:.12E}'.format(Decimal(str(n)))
                    if n > 0:
                        expr = '+' + nstr + '*' + 'x' + \
                            str(j + 1) + '*x' + str(k + 1) + '\n'
                    elif n < 0:
                        expr = nstr + '*' + 'x' + str(j + 1) + \
                            '*x' + str(k + 1) + '\n'
                    else:
                        expr = ''
                    eqs[i] += expr

        for i in range(0, N):
            for j in range(0, N):
                n = self.F[i, j]
                nstr = '{0:.12E}'.format(Decimal(str(n)))
                if n > 0:
                    expr = '+' + nstr + '*' + 'x' + str(j + 1) + '\n'
                elif n < 0:
                    expr = nstr + '*' + 'x' + str(j + 1) + '\n'
                else:
                    expr = ''
                eqs[i] += expr

        for i in range(0, N): # Removes \n after last term
            eqs[i] = eqs[i][0:-1]

        for i in range(0, N): # Removes leading +
            if eqs[i][0] == '+':
                eqs[i] = eqs[i][1:]

        with open(self.IN_NAME, 'w') as f:

            def fw(x): f.write(x)

            fw('{\n')

            for i in range(0, N):
                fw(eqs[i])
                if i < N - 1:
                    fw(';\n')
                else:
                    fw('\n')

            fw('}')

    def run(self):

        CUR_DIR = os.getcwd() + '/'

        os.rename(
            CUR_DIR + self.IN_NAME,
            PS_EXEC_DIR + self.IN_NAME)

        os.chdir(PS_EXEC_DIR)

        os.system(
            'echo 1 | ./hom4ps2 ' + self.IN_NAME + \
                ' > ' + self.VER_NAME)

        os.rename(
            PS_EXEC_DIR + self.OUT_NAME,
            CUR_DIR + self.OUT_NAME)

        os.rename(
            PS_EXEC_DIR + self.VER_NAME,
            CUR_DIR + self.VER_NAME)

        os.remove(self.IN_NAME)

        os.chdir(CUR_DIR)

    def read_log(self):

        with open(self.VER_NAME, 'r') as f:
            lns = f.readlines()

        for ln in lns:

            if 'paths followed' in ln:
                num_path_followed = int(ln.split(' ')[-1])

            if 'of roots =' in ln:
                num_roots = int(ln.split(' ')[-1])

            if 'of real roots' in ln:
                num_real_roots = int(ln.split(' ')[-1])

            if 'blow_up' in ln:
                num_blowups = \
                    int(ln.split('=')[1].replace(' ', \
                        '').split('(')[0])
        try:

            self.num_path_followed = num_path_followed
            self.num_roots = num_roots
            self.num_real_roots = num_real_roots
            self.num_blowups = num_blowups

        except NameError:

            raise RuntimeError('log file corrupted.')

        os.remove(self.VER_NAME)

    def read_outputfile(self):

        # List of lists with lines for each root
        rts = []
        rt = []

        with open(self.OUT_NAME, 'r') as f:
            lns = f.readlines()

        for i, ln in enumerate(lns):

            if not '----' in ln:
                rt.append(ln)
            else:
                rts.append(rt)
                rt = []

            if 'order of variables' in ln:
                ind_order = i

        varpos = {
            '1': int(lns[ind_order + 1].split('x')[1][0]),
            '2': int(lns[ind_order + 2].split('x')[1][0]),
            '3': int(lns[ind_order + 3].split('x')[1][0]),
            '4': int(lns[ind_order + 4].split('x')[1][0]),
            '5': int(lns[ind_order + 5].split('x')[1][0]),
            '6': int(lns[ind_order + 6].split('x')[1][0])
        }

        y = np.zeros(
            shape=(len(rts), self.num_fields),
            dtype='complex128')

        for i, rt in enumerate(rts):

            x = self._read_root_from_lines(rt, varpos)
            y[i, ...] = x

        self.complex_roots = y

        self._extract_real_roots()

        self._check_output()

        os.remove(self.OUT_NAME)

    def _read_root_from_lines(self, rt, varpos):

        x = np.zeros(
            shape=(self.num_fields, ),
            dtype='complex128')

        def fn(ln):

            y = ln.split(',')[0].split('(')[1]
            y = y.replace(' ', '')
            return float(y)

        def sn(ln):

            y = ln.split(',')[1].split(')')[0]
            y = y.replace(' ', '')
            return float(y)

        for i, ln in enumerate(rt):

            try:

                rlpt = fn(ln)
                impt = sn(ln)
                flid = varpos[str(i + 1)] - 1
                x[flid] = rlpt + 1j * impt

            except IndexError:

                break

        return x

    def _extract_real_roots(self):

        y = self.complex_roots
        xl  = []

        for i in range(y.shape[0]):

            x = y[i, ...]
            is_real = True

            for xi in x:
                if abs(xi.imag) > self.imag_part_thresh:
                    is_real = False

            if is_real:
                xl.append(x.real)

        self.real_roots = np.array(xl)

    def _check_output(self):

        if len(self.complex_roots) != self.num_roots:
            raise RuntimeError(
                'A root is lost.')

        if len(self.real_roots) != self.num_real_roots:
            warnings.warn(
                'A real root is lost.',
                RuntimeWarning)

        self.ind_ew_in_real, \
            self.sol_ew = self._check_ew_found()

    def _check_ew_found(self):

        y = self.real_roots
        ew = self.pt.xew

        found = False

        for i in range(0, len(y)):

            x = y[i, ...]
            if np.sum(np.abs(x - ew)) < self.tadpoles_thresh * 1.e1:
                iew = i
                y = iew, y[iew, ...]
                break

        else:

            warnings.warn(
                'Did not find EW vacuum.',
                RuntimeWarning)

            y = None, None

        return y

    def check_solutions(self):

        sol = self.real_roots
        newsol = []

        for x in sol:

            gr = self.calc_tadpoles(x)
            eps = np.sum(np.abs(gr))


            if eps > self.tadpoles_thresh:
                print(gr)

                warnings.warn(
                    'HOM4PS2 solution not real solution. Throw away...',
                    RuntimeWarning)

            else:

                newsol.append(x)

        self.real_roots = np.array(newsol)

    def extract_minima(self):

        mn = []

        for x in self.real_roots:

            hs = self.hessV0(x)
            ei = np.linalg.eig(hs)[0]

            if np.all(ei > 0.0):

                mn.append(x)

        self.local_minima = np.array(mn)

    def check_deeper(self):

        dangs = []

        xew = self.pt.xew
        Vew = self.V0(xew)

        for x in self.local_minima:

            V = self.V0(x)

            if V < Vew:
                xdiff = np.sum(np.abs(np.abs(x) - xew))
                if xdiff > 1.e2 * self.tadpoles_thresh:
                    dangs.append(x)

        self.dangerous_minima = np.array(dangs)

    def V0(self, X):

        m11sq = self.pt.m11sq
        m12sq = self.pt.m12sq
        m22sq = self.pt.m22sq
        mSsq = self.pt.mSsq
        mXsq = self.pt.mXsq

        L1 = self.pt.lam1
        L2 = self.pt.lam2
        L3 = self.pt.lam3
        L4 = self.pt.lam4
        L5 = self.pt.lam5
        L6 = self.pt.lam6
        L7 = self.pt.lam7
        L8 = self.pt.lam8

        vh1r0 = X[..., 0]
        vh2rp = X[..., 1]
        vh2r0 = X[..., 2]
        vh2i0 = X[..., 3]
        vhsr0 = X[..., 4]
        vhsi0 = X[..., 5]

        res = (
            (m11sq*vh1r0**2)/2. + (L1*vh1r0**4)/8. + (m22sq*vh2i0**2)/2. +
              (L3*vh1r0**2*vh2i0**2)/4. + (L4*vh1r0**2*vh2i0**2)/4. -
              (L5*vh1r0**2*vh2i0**2)/4. + (L2*vh2i0**4)/8. - m12sq*vh1r0*vh2r0 +
              (m22sq*vh2r0**2)/2. + (L3*vh1r0**2*vh2r0**2)/4. +
              (L4*vh1r0**2*vh2r0**2)/4. + (L5*vh1r0**2*vh2r0**2)/4. +
              (L2*vh2i0**2*vh2r0**2)/4. + (L2*vh2r0**4)/8. + (m22sq*vh2rp**2)/2. +
              (L3*vh1r0**2*vh2rp**2)/4. + (L2*vh2i0**2*vh2rp**2)/4. +
              (L2*vh2r0**2*vh2rp**2)/4. + (L2*vh2rp**4)/8. + (mSsq*vhsi0**2)/4. +
              (mXsq*vhsi0**2)/4. + (L7*vh1r0**2*vhsi0**2)/4. +
              (L8*vh2i0**2*vhsi0**2)/4. + (L8*vh2r0**2*vhsi0**2)/4. +
              (L8*vh2rp**2*vhsi0**2)/4. + (L6*vhsi0**4)/8. + (mSsq*vhsr0**2)/4. -
              (mXsq*vhsr0**2)/4. + (L7*vh1r0**2*vhsr0**2)/4. +
              (L8*vh2i0**2*vhsr0**2)/4. + (L8*vh2r0**2*vhsr0**2)/4. +
              (L8*vh2rp**2*vhsr0**2)/4. + (L6*vhsi0**2*vhsr0**2)/4. + (L6*vhsr0**4)/8.
        )

        return res

    def hessV0(self, X):

        N = self.num_fields

        res = np.zeros(shape=(N, N))

        m11sq = self.pt.m11sq
        m12sq = self.pt.m12sq
        m22sq = self.pt.m22sq
        mSsq = self.pt.mSsq
        mXsq = self.pt.mXsq

        L1 = self.pt.lam1
        L2 = self.pt.lam2
        L3 = self.pt.lam3
        L4 = self.pt.lam4
        L5 = self.pt.lam5
        L6 = self.pt.lam6
        L7 = self.pt.lam7
        L8 = self.pt.lam8

        vh1r0 = X[..., 0]
        vh2rp = X[..., 1]
        vh2r0 = X[..., 2]
        vh2i0 = X[..., 3]
        vhsr0 = X[..., 4]
        vhsi0 = X[..., 5]

        res[0, 0] = m11sq + (3*L1*vh1r0**2)/2. + (L3*vh2i0**2)/2. + (L4*vh2i0**2)/2. - (L5*vh2i0**2)/2. + (L3*vh2r0**2)/2. + (L4*vh2r0**2)/2. + (L5*vh2r0**2)/2. + (L3*vh2rp**2)/2. + (L7*vhsi0**2)/2. + (L7*vhsr0**2)/2.
        res[0, 1] = L3*vh1r0*vh2rp
        res[0, 2] = -m12sq + L3*vh1r0*vh2r0 + L4*vh1r0*vh2r0 + L5*vh1r0*vh2r0
        res[0, 3] = L3*vh1r0*vh2i0 + L4*vh1r0*vh2i0 - L5*vh1r0*vh2i0
        res[0, 4] = L7*vh1r0*vhsr0
        res[0, 5] = L7*vh1r0*vhsi0
        res[1, 0] = L3*vh1r0*vh2rp
        res[1, 1] = m22sq + (L3*vh1r0**2)/2. + (L2*vh2i0**2)/2. + (L2*vh2r0**2)/2. + (3*L2*vh2rp**2)/2. + (L8*vhsi0**2)/2. + (L8*vhsr0**2)/2.
        res[1, 2] = L2*vh2r0*vh2rp
        res[1, 3] = L2*vh2i0*vh2rp
        res[1, 4] = L8*vh2rp*vhsr0
        res[1, 5] = L8*vh2rp*vhsi0
        res[2, 0] = -m12sq + L3*vh1r0*vh2r0 + L4*vh1r0*vh2r0 + L5*vh1r0*vh2r0
        res[2, 1] = L2*vh2r0*vh2rp
        res[2, 2] = m22sq + (L3*vh1r0**2)/2. + (L4*vh1r0**2)/2. + (L5*vh1r0**2)/2. + (L2*vh2i0**2)/2. + (3*L2*vh2r0**2)/2. + (L2*vh2rp**2)/2. + (L8*vhsi0**2)/2. + (L8*vhsr0**2)/2.
        res[2, 3] = L2*vh2i0*vh2r0
        res[2, 4] = L8*vh2r0*vhsr0
        res[2, 5] = L8*vh2r0*vhsi0
        res[3, 0] = L3*vh1r0*vh2i0 + L4*vh1r0*vh2i0 - L5*vh1r0*vh2i0
        res[3, 1] = L2*vh2i0*vh2rp
        res[3, 2] = L2*vh2i0*vh2r0
        res[3, 3] = m22sq + (L3*vh1r0**2)/2. + (L4*vh1r0**2)/2. - (L5*vh1r0**2)/2. + (3*L2*vh2i0**2)/2. + (L2*vh2r0**2)/2. + (L2*vh2rp**2)/2. + (L8*vhsi0**2)/2. + (L8*vhsr0**2)/2.
        res[3, 4] = L8*vh2i0*vhsr0
        res[3, 5] = L8*vh2i0*vhsi0
        res[4, 0] = L7*vh1r0*vhsr0
        res[4, 1] = L8*vh2rp*vhsr0
        res[4, 2] = L8*vh2r0*vhsr0
        res[4, 3] = L8*vh2i0*vhsr0
        res[4, 4] = mSsq/2. - mXsq/2. + (L7*vh1r0**2)/2. + (L8*vh2i0**2)/2. + (L8*vh2r0**2)/2. + (L8*vh2rp**2)/2. + (L6*vhsi0**2)/2. + (3*L6*vhsr0**2)/2.
        res[4, 5] = L6*vhsi0*vhsr0
        res[5, 0] = L7*vh1r0*vhsi0
        res[5, 1] = L8*vh2rp*vhsi0
        res[5, 2] = L8*vh2r0*vhsi0
        res[5, 3] = L8*vh2i0*vhsi0
        res[5, 4] = L6*vhsi0*vhsr0
        res[5, 5] = mSsq/2. + mXsq/2. + (L7*vh1r0**2)/2. + (L8*vh2i0**2)/2. + (L8*vh2r0**2)/2. + (L8*vh2rp**2)/2. + (3*L6*vhsi0**2)/2. + (L6*vhsr0**2)/2.

        return res


