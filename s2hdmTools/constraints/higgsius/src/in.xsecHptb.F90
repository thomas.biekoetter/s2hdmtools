!---------------------------------------------------
module theoHp
!---------------------------------------------------
! This small program extracts the 13 TeV LHC cross section
! for pp -> H^+ t + X from the grid provided by the LHC Higgs
! Cross Section Working Group (YR4) in the mass ranges (145-200) GeV
! and (200-2000) GeV.
!
! This *full* version also provides the uncertainties.
!
! If you use these numbers in your publication you should cite:
! 1610.07922, 1507.02549, 1409.5615, 0906.2648, hep-ph/0312286
! as well as
! 1607.05291 (for the low mass range)
!
! Compile as
! gfortran xsecHptb_full.F90 -o xsecHptb_full
! and run as
! ./xsecHptb <THDMType> <MHp> <tanb>
! or
! ./xsecHptb -1 <MHp> <lambda_tt> <lambda_bb>
! in the case of a 2HDM with general Yukawa structure.
!
! Written by T. Stefaniak (2018-08-20)
!
! Rewritten to module for f2py by T. Biekotter (2021-05-16)
!---------------------------------------------------
 implicit none

contains

subroutine getxs(THDMType, MHp_in, tb_in, z)
 
 double precision, allocatable :: xs13data(:,:,:),t1low_xs13data(:,:,:),t2low_xs13data(:,:,:)
 
 double precision :: tb1min, tb1max, tb1sep
 double precision :: tb2min, tb2max, tb2sep
 double precision :: MHp1min, MHp1max, MHp1sep
 double precision :: MHp2min, MHp2max, MHp2sep 
 double precision :: MHp3min, MHp3max, MHp3sep 
 double precision :: MHplowmin, MHplowmax, MHplowsep  
 
 double precision :: xtmp, ytmp, ztmp
 
 integer :: Nerr = 3 ! in the high mass grid, the last entry is always == 0.
 double precision :: err1, err2, err3
 
 integer :: Ntb1, Ntb2, Ntb, NMHp1, NMHp2, NMHp3, NMHplow, NMHp, Ntot
 integer :: skip
 integer :: ios
 integer :: i,j

 double precision :: xlow,xup,xsep,ylow,yup,ysep
 integer :: xposlow,xposup,yposlow,yposup
 logical :: indata = .True.

 double precision :: x_lowdiff,x_updiff,y_lowdiff,y_updiff, z_xlow(4), z_xup(4),z(4)

 integer :: number_args
 character(LEN=100) :: temp, temp2
 integer, intent(in) :: THDMType
 double precision, intent(in) :: MHp_in, tb_in
 double precision :: tb
 double precision :: ltt_in, lbb_in, ltt, lbb, lratio, lratio_cut, ldom
 double precision :: rhoratio, rhoratio_cut
 double precision, parameter :: kappat = 0.947111
 double precision, parameter :: kappab = 0.0165456
 
 !f2py intent(in) :: THDMType, MHp_in, tb_in
 !f2py intent(out) :: z

 ltt = 0.0D0
 lbb = 0.0D0
 lratio = 0.0D0
 lratio_cut = 0.2D0
 
 rhoratio = 0.0D0
!-- Set this to a non-zero value in order to obtain an intermediate regime
!   (does not give good results at the moment) 
 rhoratio_cut = 0.0D0 
 
!-- Read arguments
 number_args = IARGC() 
 
!  if(number_args.lt.3.or.number_args.gt.4) then
!    write(*,*) 'Error: Please call this program as'
!    write(*,*) ' '
!    write(*,*) '   ./xsecHptb <2HDM-Type> <MH+> <tanb> '
!    write(*,*) ' '   
!    write(*,*) 'or'
!    write(*,*) ' '   
!    write(*,*) '   ./xsecHptb <2HDM-Type> <MH+> <lambdatt> <lambdabb> '
!    write(*,*) ' '
!    write(*,*) '(where 2HDM-Type should be -1)'
!    write(*,*) ' '
!    write(*,*) ' The output contains:'
!    write(*,*) '    13TeV XS (in pb)                      [first number]'
!    write(*,*) '    the upper scale uncertainty (in %)    [second number]'
!    write(*,*) '    the lower scale uncertainty (in %)    [third number]'
!    write(*,*) '    the PDF uncertainty (in %)            [fourth number]'
!    write(*,*) ' '
!    write(*,*) ' If MH+ >= 200 GeV, the scale+PDF uncertainty are combined.'   
!    
!    stop 'Invalid input!'
!  endif 

!   temp=""
!   call GETARG(1,temp)
!   temp2 = ""
!   temp2 = trim(temp)
!   read(temp2,*) THDMType
! 
!   temp=""
!   call GETARG(2,temp)
!   temp2 = ""
!   temp2 = trim(temp)
!   read(temp2,*) MHp_in
! 
!  if(number_args.eq.3) then
! 
!   temp=""
!   call GETARG(3,temp)
!   temp2 = ""
!   temp2 = trim(temp)
!   read(temp2,*) tb_in 
! 
! else if(number_args.eq.4) then
!   if(THDMType.ne.-1) stop 'Error: Please set 2HDM-Type = -1 for lambda input!'
!   temp=""
!   call GETARG(3,temp)
!   temp2 = ""
!   temp2 = trim(temp)
!   read(temp2,*) ltt_in
!   ltt = abs(ltt_in)
! 
!   temp=""
!   call GETARG(4,temp)
!   temp2 = ""
!   temp2 = trim(temp)
!   read(temp2,*) lbb_in 
!   lbb = abs(lbb_in)
! 
!   if(maxval( (/ lbb, ltt /) ).gt.0.0D0) then
!    lratio = (lbb - ltt)/maxval( (/ lbb, ltt /) )
!    rhoratio = (lbb*kappab - ltt*kappat)/maxval( (/ lbb*kappab, ltt*kappat /) )
! !    write(*,*) lbb, ltt, lratio
!   endif 
! 
! endif



!-- Grid specifics  
 tb1min = 0.1D0
 tb1max = 1.0D0
 tb1sep = 0.1D0
 tb2min = 1.0D0
 tb2max = 60.0D0
 tb2sep = 1.0D0 
 MHp1max = 500.0D0
 MHp1min = 200.0D0
 MHp1sep = 20.0D0
 MHp2max = 1000.0D0
 MHp2min = 500.0D0
 MHp2sep = 50.0D0
 MHp3max = 2000.0D0
 MHp3min = 1000.0D0
 MHp3sep = 100.0D0
 MHplowmin = 145.0D0
 MHplowmax = 200.0D0
 MHplowsep = 5.0D0
 
 skip =12
 
 Ntb1 = nint(  (tb1max-tb1min)/tb1sep )
 Ntb2 = nint(  (tb2max-tb2min)/tb2sep )
 
 Ntb = Ntb1 + Ntb2 + 1
 
 NMHp1 = nint( (MHp1max-MHp1min)/MHp1sep)
 NMHp2 = nint( (MHp2max-MHp2min)/MHp2sep)
 NMHp3 = nint( (MHp3max-MHp3min)/MHp3sep)
 NMHplow = nint( (MHplowmax-MHplowmin)/MHplowsep)+1
 
 NMHp = NMHp1 + NMHp2 + NMHp3 + 1

 Ntot = NMHp * Ntb

!  write(*,*) "Creating data grid of size (",NMHp,",",Ntb,")."
!  write(*,*) "Number of entries: ",Ntot

 allocate(xs13data(Nerr+1,NMHp, Ntb))
 allocate(t1low_xs13data(Nerr+1,NMHplow, Ntb),t2low_xs13data(Nerr+1,NMHplow, Ntb))
 
!-- Read from binary file, or, if not existent, create it.
 
 open(66,file = 'xsecHptb13TeV_full.binary',form='unformatted')
 read(66,iostat=ios) t1low_xs13data
 read(66,iostat=ios) t2low_xs13data 
 read(66,iostat=ios) xs13data

 if(ios.ne.0)then
!   write(*,*) "Reading from binary failed. Reading in from raw data..."
  rewind(66)
 
  open(67,file='XXXDATAPATHXXX/type1_cH_145-200_full.dat')

  do i=1,NMHplow
   do j=1,Ntb
    read(67,*) xtmp, ytmp, ztmp, err1, err2, err3
    if(abs(xtmp-(MHplowmin+(i-1)*MHplowsep)).gt.1.0D-7) then
     write(*,*) "Warning --- MHp mismatch: ", xtmp, MHplowmin+(i-1)*MHplowsep, i
    endif
    if(j.le.10) then
     if(abs(ytmp-(tb1min+(j-1)*tb1sep)).gt.1.0D-7) then
      write(*,*) "Warning --- TB(low) mismatch: ", ytmp, j
     endif    
    else
     if(abs(ytmp-(tb2min+(j-10)*tb2sep)).gt.1.0D-7) then
      write(*,*) "Warning --- TB(high) mismatch: ", ytmp, j
     endif
    endif

    t1low_xs13data(1,i,j) = ztmp
    t1low_xs13data(2,i,j) = err1
    t1low_xs13data(3,i,j) = err2
    t1low_xs13data(4,i,j) = err3
   enddo
  enddo   

  close(67)  
  write(66) t1low_xs13data

  open(67,file='XXXDATAPATHXXX/type2_cH_145-200_full.dat')

  do i=1,NMHplow
   do j=1,Ntb
    read(67,*) xtmp, ytmp, ztmp, err1, err2, err3
    if(abs(xtmp-(MHplowmin+(i-1)*MHplowsep)).gt.1.0D-7) then
     write(*,*) "Warning --- MHp mismatch: ", xtmp, MHplowmin+(i-1)*MHplowsep, i
    endif
    if(j.le.10) then
     if(abs(ytmp-(tb1min+(j-1)*tb1sep)).gt.1.0D-7) then
      write(*,*) "Warning --- TB(low) mismatch: ", ytmp, j
     endif    
    else
     if(abs(ytmp-(tb2min+(j-10)*tb2sep)).gt.1.0D-7) then
      write(*,*) "Warning --- TB(high) mismatch: ", ytmp, j
     endif
    endif

    t2low_xs13data(1,i,j) = ztmp
    t2low_xs13data(2,i,j) = err1
    t2low_xs13data(3,i,j) = err2
    t2low_xs13data(4,i,j) = err3

   enddo
  enddo   

  close(67)  
  write(66) t2low_xs13data


  open(67,file='XXXDATAPATHXXX/xsec_13TeV_tHp_2016_2_5.dat')
 
  do i=1,skip
   read(67,*)
  enddo 

  do i=1,NMHp
   do j=1,Ntb
    read(67,*) xtmp, ytmp, ztmp, err1, err2
    if(i.le.NMHp1+1) then
     if(abs(xtmp-(MHp1min+(i-1)*MHp1sep)).gt.1.0D-7) then
      write(*,*) "Warning --- MHp mismatch: ", xtmp, MHp1min+(i-1)*MHp1sep, i
     endif
    else if(i.le.NMHp1+NMHp2+1) then 
     if(abs(xtmp-(MHp2min+(i-NMHp1-1)*MHp2sep)).gt.1.0D-7) then
      write(*,*) "Warning --- MHp mismatch: ", xtmp, (MHp2min+(i-NMHp1-1)*MHp2sep), i
     endif
    else
     if(abs(xtmp-(MHp3min+(i-NMHp1-NMHp2-1)*MHp3sep)).gt.1.0D-7) then
      write(*,*) "Warning --- MHp mismatch: ", xtmp, MHp3min+(i-NMHp1-NMHp2-1)*MHp3sep, i
     endif
    endif 
    if(j.le.10) then
     if(abs(ytmp-(tb1min+(j-1)*tb1sep)).gt.1.0D-7) then
      write(*,*) "Warning --- TB(low) mismatch: ", ytmp, j
     endif    
    else
     if(abs(ytmp-(tb2min+(j-10)*tb2sep)).gt.1.0D-7) then
      write(*,*) "Warning --- TB(high) mismatch: ", ytmp, j
     endif
    endif

    xs13data(1,i,j) = ztmp  
    xs13data(2,i,j) = err1
    xs13data(3,i,j) = err2  
    xs13data(4,i,j) = 0.0D0
   enddo
  enddo   

  close(67)
  
  write(66) xs13data
  close(66)
 endif

!-- Locate the input parameters in the grid
 tb=tb_in
 if(THDMType.eq.2.or.THDMType.eq.3) then
  if(MHp_in.ge.200.0D0) then
   call locate_data(MHp_in,tb,xposlow,xlow,xposup,xup,xsep,yposlow,ylow,yposup,yup,ysep,indata)
  endif   
 else if(THDMType.eq.1.or.THDMType.eq.4) then
  if(MHp_in.ge.200.0D0) then  
   tb=1.0D0
   call locate_data(MHp_in,tb,xposlow,xlow,xposup,xup,xsep,yposlow,ylow,yposup,yup,ysep,indata)
  endif
 else if(THDMType.eq.-1) then
!   if(MHp_in.ge.200.0D0) then
!    if(lratio.lt.-lratio_cut) then
   if(rhoratio.lt.-rhoratio_cut) then
! ltt dominates
   ldom=ltt
   tb=0.1D0
!    else if(lratio.gt.lratio_cut) then
   else if(rhoratio.gt.rhoratio_cut) then   
! lbb dominates
   ldom=lbb
   tb=60.0D0
   else
! ltt ~ lbb
!    ldom=maxval((/ ltt, lbb /))
!    ldom=(ltt + lbb)/2.0D0
   ldom=lbb
   tb=1.0D0
!    tb=ldom
   endif
!    write(*,*) ldom, tb, rhoratio
   if(MHp_in.ge.200.0D0) then
    call locate_data(MHp_in,tb,xposlow,xlow,xposup,xup,xsep,yposlow,ylow,yposup,yup,ysep,indata) 
   endif   
 else
  stop "2HDM Type is unsupported. No output."
 endif 

 if(MHp_in.lt.200.0D0) then
  call locate_data_low(MHp_in,tb,xposlow,xlow,xposup,xup,xsep,yposlow,ylow,yposup,yup,ysep,indata)  
 endif 

 
 !  write(*,*) "Locate 270,9.5..."
!  write(*,*) "Locate ", MHp_in, tb 
!  write(*,*) xposlow, xposup, xsep, xlow, xup
!  write(*,*) yposlow, yposup, ysep, ylow, yup
!  
!  write(*,*) "xs13data(",xposlow,",",yposlow,") = ",xs13data(xposlow,yposlow)
!  write(*,*) "xs13data(",xposlow,",",yposup,") = ",xs13data(xposlow,yposup) 
!  write(*,*) "xs13data(",xposup,",",yposlow,") = ",xs13data(xposup,yposlow) 
!  write(*,*) "xs13data(",xposup,",",yposup,") = ",xs13data(xposup,yposup) 


!-- Do bilinear interpolation
 
  if(indata) then
   y_updiff = (yup - tb)/ysep
   y_lowdiff = (tb - ylow)/ysep
   x_updiff = (xup - MHp_in)/xsep
   x_lowdiff = (MHp_in - xlow)/xsep
   if(y_updiff.ne.0.and.y_lowdiff.ne.0) then
    if(MHp_in.ge.200.0D0) then
     do i=1,4
     z_xlow(i) = xs13data(i,xposlow,yposup)*y_lowdiff + xs13data(i,xposlow,yposlow)*y_updiff
     z_xup(i) = xs13data(i,xposup,yposup)*y_lowdiff + xs13data(i,xposup,yposlow)*y_updiff
     enddo
    else
	 if(THDMType.eq.2.or.THDMType.eq.3.or.THDMType.eq.-1) then
     do i=1,4	 
      z_xlow(i) = t2low_xs13data(i,xposlow,yposup)*y_lowdiff + t2low_xs13data(i,xposlow,yposlow)*y_updiff
      z_xup(i) = t2low_xs13data(i,xposup,yposup)*y_lowdiff + t2low_xs13data(i,xposup,yposlow)*y_updiff
     enddo 
     else
     do i=1,4
      z_xlow(i) = t1low_xs13data(i,xposlow,yposup)*y_lowdiff + t1low_xs13data(i,xposlow,yposlow)*y_updiff
      z_xup(i) = t1low_xs13data(i,xposup,yposup)*y_lowdiff + t1low_xs13data(i,xposup,yposlow)*y_updiff
     enddo 
     endif 
    endif         
   else
    if(MHp_in.ge.200.0D0) then   
    do i = 1,4
     z_xlow(i) = xs13data(i,xposlow,yposup)
     z_xup(i) = xs13data(i,xposup,yposup)
    enddo 
    else
	 if(THDMType.eq.2.or.THDMType.eq.3.or.THDMType.eq.-1) then
     do i = 1,4
      z_xlow(i) = t2low_xs13data(i,xposlow,yposup)
      z_xup(i) = t2low_xs13data(i,xposup,yposup)
     enddo
     else
     do i = 1,4
      z_xlow(i) = t1low_xs13data(i,xposlow,yposup)
      z_xup(i) = t1low_xs13data(i,xposup,yposup)	      
     enddo      
     endif
    endif        
   endif
   do i=1,4
   if(x_updiff.ne.0.and.x_lowdiff.ne.0) then   
    z(i) = z_xlow(i)*x_updiff + z_xup(i)*x_lowdiff
   else
    z(i) = z_xup(i)
   endif
   enddo

! !-- Adjust to the THDM Type   
!    if(THDMType.eq.2.or.THDMType.eq.3) then
!      write(*,*) z
!    else if(THDMType.eq.1.or.THDMType.eq.4) then
!     if(MHp_in.ge.200.0D0) then
! !-- Rescale result at tanb=1 from grid by cotb^2
!      write(*,*) z(1)/tb_in**2, z(2), z(3), z(4)
!     else
!      write(*,*) z
!     endif 
!    else if(THDMType.eq.-1) then
! !     if(lratio.lt.-lratio_cut) then
!     if(rhoratio.lt.-rhoratio_cut) then
!      write(*,*) z(1)*(ldom*tb)**2, z(2), z(3), z(4)
!     else
!      write(*,*) z*(ldom/tb)**2, z(2), z(3), z(4)
!     endif
!    endif 
  else   
!    write(*,*) 0.0D0, 0.0D0, 0.0D0, 0.0D0 
  endif
   

 contains
 
 subroutine locate_data(x,y,xposlow,xlow,xposup,xup,xsep,yposlow,ylow,yposup,yup,ysep,indata)
  implicit none
  double precision, intent(in) :: x,y
  double precision, intent(out) :: xlow,xup,xsep,ylow,yup,ysep
  integer, intent(out) :: xposlow,xposup,yposlow,yposup
  logical, intent(out) :: indata
 
  indata = .True.
 
  if(x.ge.MHp1min.and.x.le.MHp3max) then
   if(x.le.MHp1max) then
    xposlow = floor( (x-MHp1min)/MHp1sep ) +1 
    xposup = ceiling( (x-MHp1min)/MHp1sep ) +1
    xsep = MHp1sep
    xlow = MHp1min + (xposlow-1) * xsep
    xup = MHp1min + (xposup-1) * xsep    
   else if(x.le.MHp2max) then
    xposlow = floor( (x-MHp2min)/MHp2sep ) +1 + NMHp1
    xposup = ceiling( (x-MHp2min)/MHp2sep ) +1 + NMHp1
    xsep = MHp2sep
    xlow = MHp2min + (xposlow-1-NMHp1) * xsep
    xup = MHp2min + (xposup-1-NMHp1) * xsep        
   else
    xposlow = floor( (x-MHp3min)/MHp3sep ) +1 + NMHp1 + NMHp2
    xposup = ceiling( (x-MHp3min)/MHp3sep ) +1 + NMHp1 + NMHp2
    xsep = MHp3sep
    xlow = MHp3min + (xposlow-1-NMHp1-NMHp2) * xsep
    xup = MHp3min + (xposup-1-NMHp1-NMHp2) * xsep     
   endif
  else
   indata = .False.
  endif
  
  if(y.ge.tb1min.and.y.le.tb2max) then
   if(y.le.tb1max) then
    yposlow = floor( (y-tb1min)/tb1sep ) + 1
    yposup = ceiling( (y-tb1min)/tb1sep ) + 1
    ysep = tb1sep
    ylow = tb1min + (yposlow-1) * ysep
    yup = tb1min + (yposup-1) * ysep  
   else
    yposlow = floor( (y-tb2min)/tb2sep ) + 1 + Ntb1
    yposup = ceiling( (y-tb2min)/tb2sep ) + 1 + Ntb1
    ysep = tb2sep
    ylow = tb2min + (yposlow-1-Ntb1) * ysep
    yup = tb2min + (yposup-1-Ntb1) * ysep  
   endif
  else
   indata = .False.
  endif  
 
 end subroutine locate_data

 subroutine locate_data_low(x,y,xposlow,xlow,xposup,xup,xsep,yposlow,ylow,yposup,yup,ysep,indata)
  implicit none
  double precision, intent(in) :: x,y
  double precision, intent(out) :: xlow,xup,xsep,ylow,yup,ysep
  integer, intent(out) :: xposlow,xposup,yposlow,yposup
  logical, intent(out) :: indata
 
   indata = .True.
 
  if(x.ge.MHplowmin.and.x.lt.MHplowmax) then
    xposlow = floor( (x-MHplowmin)/MHplowsep ) +1 
    xposup = ceiling( (x-MHplowmin)/MHplowsep ) +1
    xsep = MHplowsep
    xlow = MHplowmin + (xposlow-1) * xsep
    xup = MHplowmin + (xposup-1) * xsep    
  else
   indata = .False.
  endif
  
  if(y.ge.tb1min.and.y.le.tb2max) then
   if(y.le.tb1max) then
    yposlow = floor( (y-tb1min)/tb1sep ) + 1
    yposup = ceiling( (y-tb1min)/tb1sep ) + 1
    ysep = tb1sep
    ylow = tb1min + (yposlow-1) * ysep
    yup = tb1min + (yposup-1) * ysep  
   else
    yposlow = floor( (y-tb2min)/tb2sep ) + 1 + Ntb1
    yposup = ceiling( (y-tb2min)/tb2sep ) + 1 + Ntb1
    ysep = tb2sep
    ylow = tb2min + (yposlow-1-Ntb1) * ysep
    yup = tb2min + (yposup-1-Ntb1) * ysep  
   endif
  else
   indata = .False.
  endif  
 
 end subroutine locate_data_low

end subroutine getxs

end module
