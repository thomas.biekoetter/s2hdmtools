import sys


def main(argv):

    project_dir = argv[0]

    rplc = 'XXXDATAPATHXXX'

    with open('in.xsecHptb.F90') as f:
        orig = f.readlines()

    res = []

    for ln in orig:

        if rplc in ln:
            new = ln.replace(rplc, project_dir + '/data')
        else:
            new = ln

        res.append(new)

    with open('xsecHptb.F90', 'w') as f:
        f.writelines(res)


if __name__ == "__main__":
   main(sys.argv[1:])
