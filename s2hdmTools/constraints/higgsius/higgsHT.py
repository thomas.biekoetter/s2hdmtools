import os
import numpy as np

from Higgs import Predictions
from Higgs import predictions as HP
from Higgs import Bounds
from Higgs import Signals

from s2hdmTools.constraints.higgsius.hbdataPath import HB_DATA_PATH
from s2hdmTools.constraints.higgsius.hsdataPath import HS_DATA_PATH
from s2hdmTools.constraints.higgsius.standardModel.higgs \
    import BR
from s2hdmTools.constraints.higgsius.theoHp import theohp


mH125 = 125.09


class HiggsHT:
    """
    The base class for the interface to
    HiggsTools.

    An instance of this class has to be
    created only once in a python session.
    Afterwards, as many points as desired
    can be checked. At the end of the
    python session HiggsBounds should
    be closed using the function `adios`.
    """

    def __init__(self, delM=2.):
        """
        Initialize an instance of the
        HiggsTools interface.

        The mass uncertainty of the Higgs
        bosons is set to 2 GeV by default.
        """

        self.delM = delM

        self.pred = Predictions()
        self.bounds = Bounds(HB_DATA_PATH)
        self.signals = Signals(HS_DATA_PATH)
        self.hs_numobs = self.signals.observableCount()

        hCP = HP.CP(1)
        ACP = HP.CP(-1)
        self.h1 = self.pred.addParticle(HP.NeutralScalar("h1", hCP))
        self.h2 = self.pred.addParticle(HP.NeutralScalar("h2", hCP))
        self.h3 = self.pred.addParticle(HP.NeutralScalar("h3", hCP))
        self.Ah = self.pred.addParticle(HP.NeutralScalar("Ah", ACP))
        self.Hpm = self.pred.addParticle(HP.ChargedScalar("Hp"))

        self.h1.setMassUnc(self.delM)
        self.h2.setMassUnc(self.delM)
        self.h3.setMassUnc(self.delM)
        self.Ah.setMassUnc(self.delM)
        self.Hpm.setMassUnc(self.delM)

        self.brSM = BR()

        self.get_higgssignals_sm()

    def get_higgssignals_sm(self):
        sm = Predictions()
        smH = sm.addParticle(HP.NeutralScalar("h", "even"))
        smH.setMass(mH125)
        smH.setMassUnc(self.delM)
        HP.effectiveCouplingInput(
            smH,
            HP.smLikeEffCouplings,
            reference="SMHiggsEW")
        self.SMchisq = self.signals(sm)
        self.SMcontrs = {m.reference(): m(sm) for m in self.signals.measurements()}

    def check_point(self, pt):
        """
        Check the parameter point against
        collider constraints.

        The results are stored in the dictionaries:
        ```
        pt.HT
        ```
        The dictionary contains the details
        of the HiggsBounds and HiggsSignals tests.

        Args:
            pt (ParamPoint): The parameter point
                to be checked.
        """

        try:
            pt.b_H1
        except AttributeError:
            pt.calculate_branching_ratios()

        pt.effCpls = {}

        self.h1.setMass(pt.mH1)
        self.h2.setMass(pt.mH2)
        self.h3.setMass(pt.mH3)
        self.Ah.setMass(pt.mA)
        self.Hpm.setMass(pt.mHp)

        ghjss_s = np.zeros(shape=(4, ))
        ghjss_p = np.zeros(shape=(4, ))
        ghjcc_s = np.zeros(shape=(4, ))
        ghjcc_p = np.zeros(shape=(4, ))
        ghjbb_s = self.calc_eff_bbh_cpls(pt)
        ghjbb_p = self.calc_eff_bbA_cpls(pt)
        ghjtt_s = self.calc_eff_tth_cpls(pt)
        ghjtt_p = self.calc_eff_ttA_cpls(pt)
        ghjmumu_s = np.zeros(shape=(4, ))
        ghjmumu_p = np.zeros(shape=(4, ))
        ghjtautau_s = np.zeros(shape=(4, ))
        ghjtautau_p = np.zeros(shape=(4, ))
        ghjWW = self.calc_eff_VV_cpls(pt)
        ghjZZ = ghjWW
        ghjZga = np.zeros(shape=(4, ))
        ghjgaga = np.zeros(shape=(4, ))
        ghjhiZ = self.calc_eff_hZA_cpls(pt)
        coupsh1 = HP.NeutralEffectiveCouplings()
        coupsh2 = HP.NeutralEffectiveCouplings()
        coupsh3 = HP.NeutralEffectiveCouplings()
        coupsAh = HP.NeutralEffectiveCouplings()

        coupsh1.tt = ghjtt_s[0]
        coupsh2.tt = ghjtt_s[1]
        coupsh3.tt = ghjtt_s[2]
        coupsAh.tt = 1j * ghjtt_p[3]

        coupsh1.bb = ghjbb_s[0]
        coupsh2.bb = ghjbb_s[1]
        coupsh3.bb = ghjbb_s[2]
        coupsAh.bb = 1j * ghjbb_p[3]

        coupsh1.tautau = ghjtautau_s[0]
        coupsh2.tautau = ghjtautau_s[1]
        coupsh3.tautau = ghjtautau_s[2]
        coupsAh.tautau = 1j * ghjtautau_p[3]

        coupsh1.gamgam = ghjgaga[0]
        coupsh2.gamgam = ghjgaga[1]
        coupsh3.gamgam = ghjgaga[2]
        coupsAh.gamgam = ghjgaga[3]

        coupsh1.ZZ = ghjZZ[0]
        coupsh2.ZZ = ghjZZ[1]
        coupsh3.ZZ = ghjZZ[2]
        coupsAh.ZZ = ghjZZ[3]

        coupsh1.WW = ghjWW[0]
        coupsh2.WW = ghjWW[1]
        coupsh3.WW = ghjWW[2]
        coupsAh.WW = ghjWW[3]

        HP.effectiveCouplingInput(self.h1, coupsh1)
        HP.effectiveCouplingInput(self.h2, coupsh2)
        HP.effectiveCouplingInput(self.h3, coupsh3)
        HP.effectiveCouplingInput(self.Ah, coupsAh)

        CS_Hpjtb = 2. * theohp.getxs(
            pt.yuktype,
            pt.mHp,
            pt.tb)[0]
        self.Hpm.setCxn('LHC13', 'Hpmtb', CS_Hpjtb)

        wh1 = pt.wTot['H1']
        wh2 = pt.wTot['H2']
        wh3 = pt.wTot['H3']
        wAh = pt.wTot['A']
        wHpm = pt.wTot['Hp']
        self.h1.setDecayWidth('tt', pt.b_H1['tt'] * wh1)
        self.h2.setDecayWidth('tt', pt.b_H2['tt'] * wh2)
        self.h3.setDecayWidth('tt', pt.b_H3['tt'] * wh3)
        self.Ah.setDecayWidth('tt', pt.b_A['tt'] * wAh)
        self.h1.setDecayWidth('bb', pt.b_H1['bb'] * wh1)
        self.h2.setDecayWidth('bb', pt.b_H2['bb'] * wh2)
        self.h3.setDecayWidth('bb', pt.b_H3['bb'] * wh3)
        self.Ah.setDecayWidth('bb', pt.b_A['bb'] * wAh)
        self.h1.setDecayWidth('cc', pt.b_H1['cc'] * wh1)
        self.h2.setDecayWidth('cc', pt.b_H2['cc'] * wh2)
        self.h3.setDecayWidth('cc', pt.b_H3['cc'] * wh3)
        self.Ah.setDecayWidth('cc', pt.b_A['cc'] * wAh)
        self.h1.setDecayWidth('ss', pt.b_H1['ss'] * wh1)
        self.h2.setDecayWidth('ss', pt.b_H2['ss'] * wh2)
        self.h3.setDecayWidth('ss', pt.b_H3['ss'] * wh3)
        self.Ah.setDecayWidth('ss', pt.b_A['ss'] * wAh)
        self.h1.setDecayWidth('tautau', pt.b_H1['ll'] * wh1)
        self.h2.setDecayWidth('tautau', pt.b_H2['ll'] * wh2)
        self.h3.setDecayWidth('tautau', pt.b_H3['ll'] * wh3)
        self.Ah.setDecayWidth('tautau', pt.b_A['ll'] * wAh)
        self.h1.setDecayWidth('mumu', pt.b_H1['mm'] * wh1)
        self.h2.setDecayWidth('mumu', pt.b_H2['mm'] * wh2)
        self.h3.setDecayWidth('mumu', pt.b_H3['mm'] * wh3)
        self.Ah.setDecayWidth('mumu', pt.b_A['mm'] * wAh)
        self.h1.setDecayWidth('gg', pt.b_H1['gg'] * wh1)
        self.h2.setDecayWidth('gg', pt.b_H2['gg'] * wh2)
        self.h3.setDecayWidth('gg', pt.b_H3['gg'] * wh3)
        self.Ah.setDecayWidth('gg', pt.b_A['gg'] * wAh)

        self.h1.setDecayWidth('gamgam', pt.b_H1['yy'] * wh1)
        self.h2.setDecayWidth('gamgam', pt.b_H2['yy'] * wh2)
        self.h3.setDecayWidth('gamgam', pt.b_H3['yy'] * wh3)
        self.Ah.setDecayWidth('gamgam', pt.b_A['yy'] * wAh)

        self.h1.setDecayWidth('ZZ', pt.b_H1['ZZ'] * wh1)
        self.h2.setDecayWidth('ZZ', pt.b_H2['ZZ'] * wh2)
        self.h3.setDecayWidth('ZZ', pt.b_H3['ZZ'] * wh3)
        self.Ah.setDecayWidth('ZZ', 0.0)

        self.h1.setDecayWidth('WW', pt.b_H1['WW'] * wh1)
        self.h2.setDecayWidth('WW', pt.b_H2['WW'] * wh2)
        self.h3.setDecayWidth('WW', pt.b_H3['WW'] * wh3)
        self.Ah.setDecayWidth('WW', 0.0)

        self.h1.setDecayWidth('Zgam', pt.b_H1['Zy'] * wh1)
        self.h2.setDecayWidth('Zgam', pt.b_H2['Zy'] * wh2)
        self.h3.setDecayWidth('Zgam', pt.b_H3['Zy'] * wh3)
        self.Ah.setDecayWidth('Zgam', 0.0)

        self.h2.setDecayWidth('h1', 'h1', pt.b_H2['H1H1'] * wh2)
        self.h3.setDecayWidth('h1', 'h1', pt.b_H3['H1H1'] * wh3)
        self.h3.setDecayWidth('h1', 'h2', pt.b_H3['H1H2'] * wh3)
        self.h3.setDecayWidth('h2', 'h2', pt.b_H3['H2H2'] * wh3)

        self.h1.setDecayWidth('Ah', 'Ah', pt.b_H1['AA'] * wh1)
        self.h2.setDecayWidth('Ah', 'Ah', pt.b_H2['AA'] * wh2)
        self.h3.setDecayWidth('Ah', 'Ah', pt.b_H3['AA'] * wh3)

        self.Ah.setDecayWidth('Z', 'h1', pt.b_A['ZH1'] * wAh)
        self.Ah.setDecayWidth('Z', 'h2', pt.b_A['ZH2'] * wAh)
        self.Ah.setDecayWidth('Z', 'h3', pt.b_A['ZH3'] * wAh)

        self.h1.setDecayWidth('Z', 'Ah', pt.b_H1['ZA'] * wh1)
        self.h2.setDecayWidth('Z', 'Ah', pt.b_H2['ZA'] * wh2)
        self.h3.setDecayWidth('Z', 'Ah', pt.b_H3['ZA'] * wh3)

        self.h1.setDecayWidth('W', 'Hpm', pt.b_H1['WpHm'] * wh1)
        self.h2.setDecayWidth('W', 'Hpm', pt.b_H2['WpHm'] * wh2)
        self.h3.setDecayWidth('W', 'Hpm', pt.b_H3['WpHm'] * wh3)
        self.Ah.setDecayWidth('W', 'Hpm', pt.b_A['WpHm'] * wAh)

        self.h1.setDecayWidth('Hpm', 'Hpm', pt.b_H1['HpHm'] * wh1)
        self.h2.setDecayWidth('Hpm', 'Hpm', pt.b_H2['HpHm'] * wh2)
        self.h3.setDecayWidth('Hpm', 'Hpm', pt.b_H3['HpHm'] * wh3)

        self.h1.setDecayWidth('directInv', pt.b_H1['XX'] * wh1)
        self.h2.setDecayWidth('directInv', pt.b_H2['XX'] * wh2)
        self.h3.setDecayWidth('directInv', pt.b_H3['XX'] * wh3)
        self.Ah.setDecayWidth('directInv', 0.0)

        self.Hpm.setDecayWidth('tb', pt.b_Hp['tb'] * wHpm)
        self.Hpm.setDecayWidth('t', 's', pt.b_Hp['ts'] * wHpm)
        self.Hpm.setDecayWidth('t', 'd', pt.b_Hp['td'] * wHpm)
        self.Hpm.setDecayWidth('cs', pt.b_Hp['cs'] * wHpm)
        self.Hpm.setDecayWidth('cb', pt.b_Hp['cb'] * wHpm)
        self.Hpm.setDecayWidth('cd', pt.b_Hp['cd'] * wHpm)
        self.Hpm.setDecayWidth('ub', pt.b_Hp['ub'] * wHpm)
        self.Hpm.setDecayWidth('us', pt.b_Hp['us'] * wHpm)
        self.Hpm.setDecayWidth('taunu', pt.b_Hp['ln'] * wHpm)
        self.Hpm.setDecayWidth('mu', 'nu', pt.b_Hp['mn'] * wHpm)
        self.Hpm.setDecayWidth('W', 'h1', pt.b_Hp['WH1'] * wHpm)
        self.Hpm.setDecayWidth('W', 'h2', pt.b_Hp['WH2'] * wHpm)
        self.Hpm.setDecayWidth('W', 'h3', pt.b_Hp['WH3'] * wHpm)
        self.Hpm.setDecayWidth('W', 'Ah', pt.b_Hp['WA'] * wHpm)

        if abs(self.h1.totalWidth() - wh1) > 1e-4:
            print(self.h1.totalWidth(), wh1)
            raise RuntimeError("Problem with width of h1.")
        if abs(self.h2.totalWidth() - wh2) > 1e-4:
            print(self.h2.totalWidth(), wh2)
            raise RuntimeError("Problem with width of h2.")
        if abs(self.h3.totalWidth() - wh3) > 1e-4:
            print(self.h3.totalWidth(), wh3)
            raise RuntimeError("Problem with width of h3.")
        if abs(self.Ah.totalWidth() - wAh) > 1e-4:
            print(self.Ah.totalWidth(), wAh)
            raise RuntimeError("Problem with width of A.")
        if abs(self.Hpm.totalWidth() - wHpm) > 1e-4:
            print(self.Hpm.totalWidth(), wHpm)
            raise RuntimeError("Problem with width of Hpm.")
        self.h1.setTotalWidth(wh1)
        self.h2.setTotalWidth(wh2)
        self.h3.setTotalWidth(wh3)
        self.Ah.setTotalWidth(wAh)
        self.Hpm.setTotalWidth(wHpm)

        res = self.bounds(self.pred)
        pt.hb_result = res
        HB = {}
        HS = {}
        XS = {}
        pt.HT = {'HB': HB, 'HS': HS, 'XS': XS}
        particle_results = []
        try:
            HB['expratio_H1'] = res.selectedLimits['h1'].expRatio()
            HB['obsratio_H1'] = res.selectedLimits['h1'].obsRatio()
            HB['channel_H1'] = res.selectedLimits['h1'].limit().citeKey()
            HB['channel_H1_ref'] = res.selectedLimits['h1'].limit().reference()
            HB['channel_H1_descr'] = res.selectedLimits['h1'].limit().__str__()
            if HB['obsratio_H1'] < 1:
                HB['result_H1'] = 1
            else:
                HB['result_H1'] = 0
            particle_results.append(HB['result_H1'])
        except KeyError:
            pass
        try:
            HB['expratio_H2'] = res.selectedLimits['h2'].expRatio()
            HB['obsratio_H2'] = res.selectedLimits['h2'].obsRatio()
            HB['channel_H2'] = res.selectedLimits['h2'].limit().citeKey()
            HB['channel_H2_ref'] = res.selectedLimits['h2'].limit().reference()
            HB['channel_H2_descr'] = res.selectedLimits['h2'].limit().__str__()
            if HB['obsratio_H2'] < 1:
                HB['result_H2'] = 1
            else:
                HB['result_H2'] = 0
            particle_results.append(HB['result_H2'])
        except KeyError:
            pass
        try:
            HB['expratio_H3'] = res.selectedLimits['h3'].expRatio()
            HB['obsratio_H3'] = res.selectedLimits['h3'].obsRatio()
            HB['channel_H3'] = res.selectedLimits['h3'].limit().citeKey()
            HB['channel_H3_ref'] = res.selectedLimits['h3'].limit().reference()
            HB['channel_H3_descr'] = res.selectedLimits['h3'].limit().__str__()
            if HB['obsratio_H3'] < 1:
                HB['result_H3'] = 1
            else:
                HB['result_H3'] = 0
            particle_results.append(HB['result_H3'])
        except KeyError:
            pass
        try:
            HB['expratio_A'] = res.selectedLimits['Ah'].expRatio()
            HB['obsratio_A'] = res.selectedLimits['Ah'].obsRatio()
            HB['channel_A'] = res.selectedLimits['Ah'].limit().citeKey()
            HB['channel_A_ref'] = res.selectedLimits['Ah'].limit().reference()
            HB['channel_A_descr'] = res.selectedLimits['Ah'].limit().__str__()
            if HB['obsratio_A'] < 1:
                HB['result_A'] = 1
            else:
                HB['result_A'] = 0
            particle_results.append(HB['result_A'])
        except KeyError:
            pass
        try:
            HB['expratio_Hp'] = res.selectedLimits['Hp'].expRatio()
            HB['obsratio_Hp'] = res.selectedLimits['Hp'].obsRatio()
            HB['channel_Hp'] = res.selectedLimits['Hp'].limit().citeKey()
            HB['channel_Hp_ref'] = res.selectedLimits['Hp'].limit().reference()
            HB['channel_Hp_descr'] = res.selectedLimits['Hp'].limit().__str__()
            if HB['obsratio_Hp'] < 1:
                HB['result_Hp'] = 1
            else:
                HB['result_Hp'] = 0
            particle_results.append(HB['result_Hp'])
        except KeyError:
            pass
        if 0 in particle_results:
            HB['result'] = 0
        else:
            HB['result'] = 1

        chisq = self.signals(self.pred)
        contrs = {m.reference(): m(self.pred) for m in self.signals.measurements()}
        HS['Chisq'] = chisq
        HS['ChisqSM'] = self.SMchisq
        HS['nobs'] = self.hs_numobs
        diffsm = {k: [v, v - self.SMcontrs[k]] for k, v in contrs.items()}
        for k, v in diffsm.items():
            HS[k] = {'Chisq': v[0], 'DeltaChisq': v[1]}

        XS['gg_H1'] = self.h1.cxn('LHC13', 'ggH')
        XS['gg_H2'] = self.h2.cxn('LHC13', 'ggH')
        XS['gg_H3'] = self.h3.cxn('LHC13', 'ggH')
        XS['gg_A'] = self.Ah.cxn('LHC13', 'ggH')
        XS['ttH_H1'] = self.h1.cxn('LHC13', 'Htt')
        XS['ttH_H2'] = self.h2.cxn('LHC13', 'Htt')
        XS['ttH_H3'] = self.h3.cxn('LHC13', 'Htt')
        XS['ttH_A'] = self.Ah.cxn('LHC13', 'Htt')
        XS['bb_H1'] = self.h1.cxn('LHC13', 'bbH')
        XS['bb_H2'] = self.h2.cxn('LHC13', 'bbH')
        XS['bb_H3'] = self.h3.cxn('LHC13', 'bbH')
        XS['bb_A'] = self.Ah.cxn('LHC13', 'bbH')
        XS['tH_H1'] = self.h1.cxn('LHC13', 'Ht')
        XS['tH_H2'] = self.h2.cxn('LHC13', 'Ht')
        XS['tH_H3'] = self.h3.cxn('LHC13', 'Ht')
        XS['tH_A'] = self.Ah.cxn('LHC13', 'Ht')
        XS['vbf_H1'] = self.h1.cxn('LHC13', 'vbfH')
        XS['vbf_H2'] = self.h2.cxn('LHC13', 'vbfH')
        XS['vbf_H3'] = self.h3.cxn('LHC13', 'vbfH')
        XS['tb_Hp'] = self.Hpm.cxn('LHC13', 'Hpmtb')
        XS['ZH_H1'] = self.h1.cxn('LHC13', 'HZ')
        XS['ZH_H2'] = self.h2.cxn('LHC13', 'HZ')
        XS['ZH_H3'] = self.h3.cxn('LHC13', 'HZ')
        XS['ZH_A'] = self.Ah.cxn('LHC13', 'HZ')
        XS['WH_H1'] = self.h1.cxn('LHC13', 'HW')
        XS['WH_H2'] = self.h2.cxn('LHC13', 'HW')
        XS['WH_H3'] = self.h3.cxn('LHC13', 'HW')
        XS['WH_A'] = self.Ah.cxn('LHC13', 'HW')

    def calc_eff_bbh_cpls(self, pt):
        cpls = np.zeros(shape=(4, ))
        for i in range(0, 3):
            if pt.yuktype == 1:
                cpls[i] = pt.R[i, 1] / pt.sb
            elif pt.yuktype == 2:
                cpls[i] = pt.R[i, 0] / pt.cb
            elif pt.yuktype == 3:
                cpls[i] = pt.R[i, 1] / pt.sb
            elif pt.yuktype == 4:
                cpls[i] = pt.R[i, 0] / pt.cb
        pt.effCpls['Hdd'] = cpls[0:3]
        return cpls

    def calc_eff_bbA_cpls(self, pt):
        cpls = np.zeros(shape=(4, ))
        # See https://www.itp.kit.edu/~rauch/Teaching/WS1415_BSMHiggs/bsm.pdf
        if pt.yuktype == 1:
            cpls[3] = -1.0 / pt.tb
        elif pt.yuktype == 2:
            cpls[3] = pt.tb
        elif pt.yuktype == 3:
            cpls[3] = -1.0 / pt.tb
        elif pt.yuktype == 4:
            cpls[3] = pt.tb
        pt.effCpls['Add'] = cpls[3]
        return cpls

    def calc_eff_tth_cpls(self, pt):
        cpls = np.zeros(shape=(4, ))
        for i in range(0, 3):
            cpls[i] = pt.R[i, 1] / pt.sb
        pt.effCpls['Huu'] = cpls[0:3]
        return cpls

    def calc_eff_ttA_cpls(self, pt):
        cpls = np.zeros(shape=(4, ))
        # See https://www.itp.kit.edu/~rauch/Teaching/WS1415_BSMHiggs/bsm.pdf
        cpls[3] = 1.0 / pt.tb
        pt.effCpls['Auu'] = cpls[3]
        return cpls

    def calc_eff_VV_cpls(self, pt):
        cpls = np.zeros(shape=(4, ))
        for i in range(0, 3):
            cpls[i] = pt.cb * pt.R[i, 0] + pt.sb * pt.R[i, 1]
        pt.effCpls['HVV'] = cpls[0:3]
        return cpls

    def calc_eff_hZA_cpls(self, pt):
        cpls = np.zeros(shape=(4, 4))
        a1 = pt.al1
        a2 = pt.al2
        a3 = pt.al3
        b = np.arctan(pt.tb)
        c = np.cos
        s = np.sin
        # See scrutinity paper
        cpls[3, 0] = -c(a2) * s(b - a1)
        cpls[0, 3] = cpls[3, 0]
        cpls[3, 1] = s(b - a1) * s(a2) * s(a3) + \
            c(a3) * c(b - a1)
        cpls[1, 3] = cpls[3, 1]
        cpls[3, 2] = c(a3) * s(b - a1) * s(a2) - \
            s(a3) * c(b - a1)
        cpls[2, 3] = cpls[3, 2]
        return cpls
