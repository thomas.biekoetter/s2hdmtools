from scipy.interpolate import interp1d
import numpy as np
import os


# class XS:
# 
#     def __init__(self):
#         self.__create_interpolations()
# 
#     def __create_interpolations(self):
#         this_dir, this_filename = os.path.split(__file__)
#         df = pd.read_csv(
#             this_dir +
#             "/Data/Higgs/XS/BSM_XS_13_ggHVBFbbH_YR4update.dat",
#             sep=r"\s+", header=None)
#         mh = df[0].to_numpy()
#         ggh = df[1].to_numpy()
#         vbf = df[2].to_numpy()
#         bbh = df[3].to_numpy()
#         self.__f_ggh_13TeV = interp1d(mh, ggh)
#         self.__f_vbf_13TeV = interp1d(mh, vbf)
#         self.__f_bbh_13TeV = interp1d(mh, bbh)
#         df = pd.read_csv(
#             this_dir +
#             "/Data/Higgs/XS/BSM_XS_13_WHZH.dat",
#             sep=r"\s+", header=None)
#         mh = df[0].to_numpy()
#         wh = df[1].to_numpy()
#         zh = df[2].to_numpy()
#         self.__f_wh_13TeV = interp1d(mh, wh)
#         self.__f_zh_13TeV = interp1d(mh, zh)
#         df = pd.read_csv(
#             this_dir + "/Data/Higgs/XS/BSM_XS_13_ttH.dat",
#             sep=r"\s+", header=None)
#         mh = df[0].to_numpy()
#         tth = df[1].to_numpy()
#         self.__f_tth_13TeV = interp1d(mh, tth)
# 
#     def get_ggh(self, m, com=13):
#         if com == 13:
#             return self.__f_ggh_13TeV(m)
# 
#     def get_vbf(self, m, com=13):
#         if com == 13:
#             return self.__f_vbf_13TeV(m)
# 
#     def get_bbh(self, m, com=13):
#         if com == 13:
#             return self.__f_bbh_13TeV(m)
# 
#     def get_wh(self, m, com=13):
#         if com == 13:
#             return self.__f_wh_13TeV(m)
# 
#     def get_zh(self, m, com=13):
#         if com == 13:
#             return self.__f_zh_13TeV(m)
# 
#     def get_tth(self, m, com=13):
#         if com == 13:
#             return self.__f_tth_13TeV(m)


class BR:

    def __init__(self):
        self.__create_interpolations()

    def __create_interpolations(self):
        this_dir, this_filename = os.path.split(__file__)
        with open(this_dir + "/Data/Higgs/BR/br.sm1_LHCHiggsXSWG_HDecay", "r") as f:
            lns = f.readlines()
        data = [[float(x) for x in ln.split(" ") if not x in ['', '\n']] for ln in lns[1::]]
        nn = len(data)
        mh = np.zeros(shape=(nn, ))
        br_bb = np.zeros(shape=(nn, ))
        br_ll = np.zeros(shape=(nn, ))
        br_mm = np.zeros(shape=(nn, ))
        br_ss = np.zeros(shape=(nn, ))
        br_cc = np.zeros(shape=(nn, ))
        br_tt = np.zeros(shape=(nn, ))
        for i, r in enumerate(data):
            mh[i] = r[0]
            br_bb[i] = r[1]
            br_ll[i] = r[2]
            br_mm[i] = r[3]
            br_ss[i] = r[4]
            br_cc[i] = r[5]
            br_tt[i] = r[6]
        self.__f_br_bb = interp1d(mh, br_bb)
        self.__f_br_ll = interp1d(mh, br_ll)
        self.__f_br_mm = interp1d(mh, br_mm)
        self.__f_br_ss = interp1d(mh, br_ss)
        self.__f_br_cc = interp1d(mh, br_cc)
        self.__f_br_tt = interp1d(mh, br_tt)
        with open(this_dir + "/Data/Higgs/BR/br.sm2_LHCHiggsXSWG_HDecay", "r") as f:
            lns = f.readlines()
        data = [[float(x) for x in ln.split(" ") if not x in ['', '\n']] for ln in lns[1::]]
        nn = len(data)
        mh = np.zeros(shape=(nn, ))
        br_gg = np.zeros(shape=(nn, ))
        br_yy = np.zeros(shape=(nn, ))
        br_zy = np.zeros(shape=(nn, ))
        br_ww = np.zeros(shape=(nn, ))
        br_zz = np.zeros(shape=(nn, ))
        gam_tot = np.zeros(shape=(nn, ))
        for i, r in enumerate(data):
            mh[i] = r[0]
            br_gg[i] = r[1]
            br_yy[i] = r[2]
            br_zy[i] = r[3]
            br_ww[i] = r[4]
            br_zz[i] = r[5]
            gam_tot[i] = r[6]
        self.__f_br_gg = interp1d(mh, br_gg)
        self.__f_br_yy = interp1d(mh, br_yy)
        self.__f_br_zy = interp1d(mh, br_zy)
        self.__f_br_ww = interp1d(mh, br_ww)
        self.__f_br_zz = interp1d(mh, br_zz)
        self.__f_gam_tot = interp1d(mh, gam_tot)

    def get_bb(self, m):
        return self.__f_br_bb(m)

    def get_ll(self, m):
        return self.__f_br_ll(m)

    def get_mm(self, m):
        return self.__f_br_mm(m)

    def get_ss(self, m):
        return self.__f_br_ss(m)

    def get_cc(self, m):
        return self.__f_br_cc(m)

    def get_tt(self, m):
        return self.__f_br_tt(m)

    def get_gg(self, m):
        return self.__f_br_gg(m)

    def get_yy(self, m):
        return self.__f_br_yy(m)

    def get_Zy(self, m):
        return self.__f_br_zy(m)

    def get_WW(self, m):
        return self.__f_br_ww(m)

    def get_ZZ(self, m):
        return self.__f_br_zz(m)

    def get_Gam_tot(self, m):
        return self.__f_gam_tot(m)
