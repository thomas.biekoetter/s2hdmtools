import numpy as np

from s2hdmTools.constraints.higgsius.controlHB \
    import controlhb
from s2hdmTools.constraints.higgsius.standardModel.higgs \
    import BR
from s2hdmTools.constraints.higgsius.theoHp import theohp


class HiggsBS:
    """
    The base class for the interface to
    HiggsBounds and HiggsSignals.

    An instance of this class has to be
    created only once in a python session.
    Afterwards, as many points as desired
    can be checked. At the end of the
    python session HiggsBounds should
    be closed using the function `adios`.
    """

    def __init__(self):
        """
        Initialize an instance of the
        HiggsBounds interface.

        The routines of HiggsBounds and
        HiggsSignals are loaded automatically
        with the number of HiggsBosons
        of the S2HDM, taking into account
        their CP properties and their charges.
        The mass uncertainty of the Higgs
        bosons is set to 0.5 GeV.
        """

        controlhb.init()

        # Not used but setting random value
        self.dm = 0.5

        self.brSM = BR()

    def check_point(self, pt):
        """
        Check the parameter point against
        collider constraints.

        The results are stored in the dictionaries:
        ```
        pt.HB
        pt.HS
        pt.XS
        ```
        The first dictionary contains the details
        of the HiggsBounds check. The second
        dictionary contains the details of the
        HiggsSignals check. The third dictionary
        contains values of the cross sections that
        were derived by HiggsBounds as functions
        of the effective coupling coefficients
        of the Higgs bosons.

        Args:
            pt (ParamPoint): The parameter point
                to be checked.
        """

        try:
            pt.b_H1
        except AttributeError:
            pt.calculate_branching_ratios()

        pt.effCpls = {}

        mh = np.array([pt.mH1, pt.mH2, pt.mH3, pt.mA])

        gammatotal = np.array([
            pt.wTot['H1'], pt.wTot['H2'],
            pt.wTot['H3'], pt.wTot['A']])

        cp_value = np.array([1, 1, 1, -1])

        dmhneut = np.array([self.dm] * 4)

        dmhch = np.array([self.dm])

        ghjss_s = np.zeros(shape=(4, ))
        ghjss_p = np.zeros(shape=(4, ))

        ghjcc_s = np.zeros(shape=(4, ))
        ghjcc_p = np.zeros(shape=(4, ))

        ghjbb_s = self.calc_eff_bbh_cpls(pt)
        ghjbb_p = self.calc_eff_bbA_cpls(pt)

        ghjtt_s = self.calc_eff_tth_cpls(pt)
        ghjtt_p = self.calc_eff_ttA_cpls(pt)

        ghjmumu_s = np.zeros(shape=(4, ))
        ghjmumu_p = np.zeros(shape=(4, ))

        ghjtautau_s = np.zeros(shape=(4, ))
        ghjtautau_p = np.zeros(shape=(4, ))

        ghjWW = self.calc_eff_VV_cpls(pt)
        ghjZZ = ghjWW

        ghjZga = np.zeros(shape=(4, ))

        ghjgaga = np.zeros(shape=(4, ))

        ghjgg = self.calc_eff_gg_cpls(pt)

        ghjhiZ = self.calc_eff_hZA_cpls(pt)

        BR_hjss = np.array([
            pt.b_H1['ss'],
            pt.b_H2['ss'],
            pt.b_H3['ss'],
            pt.b_A['ss']])

        BR_hjcc = np.array([
            pt.b_H1['cc'],
            pt.b_H2['cc'],
            pt.b_H3['cc'],
            pt.b_A['cc']])

        BR_hjbb = np.array([
            pt.b_H1['bb'],
            pt.b_H2['bb'],
            pt.b_H3['bb'],
            pt.b_A['bb']])

        BR_hjtt = np.array([
            pt.b_H1['tt'],
            pt.b_H2['tt'],
            pt.b_H3['tt'],
            pt.b_A['tt']])

        BR_hjmumu = np.array([
            pt.b_H1['mm'],
            pt.b_H2['mm'],
            pt.b_H3['mm'],
            pt.b_A['mm']])

        BR_hjtautau = np.array([
            pt.b_H1['ll'],
            pt.b_H2['ll'],
            pt.b_H3['ll'],
            pt.b_A['ll']])

        BR_hjWW = np.array([
            pt.b_H1['WW'],
            pt.b_H2['WW'],
            pt.b_H3['WW'],
            0.0])

        BR_hjZZ = np.array([
            pt.b_H1['ZZ'],
            pt.b_H2['ZZ'],
            pt.b_H3['ZZ'],
            0.0])

        BR_hjZga = np.array([
            pt.b_H1['Zy'],
            pt.b_H2['Zy'],
            pt.b_H3['Zy'],
            pt.b_A['Zy']])

        BR_hjgaga = np.array([
            pt.b_H1['yy'],
            pt.b_H2['yy'],
            pt.b_H3['yy'],
            pt.b_A['yy']])

        BR_hjgg = np.array([
            pt.b_H1['gg'],
            pt.b_H2['gg'],
            pt.b_H3['gg'],
            pt.b_A['gg']])

        BR_hjinvisible = np.array([
            pt.b_H1['XX'],
            pt.b_H2['XX'],
            pt.b_H3['XX'],
            0.0])

        BR_hkhjhi = np.zeros(shape=(4, 4, 4))
        BR_hkhjhi[1, 0, 0] = pt.b_H2['H1H1']
        BR_hkhjhi[2, 0, 0] = pt.b_H3['H1H1']
        BR_hkhjhi[2, 0, 1] = pt.b_H3['H1H2']
        BR_hkhjhi[2, 1, 0] = BR_hkhjhi[2, 0, 1]
        BR_hkhjhi[2, 1, 1] = pt.b_H3['H2H2']
        BR_hkhjhi[0, 3, 3] = pt.b_H1['AA']
        BR_hkhjhi[1, 3, 3] = pt.b_H2['AA']
        BR_hkhjhi[2, 3, 3] = pt.b_H3['AA']

        BR_hjhiZ = np.zeros(shape=(4, 4))
        BR_hjhiZ[0, 3] = pt.b_H1['ZA']
        BR_hjhiZ[1, 3] = pt.b_H2['ZA']
        BR_hjhiZ[2, 3] = pt.b_H3['ZA']
        BR_hjhiZ[3, 0] = pt.b_A['ZH1']
        BR_hjhiZ[3, 1] = pt.b_A['ZH2']
        BR_hjhiZ[3, 2] = pt.b_A['ZH3']

        BR_hjemu = np.zeros(shape=(4, ))
        BR_hjetau = np.zeros(shape=(4, ))
        BR_hjmutau = np.zeros(shape=(4, ))

        BR_hjHpiW = np.zeros(shape=(4, 1))
        BR_hjHpiW[0, 0] = pt.b_H1['WpHm']
        BR_hjHpiW[1, 0] = pt.b_H2['WpHm']
        BR_hjHpiW[2, 0] = pt.b_H3['WpHm']
        BR_hjHpiW[3, 0] = pt.b_A['WpHm']

        Mhplus = np.array([pt.mHp])

        GammaTotal_Hpj = np.array([pt.wTot['Hp']])

        CS_lep_HpjHmj_ratio = np.array([1.0])

        BR_tWpb = pt.b_t['Wb']

        BR_tHpjb = np.array([pt.b_t['Hpb']])

        BR_Hpjcs = np.array([pt.b_Hp['cs']])

        BR_Hpjcb = np.array([pt.b_Hp['cb']])

        BR_Hpjtaunu = np.array([pt.b_Hp['ln']])

        BR_Hpjtb = np.array([pt.b_Hp['tb']])

        BR_HpjWZ = np.array([0.0])

        BR_HpjhiW = np.zeros(shape=(1, 4))
        BR_HpjhiW[0, 0] = pt.b_Hp['WH1']
        BR_HpjhiW[0, 1] = pt.b_Hp['WH2']
        BR_HpjhiW[0, 2] = pt.b_Hp['WH3']
        BR_HpjhiW[0, 3] = pt.b_Hp['WA']

        CS_Hpjtb = np.zeros(shape=(1, ))

        CS_Hpjtb[0] = 2. * theohp.getxs(
            pt.yuktype,
            pt.mHp,
            pt.tb)[0]

        CS_Hpjcb = np.zeros(shape=(1, ))

        CS_Hpjbjet = np.zeros(shape=(1, ))

        CS_Hpjcjet = np.zeros(shape=(1, ))

        CS_Hpjjetjet = np.zeros(shape=(1, ))

        CS_HpjW = np.zeros(shape=(1, ))

        CS_HpjZ = np.zeros(shape=(1, ))

        CS_vbf_Hpj = np.zeros(shape=(1, ))

        CS_HpjHmj = np.zeros(shape=(1, ))

        CS_Hpjhi = np.zeros(shape=(1, 4))

        raw = controlhb.execute(
            mh, gammatotal, cp_value,
            dmhneut, dmhch,
            ghjss_s, ghjss_p,
            ghjcc_s, ghjcc_p,
            ghjbb_s, ghjbb_p,
            ghjtt_s, ghjtt_p,
            ghjmumu_s, ghjmumu_p,
            ghjtautau_s, ghjtautau_p,
            ghjWW, ghjZZ, ghjZga,
            ghjgaga, ghjgg, ghjhiZ,
            BR_hjss, BR_hjcc, BR_hjbb,
            BR_hjtt, BR_hjmumu,
            BR_hjtautau, BR_hjWW,
            BR_hjZZ, BR_hjZga, BR_hjgaga,
            BR_hjgg,
            BR_hjinvisible, BR_hkhjhi,
            BR_hjhiZ, BR_hjemu, BR_hjetau,
            BR_hjmutau, BR_hjHpiW,
            Mhplus, GammaTotal_Hpj,
            CS_lep_HpjHmj_ratio,
            BR_tWpb, BR_tHpjb,
            BR_Hpjcs, BR_Hpjcb,
            BR_Hpjtaunu, BR_Hpjtb,
            BR_HpjWZ, BR_HpjhiW,
            CS_Hpjtb, CS_Hpjcb,
            CS_Hpjbjet, CS_Hpjcjet,
            CS_Hpjjetjet, CS_HpjW,
            CS_HpjZ, CS_vbf_Hpj,
            CS_HpjHmj, CS_Hpjhi)

        HB = {}
        HS = {}
        XS = {}

        HB['result'] = raw[0][0]
        HB['result_H1'] = raw[0][1]
        HB['result_H2'] = raw[0][2]
        HB['result_H3'] = raw[0][3]
        HB['result_A'] = raw[0][4]
        HB['result_Hp'] = raw[0][5]

        HB['channel'] = raw[1][0]
        HB['channel_H1'] = raw[1][1]
        HB['channel_H2'] = raw[1][2]
        HB['channel_H3'] = raw[1][3]
        HB['channel_A'] = raw[1][4]
        HB['channel_Hp'] = raw[1][5]

        HB['obsratio'] = raw[2][0]
        HB['obsratio_H1'] = raw[2][1]
        HB['obsratio_H2'] = raw[2][2]
        HB['obsratio_H3'] = raw[2][3]
        HB['obsratio_A'] = raw[2][4]
        HB['obsratio_Hp'] = raw[2][5]

        HB['ncombined'] = raw[3][0]
        HB['ncombined_H1'] = raw[3][1]
        HB['ncombined_H2'] = raw[3][2]
        HB['ncombined_H3'] = raw[3][3]
        HB['ncombined_A'] = raw[3][4]
        HB['ncombined_Hp'] = raw[3][5]

        # HS['Chisq'] = raw[4]
        # HS['Chisq_mh'] = raw[5]
        HS['Chisq'] = raw[6]
        HS['nobs'] = raw[7]
        HS['Pvalue'] = raw[8]

        XS['singleH_H1_Tev2TeV'] = raw[9][0][0]
        XS['singleH_H1_LHC7TeV'] = raw[9][0][1]
        XS['singleH_H1_LHC8TeV'] = raw[9][0][2]
        XS['singleH_H1_LHC13TeV'] = raw[9][0][3]

        XS['singleH_H2_Tev2TeV'] = raw[9][1][0]
        XS['singleH_H2_LHC7TeV'] = raw[9][1][1]
        XS['singleH_H2_LHC8TeV'] = raw[9][1][2]
        XS['singleH_H2_LHC13TeV'] = raw[9][1][3]

        XS['singleH_H3_Tev2TeV'] = raw[9][2][0]
        XS['singleH_H3_LHC7TeV'] = raw[9][2][1]
        XS['singleH_H3_LHC8TeV'] = raw[9][2][2]
        XS['singleH_H3_LHC13TeV'] = raw[9][2][3]

        XS['singleH_A_Tev2TeV'] = raw[9][3][0]
        XS['singleH_A_LHC7TeV'] = raw[9][3][1]
        XS['singleH_A_LHC8TeV'] = raw[9][3][2]
        XS['singleH_A_LHC13TeV'] = raw[9][3][3]

        XS['ggH_H1_Tev2TeV'] = raw[10][0][0]
        XS['ggH_H1_LHC7TeV'] = raw[10][0][1]
        XS['ggH_H1_LHC8TeV'] = raw[10][0][2]
        XS['ggH_H1_LHC13TeV'] = raw[10][0][3]

        XS['ggH_H2_Tev2TeV'] = raw[10][1][0]
        XS['ggH_H2_LHC7TeV'] = raw[10][1][1]
        XS['ggH_H2_LHC8TeV'] = raw[10][1][2]
        XS['ggH_H2_LHC13TeV'] = raw[10][1][3]

        XS['ggH_H3_Tev2TeV'] = raw[10][2][0]
        XS['ggH_H3_LHC7TeV'] = raw[10][2][1]
        XS['ggH_H3_LHC8TeV'] = raw[10][2][2]
        XS['ggH_H3_LHC13TeV'] = raw[10][2][3]

        XS['ggH_A_Tev2TeV'] = raw[10][3][0]
        XS['ggH_A_LHC7TeV'] = raw[10][3][1]
        XS['ggH_A_LHC8TeV'] = raw[10][3][2]
        XS['ggH_A_LHC13TeV'] = raw[10][3][3]

        XS['bbH_H1_Tev2TeV'] = raw[11][0][0]
        XS['bbH_H1_LHC7TeV'] = raw[11][0][1]
        XS['bbH_H1_LHC8TeV'] = raw[11][0][2]
        XS['bbH_H1_LHC13TeV'] = raw[11][0][3]

        XS['bbH_H2_Tev2TeV'] = raw[11][1][0]
        XS['bbH_H2_LHC7TeV'] = raw[11][1][1]
        XS['bbH_H2_LHC8TeV'] = raw[11][1][2]
        XS['bbH_H2_LHC13TeV'] = raw[11][1][3]

        XS['bbH_H3_Tev2TeV'] = raw[11][2][0]
        XS['bbH_H3_LHC7TeV'] = raw[11][2][1]
        XS['bbH_H3_LHC8TeV'] = raw[11][2][2]
        XS['bbH_H3_LHC13TeV'] = raw[11][2][3]

        XS['bbH_A_Tev2TeV'] = raw[11][3][0]
        XS['bbH_A_LHC7TeV'] = raw[11][3][1]
        XS['bbH_A_LHC8TeV'] = raw[11][3][2]
        XS['bbH_A_LHC13TeV'] = raw[11][3][3]

        XS['VBF_H1_Tev2TeV'] = raw[12][0][0]
        XS['VBF_H1_LHC7TeV'] = raw[12][0][1]
        XS['VBF_H1_LHC8TeV'] = raw[12][0][2]
        XS['VBF_H1_LHC13TeV'] = raw[12][0][3]

        XS['VBF_H2_Tev2TeV'] = raw[12][1][0]
        XS['VBF_H2_LHC7TeV'] = raw[12][1][1]
        XS['VBF_H2_LHC8TeV'] = raw[12][1][2]
        XS['VBF_H2_LHC13TeV'] = raw[12][1][3]

        XS['VBF_H3_Tev2TeV'] = raw[12][2][0]
        XS['VBF_H3_LHC7TeV'] = raw[12][2][1]
        XS['VBF_H3_LHC8TeV'] = raw[12][2][2]
        XS['VBF_H3_LHC13TeV'] = raw[12][2][3]

        XS['VBF_A_Tev2TeV'] = raw[12][3][0]
        XS['VBF_A_LHC7TeV'] = raw[12][3][1]
        XS['VBF_A_LHC8TeV'] = raw[12][3][2]
        XS['VBF_A_LHC13TeV'] = raw[12][3][3]

        XS['WH_H1_Tev2TeV'] = raw[13][0][0]
        XS['WH_H1_LHC7TeV'] = raw[13][0][1]
        XS['WH_H1_LHC8TeV'] = raw[13][0][2]
        XS['WH_H1_LHC13TeV'] = raw[13][0][3]

        XS['WH_H2_Tev2TeV'] = raw[13][1][0]
        XS['WH_H2_LHC7TeV'] = raw[13][1][1]
        XS['WH_H2_LHC8TeV'] = raw[13][1][2]
        XS['WH_H2_LHC13TeV'] = raw[13][1][3]

        XS['WH_H3_Tev2TeV'] = raw[13][2][0]
        XS['WH_H3_LHC7TeV'] = raw[13][2][1]
        XS['WH_H3_LHC8TeV'] = raw[13][2][2]
        XS['WH_H3_LHC13TeV'] = raw[13][2][3]

        XS['WH_A_Tev2TeV'] = raw[13][3][0]
        XS['WH_A_LHC7TeV'] = raw[13][3][1]
        XS['WH_A_LHC8TeV'] = raw[13][3][2]
        XS['WH_A_LHC13TeV'] = raw[13][3][3]

        XS['ZH_H1_Tev2TeV'] = raw[14][0][0]
        XS['ZH_H1_LHC7TeV'] = raw[14][0][1]
        XS['ZH_H1_LHC8TeV'] = raw[14][0][2]
        XS['ZH_H1_LHC13TeV'] = raw[14][0][3]

        XS['ZH_H2_Tev2TeV'] = raw[14][1][0]
        XS['ZH_H2_LHC7TeV'] = raw[14][1][1]
        XS['ZH_H2_LHC8TeV'] = raw[14][1][2]
        XS['ZH_H2_LHC13TeV'] = raw[14][1][3]

        XS['ZH_H3_Tev2TeV'] = raw[14][2][0]
        XS['ZH_H3_LHC7TeV'] = raw[14][2][1]
        XS['ZH_H3_LHC8TeV'] = raw[14][2][2]
        XS['ZH_H3_LHC13TeV'] = raw[14][2][3]

        XS['ZH_A_Tev2TeV'] = raw[14][3][0]
        XS['ZH_A_LHC7TeV'] = raw[14][3][1]
        XS['ZH_A_LHC8TeV'] = raw[14][3][2]
        XS['ZH_A_LHC13TeV'] = raw[14][3][3]

        XS['ttH_H1_Tev2TeV'] = raw[15][0][0]
        XS['ttH_H1_LHC7TeV'] = raw[15][0][1]
        XS['ttH_H1_LHC8TeV'] = raw[15][0][2]
        XS['ttH_H1_LHC13TeV'] = raw[15][0][3]

        XS['ttH_H2_Tev2TeV'] = raw[15][1][0]
        XS['ttH_H2_LHC7TeV'] = raw[15][1][1]
        XS['ttH_H2_LHC8TeV'] = raw[15][1][2]
        XS['ttH_H2_LHC13TeV'] = raw[15][1][3]

        XS['ttH_H3_Tev2TeV'] = raw[15][2][0]
        XS['ttH_H3_LHC7TeV'] = raw[15][2][1]
        XS['ttH_H3_LHC8TeV'] = raw[15][2][2]
        XS['ttH_H3_LHC13TeV'] = raw[15][2][3]

        XS['ttH_A_Tev2TeV'] = raw[15][3][0]
        XS['ttH_A_LHC7TeV'] = raw[15][3][1]
        XS['ttH_A_LHC8TeV'] = raw[15][3][2]
        XS['ttH_A_LHC13TeV'] = raw[15][3][3]

        XS['tH_tchan_H1_Tev2TeV'] = raw[16][0][0]
        XS['tH_tchan_H1_LHC7TeV'] = raw[16][0][1]
        XS['tH_tchan_H1_LHC8TeV'] = raw[16][0][2]
        XS['tH_tchan_H1_LHC13TeV'] = raw[16][0][3]

        XS['tH_tchan_H2_Tev2TeV'] = raw[16][1][0]
        XS['tH_tchan_H2_LHC7TeV'] = raw[16][1][1]
        XS['tH_tchan_H2_LHC8TeV'] = raw[16][1][2]
        XS['tH_tchan_H2_LHC13TeV'] = raw[16][1][3]

        XS['tH_tchan_H3_Tev2TeV'] = raw[16][2][0]
        XS['tH_tchan_H3_LHC7TeV'] = raw[16][2][1]
        XS['tH_tchan_H3_LHC8TeV'] = raw[16][2][2]
        XS['tH_tchan_H3_LHC13TeV'] = raw[16][2][3]

        XS['tH_tchan_A_Tev2TeV'] = raw[16][3][0]
        XS['tH_tchan_A_LHC7TeV'] = raw[16][3][1]
        XS['tH_tchan_A_LHC8TeV'] = raw[16][3][2]
        XS['tH_tchan_A_LHC13TeV'] = raw[16][3][3]

        XS['tH_schan_H1_Tev2TeV'] = raw[17][0][0]
        XS['tH_schan_H1_LHC7TeV'] = raw[17][0][1]
        XS['tH_schan_H1_LHC8TeV'] = raw[17][0][2]
        XS['tH_schan_H1_LHC13TeV'] = raw[17][0][3]

        XS['tH_schan_H2_Tev2TeV'] = raw[17][1][0]
        XS['tH_schan_H2_LHC7TeV'] = raw[17][1][1]
        XS['tH_schan_H2_LHC8TeV'] = raw[17][1][2]
        XS['tH_schan_H2_LHC13TeV'] = raw[17][1][3]

        XS['tH_schan_H3_Tev2TeV'] = raw[17][2][0]
        XS['tH_schan_H3_LHC7TeV'] = raw[17][2][1]
        XS['tH_schan_H3_LHC8TeV'] = raw[17][2][2]
        XS['tH_schan_H3_LHC13TeV'] = raw[17][2][3]

        XS['tH_schan_A_Tev2TeV'] = raw[17][3][0]
        XS['tH_schan_A_LHC7TeV'] = raw[17][3][1]
        XS['tH_schan_A_LHC8TeV'] = raw[17][3][2]
        XS['tH_schan_A_LHC13TeV'] = raw[17][3][3]

        XS['qqZH_H1_Tev2TeV'] = raw[18][0][0]
        XS['qqZH_H1_LHC7TeV'] = raw[18][0][1]
        XS['qqZH_H1_LHC8TeV'] = raw[18][0][2]
        XS['qqZH_H1_LHC13TeV'] = raw[18][0][3]

        XS['qqZH_H2_Tev2TeV'] = raw[18][1][0]
        XS['qqZH_H2_LHC7TeV'] = raw[18][1][1]
        XS['qqZH_H2_LHC8TeV'] = raw[18][1][2]
        XS['qqZH_H2_LHC13TeV'] = raw[18][1][3]

        XS['qqZH_H3_Tev2TeV'] = raw[18][2][0]
        XS['qqZH_H3_LHC7TeV'] = raw[18][2][1]
        XS['qqZH_H3_LHC8TeV'] = raw[18][2][2]
        XS['qqZH_H3_LHC13TeV'] = raw[18][2][3]

        XS['qqZH_A_Tev2TeV'] = raw[18][3][0]
        XS['qqZH_A_LHC7TeV'] = raw[18][3][1]
        XS['qqZH_A_LHC8TeV'] = raw[18][3][2]
        XS['qqZH_A_LHC13TeV'] = raw[18][3][3]

        XS['ggZH_H1_Tev2TeV'] = raw[19][0][0]
        XS['ggZH_H1_LHC7TeV'] = raw[19][0][1]
        XS['ggZH_H1_LHC8TeV'] = raw[19][0][2]
        XS['ggZH_H1_LHC13TeV'] = raw[19][0][3]

        XS['ggZH_H2_Tev2TeV'] = raw[19][1][0]
        XS['ggZH_H2_LHC7TeV'] = raw[19][1][1]
        XS['ggZH_H2_LHC8TeV'] = raw[19][1][2]
        XS['ggZH_H2_LHC13TeV'] = raw[19][1][3]

        XS['ggZH_H3_Tev2TeV'] = raw[19][2][0]
        XS['ggZH_H3_LHC7TeV'] = raw[19][2][1]
        XS['ggZH_H3_LHC8TeV'] = raw[19][2][2]
        XS['ggZH_H3_LHC13TeV'] = raw[19][2][3]

        XS['ggZH_A_Tev2TeV'] = raw[19][3][0]
        XS['ggZH_A_LHC7TeV'] = raw[19][3][1]
        XS['ggZH_A_LHC8TeV'] = raw[19][3][2]
        XS['ggZH_A_LHC13TeV'] = raw[19][3][3]

        pt.HB = HB
        pt.HS = HS
        pt.XS = XS


    def calc_eff_bbh_cpls(self, pt):

        cpls = np.zeros(shape=(4, ))

        for i in range(0, 3):
            if pt.yuktype == 1:
                cpls[i] = pt.R[i, 1] / pt.sb
            elif pt.yuktype == 2:
                cpls[i] = pt.R[i, 0] / pt.cb
            elif pt.yuktype == 3:
                cpls[i] = pt.R[i, 1] / pt.sb
            elif pt.yuktype == 4:
                cpls[i] = pt.R[i, 0] / pt.cb

        pt.effCpls['Hdd'] = cpls[0:3]

        return cpls

    def calc_eff_bbA_cpls(self, pt):

        cpls = np.zeros(shape=(4, ))

        # See https://www.itp.kit.edu/~rauch/Teaching/WS1415_BSMHiggs/bsm.pdf
        if pt.yuktype == 1:
            cpls[3] = -1.0 / pt.tb
        elif pt.yuktype == 2:
            cpls[3] = pt.tb
        elif pt.yuktype == 3:
            cpls[3] = -1.0 / pt.tb
        elif pt.yuktype == 4:
            cpls[3] = pt.tb

        pt.effCpls['Add'] = cpls[3]

        return cpls

    def calc_eff_tth_cpls(self, pt):

        cpls = np.zeros(shape=(4, ))

        for i in range(0, 3):
            cpls[i] = pt.R[i, 1] / pt.sb

        pt.effCpls['Huu'] = cpls[0:3]

        return cpls

    def calc_eff_ttA_cpls(self, pt):

        cpls = np.zeros(shape=(4, ))

        # See https://www.itp.kit.edu/~rauch/Teaching/WS1415_BSMHiggs/bsm.pdf
        cpls[3] = 1.0 / pt.tb

        pt.effCpls['Auu'] = cpls[3]

        return cpls

    def calc_eff_VV_cpls(self, pt):

        cpls = np.zeros(shape=(4, ))

        for i in range(0, 3):
            cpls[i] = pt.cb * pt.R[i, 0] + pt.sb * pt.R[i, 1]

        pt.effCpls['HVV'] = cpls[0:3]

        return cpls

    def calc_eff_gg_cpls(self, pt):

        # Define via decay width ratio phi(m) -> gg / hSM(m) -> gg
        cpls = np.zeros(shape=(4, ))

        GamH1 = pt.b_H1['gg'] * pt.wTot['H1']
        GamSM = self.brSM.get_gg(pt.mH1) * self.brSM.get_Gam_tot(pt.mH1)
        cpls[0] = np.sqrt(GamH1 / GamSM)

        GamH2 = pt.b_H2['gg'] * pt.wTot['H2']
        GamSM = self.brSM.get_gg(pt.mH2) * self.brSM.get_Gam_tot(pt.mH2)
        cpls[1] = np.sqrt(GamH2 / GamSM)

        GamH3 = pt.b_H3['gg'] * pt.wTot['H3']
        GamSM = self.brSM.get_gg(pt.mH3) * self.brSM.get_Gam_tot(pt.mH3)
        cpls[2] = np.sqrt(GamH3 / GamSM)

        GamA = pt.b_A['gg'] * pt.wTot['A']
        GamSM = self.brSM.get_gg(pt.mA) * self.brSM.get_Gam_tot(pt.mA)
        cpls[3] = np.sqrt(GamA / GamSM)

        pt.effCpls['Hgg'] = cpls[0:3]
        pt.effCpls['Agg'] = cpls[3]

        return cpls


    def calc_eff_hZA_cpls(self, pt):

        cpls = np.zeros(shape=(4, 4))

        a1 = pt.al1
        a2 = pt.al2
        a3 = pt.al3
        b = np.arctan(pt.tb)
        c = np.cos
        s = np.sin

        # See scrutinity paper
        cpls[3, 0] = -c(a2) * s(b - a1)
        cpls[0, 3] = cpls[3, 0]
        cpls[3, 1] = s(b - a1) * s(a2) * s(a3) + \
            c(a3) * c(b - a1)
        cpls[1, 3] = cpls[3, 1]
        cpls[3, 2] = c(a3) * s(b - a1) * s(a2) - \
            s(a3) * c(b - a1)
        cpls[2, 3] = cpls[3, 2]

        return cpls

    def adios(self):
        """
        Close the HiggsBounds library
        and free the memory.

        After this function is called,
        no more calls of the function
        `check_point` can be performed.
        """

        controlhb.feierabend()
