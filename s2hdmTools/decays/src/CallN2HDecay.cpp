#include "AnyHdecay.hpp"
// #include <iostream>


double* setup(
  int yuktype,
  double tb,
  double m12sq,
  double mA,
  double mHp,
  double mH1,
  double mH2,
  double mH3,
  double al1,
  double al2,
  double al3,
  double vs) {

  using namespace AnyHdecay;

  int N;
  static const Hdecay hdec{};
  double* vals = new double[84];

  auto brs = hdec.n2hdmBroken(
    static_cast<Yukawa>(yuktype),
    TanBeta{tb},
    SquaredMassPar{m12sq},
    AMass{mA},
    HcMass{mHp},
    HMass{mH1},
    HMass{mH2},
    HMass{mH3},
    MixAngle{al1},
    MixAngle{al2},
    MixAngle{al3},
    SingletVev{vs}
  );

  N = 0;
  for (auto [key, value] : brs) {
//  std::cout<<key<<"    "<<value<<std::endl;
    vals[N] = value;
    N++;
  }

  return vals;

}

extern "C" {

  double* setup_bar(
    int yuktype,
    double tb,
    double m12sq,
    double mA,
    double mHp,
    double mH1,
    double mH2,
    double mH3,
    double al1,
    double al2,
    double al3,
    double vs) {

    return setup(
      yuktype,
      tb,
      m12sq,
      mA,
      mHp,
      mH1,
      mH2,
      mH3,
      al1,
      al2,
      al3,
      vs);

  }

}
