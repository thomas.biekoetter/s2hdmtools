import numpy as np


def calc_gammas_inv(pt):
    # Checked with FeynRules

    cpls = calc_couplings_hXX(
        pt.R, pt.lam6, pt.lam7, pt.lam8,
        pt.v1, pt.v2, pt.vs)

    Gamh1XX = calc_gamma_hXX(pt.mH1, pt.mXi, cpls[0])
    Gamh2XX = calc_gamma_hXX(pt.mH2, pt.mXi, cpls[1])
    Gamh3XX = calc_gamma_hXX(pt.mH3, pt.mXi, cpls[2])

    return np.array([Gamh1XX, Gamh2XX, Gamh3XX])

def calc_couplings_hXX(R, l6, l7, l8, v1, v2, vs):

    cpls = np.zeros(shape=(3, ))

    cpls[0] = l7 * v1
    cpls[1] = l8 * v2
    cpls[2] = l6 * vs

    return R.dot(cpls)

def calc_gamma_hXX(mH, mXi, cpl):

    if mH > 2. * mXi:
        y = 1. / (32. * np.pi * mH)
        y *= np.sqrt(1. - (4. * mXi**2) / mH**2)
        y *= cpl**2
    else:
        y = 0.

    return y
