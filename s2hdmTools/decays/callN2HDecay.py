import ctypes
import numpy as np

from s2hdmTools.decays.CallN2HDecayPATH import AN_LIB_DIR

lib = ctypes.cdll.LoadLibrary(
    AN_LIB_DIR + '/CallN2HDecay.so')

lib.setup_bar.restype = np.ctypeslib.ndpointer(
    dtype=ctypes.c_double,
    shape=(84, ))

lib.setup_bar.argtypes = [
    ctypes.c_int,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double]

zerowidth = 1e-10


class CallN2HDecay:

    def __init__(self, pt):

        raw = self.run_N2HDecay(
            pt.yuktype, pt.tb, pt.m12sq, pt.mA,
            pt.mHp, pt.mH1, pt.mH2, pt.mH3,
            pt.al1, pt.al2, pt.al3, pt.vs)

        self.convert_to_dicts(raw)

        if pt.debug:
            self.sanity_checks()

    def run_N2HDecay(
            self, yuktype, tb, m12sq, mA,
            mHp, mH1, mH2, mH3,
            al1, al2, al3, vs):

        yuktype = ctypes.c_int(yuktype)
        tb = ctypes.c_double(tb)
        m12sq = ctypes.c_double(m12sq)
        mA = ctypes.c_double(mA)
        mHp = ctypes.c_double(mHp)
        mH1 = ctypes.c_double(mH1)
        mH2 = ctypes.c_double(mH2)
        mH3 = ctypes.c_double(mH3)
        al1 = ctypes.c_double(al1)
        al2 = ctypes.c_double(al2)
        al3 = ctypes.c_double(al3)
        vs = ctypes.c_double(vs)

        n2hdata = lib.setup_bar(
            yuktype, tb, m12sq, mA,
            mHp, mH1, mH2, mH3,
            al1, al2, al3, vs)

        return n2hdata

    def convert_to_dicts(self, raw):

        self.b_A = {
            'WpHm': raw[0],
            'ZH1': raw[1],
            'ZH2': raw[2],
            'ZH3': raw[3],
            'Zy': raw[4],
            'bb': raw[5],
            'cc': raw[6],
            'yy': raw[7],
            'gg': raw[8],
            'mm': raw[9],
            'ss': raw[10],
            'll': raw[11],
            'tt': raw[12]}
        for k, v in self.b_A.items():
            if v <= zerowidth:
                self.b_A[k] = 0.

        self.b_H1 = {
            'AA': raw[13],
            'HpHm': raw[14],
            'WW': raw[15],
            'WpHm': raw[16],
            'ZA': raw[17],
            'ZZ': raw[18],
            'Zy': raw[19],
            'bb': raw[20],
            'cc': raw[21],
            'yy': raw[22],
            'gg': raw[23],
            'mm': raw[24],
            'ss': raw[25],
            'll': raw[26],
            'tt': raw[27]}
        for k, v in self.b_H1.items():
            if v <= zerowidth:
                self.b_H1[k] = 0.

        self.b_H2 = {
            'AA': raw[28],
            'H1H1': raw[29],
            'HpHm': raw[30],
            'WW': raw[31],
            'WpHm': raw[32],
            'ZA': raw[33],
            'ZZ': raw[34],
            'Zy': raw[35],
            'bb': raw[36],
            'cc': raw[37],
            'yy': raw[38],
            'gg': raw[39],
            'mm': raw[40],
            'ss': raw[41],
            'll': raw[42],
            'tt': raw[43]}
        for k, v in self.b_H2.items():
            if v <= zerowidth:
                self.b_H2[k] = 0.

        self.b_H3 = {
            'AA': raw[44],
            'H1H1': raw[45],
            'H1H2': raw[46],
            'H2H2': raw[47],
            'HpHm': raw[48],
            'WW': raw[49],
            'WpHm': raw[50],
            'ZA': raw[51],
            'ZZ': raw[52],
            'Zy': raw[53],
            'bb': raw[54],
            'cc': raw[55],
            'yy': raw[56],
            'gg': raw[57],
            'mm': raw[58],
            'ss': raw[59],
            'll': raw[60],
            'tt': raw[61]}
        for k, v in self.b_H3.items():
            if v <= zerowidth:
                self.b_H3[k] = 0.

        self.b_Hp = {
            'WA': raw[62],
            'WH1': raw[63],
            'WH2': raw[64],
            'WH3': raw[65],
            'cb': raw[66],
            'cd': raw[67],
            'cs': raw[68],
            'mn': raw[69],
            'ln': raw[70],
            'tb': raw[71],
            'td': raw[72],
            'ts': raw[73],
            'ub': raw[74],
            'us': raw[75]}
        for k, v in self.b_Hp.items():
            if v <= zerowidth:
                self.b_Hp[k] = 0.

        self.b_t = {
            'Hpb': raw[76],
            'Wb': raw[77]}

        self.w = {
            'A': raw[78],
            'H1': raw[79],
            'H2': raw[80],
            'H3': raw[81],
            'Hp': raw[82],
            't': raw[83]}

    def sanity_checks(self):

        if abs(sum(self.b_A.values()) - 1.) > 1e-6:
            raise RuntimeError(
                'Branching ratios of A do not add up to one.')

        if abs(sum(self.b_H1.values()) - 1.) > 1e-6:
            raise RuntimeError(
                'Branching ratios of H1 do not add up to one.')

        if abs(sum(self.b_H2.values()) - 1.) > 1e-6:
            raise RuntimeError(
                'Branching ratios of H2 do not add up to one.')

        if abs(sum(self.b_H3.values()) - 1.) > 1e-6:
            raise RuntimeError(
                'Branching ratios of H3 do not add up to one.')

        if abs(sum(self.b_Hp.values()) - 1.) > 1e-6:
            raise RuntimeError(
                'Branching ratios of Hp do not add up to one.')

        if abs(sum(self.b_t.values()) - 1.) > 1e-6:
            raise RuntimeError(
                'Branching ratios of t do not add up to one.')
