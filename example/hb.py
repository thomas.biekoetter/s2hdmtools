from s2hdmTools.paramPoint import ParamPoint
from s2hdmTools.constraints.higgsius.higgsBS \
    import HiggsBS
from random import random
import numpy as np


def main():

    paras0 = {
        'type': 2,
        'tb': 1.26,
        'al1': 1.27,
        'al2': -1.08,
        'al3': -1.24,
        'mH1': 96.5,
        'mH2': 125.09,
        'mH3': 535.86,
        'mXi': 100.0,
        'mA': 712.578,
        'mHp': 737.8,
        'vs': 272.0,
        'm12sq': 80644.0}

    paras1 = {
        'type': 2,
        'tb': 1.26,
        'al1': 1.27,
        'al2': -1.08,
        'al3': -1.24,
        'mH1': 96.5,
        'mH2': 125.09,
        'mH3': 535.86,
        'mXi': 100.0,
        'mA': 702.0,
        'mHp': 710.0,
        'vs': 473.0,
        'm12sq': 150644.0}

    paras2 = {
        'type': 2,
        'tb': 2.26,
        'al1': 1.27,
        'al2': -1.08,
        'al3': -1.24,
        'mH1': 86.5,
        'mH2': 125.09,
        'mH3': 535.86,
        'mXi': 1000.0,
        'mA': 412.0,
        'mHp': 737.0,
        'vs': 273.0,
        'm12sq': 80644.0}

    parasR = {
        'type': 2,
        'tb': 4. * random(),
        'al1': np.pi / 2 * random(),
        'al2': np.pi / 2 * random(),
        'al3': np.pi / 2 * random(),
        'mH1': 125.09,
        'mH2': 200. + 100. * random(),
        'mH3': 600. + 100. * random(),
        'mXi': 1000. * random(),
        'mA': 800 * random(),
        'mHp': 800. * random(),
        'vs': 1000. * random(),
        'm12sq': 1000000. * random()}


    pt0 = ParamPoint(paras0)
    pt1 = ParamPoint(paras1)
    pt2 = ParamPoint(paras2)
    ptR = ParamPoint(parasR)

    hbhs = HiggsBS()

    pt = pt0

    print(pt.v1, pt.v2, pt.vs)
    print(pt.check_boundedness())
    print(pt.check_tree_pert_uni())
    print(pt.check_ewpo())
    print(pt.EWPO)
    print(pt.check_metastability())
    hbhs.check_point(pt)
    print(pt.HB)
    print(pt.HS)

    hbhs.adios()


main()
