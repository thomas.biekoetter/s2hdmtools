from s2hdmTools.paramPoint import ParamPoint


def main():

    paras1 = {
        'type': 2,
        'tb': 1.26,
        'al1': 1.27,
        'al2': -1.08,
        'al3': -1.24,
        'mH1': 96.5,
        'mH2': 125.09,
        'mH3': 535.86,
        'mXi': 1000.0,
        'mA': 712.0,
        'mHp': 737.0,
        'vs': 273.0,
        'm12sq': 80644.0}

    pt1 = ParamPoint(paras1)
    pt1.calculate_branching_ratios()

    print(pt1.b_H1)
    print()
    print(pt1.b_H2)
    print()
    print(pt1.b_H3)
    print()
    print(pt1.b_A)
    print()
    print(pt1.b_Hp)
    print()

main()
