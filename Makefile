# Package name
PACKAGE_NAME = s2hdmTools
PROJECT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# Subpackage names
SUB_DE_NAME = decays
SUB_VS_NAME = constraints/metastability
SUB_HB_NAME = constraints/higgsius
SUB_DM_NAME = constraints/darkMatter
SUB_RG_NAME = rge

# output formatting
BOLD := \033[1m
RESET := \033[0m


# Compilers
PC = python
PC2 =
F2PC2 =
FC = gfortran
CC = gcc
CXX = g++
CM = cmake

# check if version 3.9
PYV_MIN_MAIN = 3
PYV_MIN_MINOR = 9
PYV_MAIN = $(shell $(PC) -c 'import sys; print("%d"% sys.version_info[0])' )
PYV_MINOR = $(shell $(PC) -c 'import sys; print("%d"% sys.version_info[1])' )
PYV_OK_MAIN = $(shell $(PC) -c 'import sys;\
  print(int(float("%d"% sys.version_info[0]) >= $(PYV_MIN_MAIN)))' )
PYV_OK_MINOR = $(shell $(PC) -c 'import sys;\
  print(int(float("%d"% sys.version_info[1]) >= $(PYV_MIN_MINOR)))' )
ifeq ($(PYV_OK_MAIN),0)
  $(error "python points to version $(PYV_MAIN), but version >$(PYV_MIN_MAIN) is required.")
endif
ifeq ($(PYV_OK_MINOR),0)
  $(error "python points to version $(PYV_MAIN).$(PYV_MINOR), but version >$(PYV_MIN_MAIN).$(PYV_MIN_MINOR) is required.")
endif

# check that path to python 2  and f2py was given (to use MadDM)
ifdef PC2
  ifdef F2PC2
    download-maddm: download-maddm-conditional-1
    build-maddm: build-maddm-conditional-1
  else
    $(info "f2py2 path for MadDM not given. Skipping installation of MadDM." )
    download-maddm: download-maddm-conditional-2
    build-maddm: build-maddm-conditional-2
  endif
else
  $(info "python2 path for MadDM not given. Skipping installation of MadDM." )
  download-maddm: download-maddm-conditional-2
  build-maddm: build-maddm-conditional-2
endif


# External sources

# check if HiggsTools already installed
HT_TEST = $(shell $(PC) tools/checkforhiggstools.py)

ifeq ($(HT_TEST),Yes)
    download-higgstools: download-higgstools-dont
    build-higgstools: build-higgstools-dont
endif
ifeq ($(HT_TEST),No)
    download-higgstools: download-higgstools-do
    build-higgstools: build-higgstools-do
endif

# anyhdecay
AN_NAME = anyhdecay
AN_URL = https://gitlab.com/jonaswittbrodt/anyhdecay.git
AN_GIT = git@gitlab.com:jonaswittbrodt/anyhdecay.git

# HOM4PS2
SYS = $(shell $(CC) -dumpmachine)
ifneq (, $(findstring linux, $(SYS)))
PS_NAME = HOM4PS2_64-bit
PS_FLD = HOM4PS2
else
PS_NAME = HOM4PS2_MacOSX
PS_FLD = HOM4PS2_Mac
endif
PS_TAR = $(PS_NAME).tar.gz
PS_URL = http://www.math.nsysu.edu.tw/~leetsung/works/HOM4PS_soft_files/$(PS_TAR)

# HiggsBounds
HB_NAME = higgsbounds
HB_URL = https://gitlab.com/higgsbounds/higgsbounds.git
HB_GIT = git@gitlab.com:higgsbounds/higgsbounds.git

# HiggsSignals
HS_NAME = higgssignals
HS_URL = https://gitlab.com/higgsbounds/higgssignals.git
HS_GIT = git@gitlab.com:higgsbounds/higgssignals.git

# Micromegas
MO_NAME = micromegas_5.2.13
MO_TAR = $(MO_NAME).tgz
MO_URL = http://lapth.cnrs.fr/micromegas/downloadarea/code/$(MO_NAME).tgz

# MadDM
MA_NAME = MG5_aMC_v3.1.1
MA_TAR = $(MA_NAME).tar.gz
MA_URL = https://launchpad.net/mg5amcnlo/3.0/3.1.x/+download/$(MA_NAME).tar.gz

# HiggsTools
HT_NAME = higgstools
HT_URL = https://gitlab.com/higgsbounds/higgstools
HT_GIT = git@gitlab.com:higgsbounds/higgstools.git
HB_DATA_NAME = hbdataset
HB_DATA_URL = https://gitlab.com/higgsbounds/hbdataset
HS_DATA_NAME = hsdataset
HS_DATA_URL = https://gitlab.com/higgsbounds/hsdataset

# dds2hdm
DD_NAME = dds2hdm
DD_URL = https://gitlab.com/thomas.biekoetter/dds2hdm
DD_GIT = git@gitlab.com:thomas.biekoetter/dds2hdm.git


# See compiler versions

compiler-info:
	bash -c "which $(PC)"
	bash -c "$(PC) --version"
	bash -c "which $(FC)"
	bash -c "$(FC) --version"
	bash -c "which $(CC)"
	bash -c "$(CC) --version"
	bash -c "which $(CXX)"
	bash -c "$(CXX) --version"


# Download external sources

download: download-anyhdecay download-hom4ps
download: download-higgsbounds download-higgssignals
download: download-micromegas download-maddm
download: download-higgstools download-dds2hdm

download-anyhdecay:
	@echo
	bash -c "rm -rf ./external/$(AN_NAME) ||:"
	@echo "$(BOLD)Cloning $(AN_NAME) from $(AN_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(AN_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(AN_NAME))"

download-hom4ps:
	@echo
	bash -c "rm -rf ./external/$(PS_NAME) ||:"
	@echo "$(BOLD)Downlading $(PS_NAME) from $(PS_URL)...$(RESET)"
	bash -c "cd ./external/ && wget $(PS_URL)"
	bash -c "cd ./external/ && tar -xvzf $(PS_TAR) && rm $(PS_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(PS_NAME))"

download-higgsbounds:
	@echo
	bash -c "rm -rf ./external/$(HB_NAME) ||:"
	@echo "$(BOLD)Cloning $(HB_NAME) from $(HB_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(HB_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(HB_NAME))"

download-higgssignals:
	@echo
	bash -c "rm -rf ./external/$(HS_NAME) ||:"
	@echo "$(BOLD)Cloning $(HS_NAME) from $(HS_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(HS_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(HS_NAME))"

download-micromegas:
	@echo
	@echo "$(BOLD)Downloading $(MO_NAME) from $(MO_URL)...$(RESET)"
	bash -c "cd ./external/ && mkdir -p $(MO_NAME) && wget $(MO_URL)"
	bash -c "cd ./external/ && tar -xvzf $(MO_TAR) && rm $(MO_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(MO_NAME))"

download-maddm-conditional-1:
	@echo
	@echo "$(BOLD)Downloading $(MA_NAME) from $(MA_URL)...$(RESET)"
	bash -c "cd ./external/ && wget $(MA_URL)"
	bash -c "cd ./external/ && tar -xvzf $(MA_TAR) && rm $(MA_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(MA_NAME))"

download-maddm-conditional-2:
	@echo
	@echo "$(BOLD)Not downloading $(MA_NAME)...$(RESET)"

download-higgstools-dont:
	@echo
	@echo "$(BOLD)Not downloading $(HT_NAME) because already installed...$(RESET)"
	@echo "$(BOLD)Only downloading $(HT_NAME) data repositories...$(RESET)"
	bash -c "rm -rf ./external/$(HB_DATA_NAME) ||:"
	bash -c "rm -rf ./external/$(HS_DATA_NAME) ||:"
	bash -c "cd ./external/ && git clone $(HB_DATA_URL)"
	bash -c "cd ./external/ && git clone $(HS_DATA_URL)"

download-higgstools-do:
	@echo
	bash -c "rm -rf ./external/$(HT_NAME) ||:"
	bash -c "rm -rf ./external/$(HB_DATA_NAME) ||:"
	bash -c "rm -rf ./external/$(HS_DATA_NAME) ||:"
	@echo "$(BOLD)Cloning $(HT_NAME) from $(HT_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(HT_URL)"
	bash -c "cd ./external/ && git clone $(HB_DATA_URL)"
	bash -c "cd ./external/ && git clone $(HS_DATA_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(HT_NAME))"

download-dds2hdm:
	@echo
	bash -c "rm -rf ./external/$(DD_NAME) ||:"
	@echo "$(BOLD)Cloning $(DD_NAME) from $(DD_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(DD_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(DD_NAME))"


# Build external sources

build-external: build-anyhdecay build-hom4ps
build-external: build-higgsbounds build-higgssignals
build-external: build-micromegas build-maddm
build-external: build-higgstools build-dds2hdm

build-anyhdecay:
	@echo
	@echo "$(BOLD)Building $(AN_NAME)...$(RESET)"
	bash -c "cd ./external/$(AN_NAME) && rm -fr build ||:"
	bash -c "cd ./external/$(AN_NAME) && mkdir build"
	bash -c "cd ./external/$(AN_NAME)/build && FC=$(FC) CC=$(CC) CXX=$(CXX) $(CM) .. -DCMAKE_Fortran_FLAGS='-fPIC' -DCMAKE_CXX_FLAGS='-fPIC'"
	bash -c "cd ./external/$(AN_NAME)/build && make"
	@echo "$(BOLD)Done...$(RESET) (build: $(AN_NAME))"

build-hom4ps:
	@echo
	@echo "$(BOLD)Building $(PS_NAME)...$(RESET)"
	bash -c "echo $(SYS)"
	bash -c "echo PS_EXEC_DIR = \'$(PWD)/external/$(PS_FLD)/\' > ./s2hdmTools/$(SUB_VS_NAME)/hom4ps2PATH.py"

build-higgsbounds:
	@echo
	@echo "$(BOLD)Building $(HB_NAME)...$(RESET)"
	bash -c "cd ./external/$(HB_NAME) && rm -r build ||:"
	bash -c "cd ./external/$(HB_NAME) && mkdir build"
	bash -c "cd ./external/$(HB_NAME)/build && FC=$(FC) CC=$(CC) CXX=$(CXX) $(CM) .. -DCMAKE_Fortran_FLAGS='-fPIC'"
	bash -c "cd ./external/$(HB_NAME)/build && make"
	@echo "$(BOLD)Done...$(RESET) (build: $(HB_NAME))"

build-higgssignals:
	@echo
	@echo "$(BOLD)Building $(HS_NAME)...$(RESET)"
	bash -c "cd ./external/$(HS_NAME) && rm -r build ||:"
	bash -c "cd ./external/$(HS_NAME) && mkdir build"
	bash -c "cd ./external/$(HS_NAME)/build && FC=$(FC) CC=$(CC) CXX=$(CXX) $(CM) .. -DCMAKE_Fortran_FLAGS='-fPIC' -DHiggsBounds_DIR:PATH=$(PROJECT_DIR)/external/higgsbounds/build"
	bash -c "cd ./external/$(HS_NAME)/build && make"
	@echo "$(BOLD)Done...$(RESET) (build: $(HS_NAME))"

build-micromegas:
	@echo
	@echo "$(BOLD)Building $(MO_NAME)...$(RESET)"
	bash -c "cd ./external/$(MO_NAME) && printf 'Y\n' | make clean"
	bash -c "cd ./external/$(MO_NAME) && FC=$(FC) CC=$(CC) CXX=$(CXX) make"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMI ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMIuni ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMII ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMIIuni ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMIII ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMIIIuni ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMIV ||:"
	bash -c "rm -r ./external/$(MO_NAME)/S2HDMIVuni ||:"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMI"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMIuni"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMII"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMIIuni"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMIII"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMIIIuni"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMIV"
	bash -c "cd ./external/$(MO_NAME) && ./newProject S2HDMIVuni"
	bash -c "cp ./tools/calcheps/1/feyn/*.mdl ./external/$(MO_NAME)/S2HDMI/work/models/"
	bash -c "cp ./tools/calcheps/1/uni/*.mdl ./external/$(MO_NAME)/S2HDMIuni/work/models/"
	bash -c "cp ./tools/calcheps/2/feyn/*.mdl ./external/$(MO_NAME)/S2HDMII/work/models/"
	bash -c "cp ./tools/calcheps/2/uni/*.mdl ./external/$(MO_NAME)/S2HDMIIuni/work/models/"
	bash -c "cp ./tools/calcheps/3/feyn/*.mdl ./external/$(MO_NAME)/S2HDMIII/work/models/"
	bash -c "cp ./tools/calcheps/3/uni/*.mdl ./external/$(MO_NAME)/S2HDMIIIuni/work/models/"
	bash -c "cp ./tools/calcheps/4/feyn/*.mdl ./external/$(MO_NAME)/S2HDMIV/work/models/"
	bash -c "cp ./tools/calcheps/4/uni/*.mdl ./external/$(MO_NAME)/S2HDMIVuni/work/models/"
	bash -c "cd ./external/$(MO_NAME)/S2HDMI && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIuni && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMII && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIIuni && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIII && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIIIuni && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIV && make clean"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIVuni && make clean"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMI/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMIuni/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMII/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMIIuni/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMIII/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMIIIuni/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMIV/"
	bash -c "cp ./tools/micro_mains/main.cpp ./external/$(MO_NAME)/S2HDMIVuni/"
	bash -c "cd ./external/$(MO_NAME)/S2HDMI && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIuni && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMII && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIIuni && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIII && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIIIuni && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIV && make main=main.cpp"
	bash -c "cd ./external/$(MO_NAME)/S2HDMIVuni && make main=main.cpp"
	bash -c "rm ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py ||:"
	bash -c "echo MO_EXEC_DIR_1 = \'$(PWD)/external/$(MO_NAME)/S2HDMI\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_2 = \'$(PWD)/external/$(MO_NAME)/S2HDMII\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_3 = \'$(PWD)/external/$(MO_NAME)/S2HDMIII\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_4 = \'$(PWD)/external/$(MO_NAME)/S2HDMIV\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_UNI_1 = \'$(PWD)/external/$(MO_NAME)/S2HDMIuni\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_UNI_2 = \'$(PWD)/external/$(MO_NAME)/S2HDMIIuni\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_UNI_3 = \'$(PWD)/external/$(MO_NAME)/S2HDMIIIuni\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	bash -c "echo MO_EXEC_DIR_UNI_4 = \'$(PWD)/external/$(MO_NAME)/S2HDMIVuni\' >> ./s2hdmTools/$(SUB_DM_NAME)/microPATH.py"
	@echo "$(BOLD)Done...$(RESET) (build: $(MO_NAME))"

build-maddm-conditional-1:
	@echo
	@echo "$(BOLD)Building $(MA_NAME)...$(RESET)"
	bash -c "rm ./s2hdmTools/$(SUB_DM_NAME)/python_2_exec.py ||:"
	bash -c "rm ./s2hdmTools/$(SUB_DM_NAME)/maddmPATH.py ||:"
	bash -c "echo PYTHON_2_EXEC = \'$(PC2)\' >> ./s2hdmTools/$(SUB_DM_NAME)/pythonTWO.py"
	bash -c "echo MA_EXEC_DIR = \'$(PWD)/external/MG5_aMC_v3_1_1\' >> ./s2hdmTools/$(SUB_DM_NAME)/maddmPATH.py"
	@$(PC2) -m pip install -U --no-cache-dir numpy scipy
	bash -c "echo \"install maddm\" | ./external/MG5_aMC_v3_1_1/bin/mg5_aMC"
	exit
	bash -c "cd ./tools/install_scripts/ && $(PC) setf2pyMG.py $(F2PC2)"
	bash -c "cp -r ./tools/UFOs/2/feyn/S2HDMII ./external/MG5_aMC_v3_1_1/models/"
	bash -c "rm -r ./external/MG5_aMC_v3_1_1/s2hdmii_relic ||:"
	bash -c "cp ./tools/maddm_cards/gen_relic_2.dat ./external/MG5_aMC_v3_1_1/"
	bash -c "cd ./external/MG5_aMC_v3_1_1/ && $(PC2) ./bin/maddm.py gen_relic_2.dat"
	bash -c "cp ./tools/maddm_cards/run_relic_2.dat ./external/MG5_aMC_v3_1_1/"
	bash -c "rm -r ./external/MG5_aMC_v3_1_1/s2hdmii_anom ||:"
	bash -c "cp ./tools/maddm_cards/gen_anom_2.dat ./external/MG5_aMC_v3_1_1/"
	bash -c "cd ./external/MG5_aMC_v3_1_1/ && $(PC2) ./bin/maddm.py gen_anom_2.dat"
	bash -c "cp ./tools/maddm_cards/run_anom_2.dat ./external/MG5_aMC_v3_1_1/"
	bash -c "rm ./MG5_debug ||:"
	@echo "$(BOLD)Done...$(RESET) (build: $(MA_NAME))"

build-maddm-conditional-2:
	@echo
	@echo "$(BOLD)Not building $(MA_NAME)...$(RESET)"

build-higgstools-do:
	@echo
	@echo "$(BOLD)Building $(HT_NAME)...$(RESET)"
	bash -c "cd ./external/$(HT_NAME) && CXX=$(CXX) CC=$(CC) $(PC) -m pip install . -vvv"
	bash -c "rm s2hdmTools/constraints/higgsius/hbdataPath.py ||:"
	bash -c "rm s2hdmTools/constraints/higgsius/hsdataPath.py ||:"
	bash -c "touch s2hdmTools/constraints/higgsius/hbdataPath.py"
	bash -c "touch s2hdmTools/constraints/higgsius/hsdataPath.py"
	bash -c "echo HB_DATA_PATH = \'$(PWD)/external/$(HB_DATA_NAME)\' >> ./s2hdmTools/constraints/higgsius/hbdataPath.py"
	bash -c "echo HS_DATA_PATH = \'$(PWD)/external/$(HS_DATA_NAME)\' >> ./s2hdmTools/constraints/higgsius/hsdataPath.py"
	@echo "$(BOLD)Done...$(RESET) (build: $(HT_NAME))"

build-higgstools-dont:
	@echo
	@echo "$(BOLD)Not building $(HT_NAME)...$(RESET)"
	bash -c "rm s2hdmTools/constraints/higgsius/hbdataPath.py ||:"
	bash -c "rm s2hdmTools/constraints/higgsius/hsdataPath.py ||:"
	bash -c "touch s2hdmTools/constraints/higgsius/hbdataPath.py"
	bash -c "touch s2hdmTools/constraints/higgsius/hsdataPath.py"
	bash -c "echo HB_DATA_PATH = \'$(PWD)/external/$(HB_DATA_NAME)\' >> ./s2hdmTools/constraints/higgsius/hbdataPath.py"
	bash -c "echo HS_DATA_PATH = \'$(PWD)/external/$(HS_DATA_NAME)\' >> ./s2hdmTools/constraints/higgsius/hsdataPath.py"
	@echo "$(BOLD)Done...$(RESET)"

build-dds2hdm:
	@echo
	@echo "$(BOLD)Building $(DD_NAME)...$(RESET)"
	bash -c "$(PC) -m pip uninstall -y dds2hdm"
	bash -c "cd ./external/$(DD_NAME) && make all PC=$(PC) FC=$(FC)"
	@echo "$(BOLD)Done...$(RESET) (build: $(DD_NAME))"

# Build internal sources

build-internal: build-ewpo build-decays build-higgsius build-rge

build-ewpo:
	@echo
	bash -c "echo STU_DATA_PATH = \'$(PWD)/data/ewpo\' >> ./s2hdmTools/constraints/ewpo/gfitterPath.py"
	@echo "$(BOLD)Done...$(RESET) (build: EWPO))"

build-decays:
	@echo
	@echo "$(BOLD)Building $(SUB_DE_NAME)...$(RESET)"
	bash -c "rm s2hdmTools/$(SUB_DE_NAME)/CallN2HDecay.so ||:"
	bash -c "cd ./s2hdmTools/$(SUB_DE_NAME) && make CallN2HDecay CXX=$(CXX)"
	@echo "$(BOLD)Done...$(RESET) (build: $(SUB_DE_NAME))"

build-higgsius:
	@echo
	@echo "$(BOLD)Building $(SUB_HB_NAME)...$(RESET)"
	bash -c "rm s2hdmTools/$(SUB_HB_NAME)/*.so ||:"
	bash -c "cd ./s2hdmTools/$(SUB_HB_NAME) && make higgsius PC=$(PC) FC=$(FC) CC=$(CC) CXX=$(CXX)"
	@echo "$(BOLD)Done...$(RESET) (build: $(SUB_HB_NAME))"

build-rge:
	@echo
	@echo "$(BOLD)Building $(SUB_RG_NAME)...$(RESET)"
	bash -c "rm s2hdmTools/$(SUB_RG_NAME)/*.so ||:"
	bash -c "cd ./s2hdmTools/$(SUB_RG_NAME) && make all PC=$(PC) FC=$(FC)"
	@echo "$(BOLD)Done...$(RESET) (build: $(SUB_RG_NAME))"

# Install as package

install: install-python-prerequisites install-python

install-python-prerequisites:
	@echo
	@echo "$(BOLD)Installing/upgrading prerequisites in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install pip setuptools numpy
	@echo "$(BOLD)Done...$(RESET) (installed: pip, setuptools and numpy)"

install-python:
	@echo
	@echo "$(BOLD)Installing $(PACKAGE_NAME) in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install .
	@echo "$(BOLD)Done...$(RESET) (installed: $(PACKAGE_NAME))"


# Make everything

all: install-python-prerequisites download
all: build-external build-internal install-python

# Documentation

doccs:
	@echo
	@echo "$(BOLD)Building documentation...$(RESET)"
	bash -c "$(PC) -m pip install mkdocs"
	bash -c "$(PC) -m pip install mkdocstrings[python]"
	bash -c "$(PC) -m pip install mkdocs-material"
	bash -c "$(PC) -m mkdocs build"

# Cleaning

clean:
	@echo
	@echo "$(BOLD)Cleaning...$(RESET)"
	bash -c "git clean -f -xdf"
	@echo "$(BOLD)Done...$(RESET)"


$(shell mkdir -p external)
