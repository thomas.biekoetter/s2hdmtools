import sys


MGCONFIG = '../../external/MG5_aMC_v3_1_1/input/mg5_configuration.txt'


def main(argv):

    f2py_path = argv[0]

    with open(MGCONFIG, 'r')  as f:
        lns = f.readlines()

    newlns = []

    for ln in lns:

        if 'f2py_compiler_py2' in ln:

            try:
                x = ln.split('None')[0].split('# ')[1] + f2py_path + '\n'
            except:
                x = ln.split('=')[0] + '= ' + f2py_path + '\n'
            newlns.append(x)

        else:

            newlns.append(ln)

    with open(MGCONFIG, 'w') as f:
        f.writelines(newlns)


if __name__ == "__main__":
    main(sys.argv[1:])
