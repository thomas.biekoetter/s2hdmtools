# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Fri 18 Jun 2021 18:05:55


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-G',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-(CB*complex(0,1)*I1a11)',
                order = {'QED':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(CB*complex(0,1)*I1a12)',
                 order = {'QED':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-(CB*complex(0,1)*I1a13)',
                 order = {'QED':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-(CB*complex(0,1)*I1a21)',
                 order = {'QED':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-(CB*complex(0,1)*I1a22)',
                 order = {'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(CB*complex(0,1)*I1a23)',
                 order = {'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '-(CB*complex(0,1)*I1a31)',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(CB*complex(0,1)*I1a32)',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-(CB*complex(0,1)*I1a33)',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'CB*complex(0,1)*I2a11',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'CB*complex(0,1)*I2a12',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = 'CB*complex(0,1)*I2a13',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = 'CB*complex(0,1)*I2a21',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = 'CB*complex(0,1)*I2a22',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'CB*complex(0,1)*I2a23',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = 'CB*complex(0,1)*I2a31',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'CB*complex(0,1)*I2a32',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = 'CB*complex(0,1)*I2a33',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = 'CB*complex(0,1)*I3a11',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'CB*complex(0,1)*I3a12',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = 'CB*complex(0,1)*I3a13',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = 'CB*complex(0,1)*I3a21',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = 'CB*complex(0,1)*I3a22',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = 'CB*complex(0,1)*I3a23',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = 'CB*complex(0,1)*I3a31',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = 'CB*complex(0,1)*I3a32',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = 'CB*complex(0,1)*I3a33',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '-(CB*complex(0,1)*I4a11)',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-(CB*complex(0,1)*I4a12)',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '-(CB*complex(0,1)*I4a13)',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '-(CB*complex(0,1)*I4a21)',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(CB*complex(0,1)*I4a22)',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-(CB*complex(0,1)*I4a23)',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(CB*complex(0,1)*I4a31)',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-(CB*complex(0,1)*I4a32)',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-(CB*complex(0,1)*I4a33)',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-3*complex(0,1)*lam6',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '-(complex(0,1)*lam7*RA1x1**2) - complex(0,1)*lam8*RA1x2**2 - complex(0,1)*lam6*RA1x3**2',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-3*complex(0,1)*lam1*RA1x1**4 - 6*complex(0,1)*lam3*RA1x1**2*RA1x2**2 - 6*complex(0,1)*lam4*RA1x1**2*RA1x2**2 - 6*complex(0,1)*lam5*RA1x1**2*RA1x2**2 - 3*complex(0,1)*lam2*RA1x2**4 - 6*complex(0,1)*lam7*RA1x1**2*RA1x3**2 - 6*complex(0,1)*lam8*RA1x2**2*RA1x3**2 - 3*complex(0,1)*lam6*RA1x3**4',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(complex(0,1)*lam7*RA1x1*RA2x1) - complex(0,1)*lam8*RA1x2*RA2x2 - complex(0,1)*lam6*RA1x3*RA2x3',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-3*complex(0,1)*lam1*RA1x1**3*RA2x1 - 3*complex(0,1)*lam3*RA1x1*RA1x2**2*RA2x1 - 3*complex(0,1)*lam4*RA1x1*RA1x2**2*RA2x1 - 3*complex(0,1)*lam5*RA1x1*RA1x2**2*RA2x1 - 3*complex(0,1)*lam7*RA1x1*RA1x3**2*RA2x1 - 3*complex(0,1)*lam3*RA1x1**2*RA1x2*RA2x2 - 3*complex(0,1)*lam4*RA1x1**2*RA1x2*RA2x2 - 3*complex(0,1)*lam5*RA1x1**2*RA1x2*RA2x2 - 3*complex(0,1)*lam2*RA1x2**3*RA2x2 - 3*complex(0,1)*lam8*RA1x2*RA1x3**2*RA2x2 - 3*complex(0,1)*lam7*RA1x1**2*RA1x3*RA2x3 - 3*complex(0,1)*lam8*RA1x2**2*RA1x3*RA2x3 - 3*complex(0,1)*lam6*RA1x3**3*RA2x3',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '-(complex(0,1)*lam7*RA2x1**2) - complex(0,1)*lam8*RA2x2**2 - complex(0,1)*lam6*RA2x3**2',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '-3*complex(0,1)*lam1*RA1x1**2*RA2x1**2 - complex(0,1)*lam3*RA1x2**2*RA2x1**2 - complex(0,1)*lam4*RA1x2**2*RA2x1**2 - complex(0,1)*lam5*RA1x2**2*RA2x1**2 - complex(0,1)*lam7*RA1x3**2*RA2x1**2 - 4*complex(0,1)*lam3*RA1x1*RA1x2*RA2x1*RA2x2 - 4*complex(0,1)*lam4*RA1x1*RA1x2*RA2x1*RA2x2 - 4*complex(0,1)*lam5*RA1x1*RA1x2*RA2x1*RA2x2 - complex(0,1)*lam3*RA1x1**2*RA2x2**2 - complex(0,1)*lam4*RA1x1**2*RA2x2**2 - complex(0,1)*lam5*RA1x1**2*RA2x2**2 - 3*complex(0,1)*lam2*RA1x2**2*RA2x2**2 - complex(0,1)*lam8*RA1x3**2*RA2x2**2 - 4*complex(0,1)*lam7*RA1x1*RA1x3*RA2x1*RA2x3 - 4*complex(0,1)*lam8*RA1x2*RA1x3*RA2x2*RA2x3 - complex(0,1)*lam7*RA1x1**2*RA2x3**2 - complex(0,1)*lam8*RA1x2**2*RA2x3**2 - 3*complex(0,1)*lam6*RA1x3**2*RA2x3**2',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-3*complex(0,1)*lam1*RA1x1*RA2x1**3 - 3*complex(0,1)*lam3*RA1x2*RA2x1**2*RA2x2 - 3*complex(0,1)*lam4*RA1x2*RA2x1**2*RA2x2 - 3*complex(0,1)*lam5*RA1x2*RA2x1**2*RA2x2 - 3*complex(0,1)*lam3*RA1x1*RA2x1*RA2x2**2 - 3*complex(0,1)*lam4*RA1x1*RA2x1*RA2x2**2 - 3*complex(0,1)*lam5*RA1x1*RA2x1*RA2x2**2 - 3*complex(0,1)*lam2*RA1x2*RA2x2**3 - 3*complex(0,1)*lam7*RA1x3*RA2x1**2*RA2x3 - 3*complex(0,1)*lam8*RA1x3*RA2x2**2*RA2x3 - 3*complex(0,1)*lam7*RA1x1*RA2x1*RA2x3**2 - 3*complex(0,1)*lam8*RA1x2*RA2x2*RA2x3**2 - 3*complex(0,1)*lam6*RA1x3*RA2x3**3',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '-3*complex(0,1)*lam1*RA2x1**4 - 6*complex(0,1)*lam3*RA2x1**2*RA2x2**2 - 6*complex(0,1)*lam4*RA2x1**2*RA2x2**2 - 6*complex(0,1)*lam5*RA2x1**2*RA2x2**2 - 3*complex(0,1)*lam2*RA2x2**4 - 6*complex(0,1)*lam7*RA2x1**2*RA2x3**2 - 6*complex(0,1)*lam8*RA2x2**2*RA2x3**2 - 3*complex(0,1)*lam6*RA2x3**4',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(complex(0,1)*lam7*RA1x1*RA3x1) - complex(0,1)*lam8*RA1x2*RA3x2 - complex(0,1)*lam6*RA1x3*RA3x3',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '-3*complex(0,1)*lam1*RA1x1**3*RA3x1 - 3*complex(0,1)*lam3*RA1x1*RA1x2**2*RA3x1 - 3*complex(0,1)*lam4*RA1x1*RA1x2**2*RA3x1 - 3*complex(0,1)*lam5*RA1x1*RA1x2**2*RA3x1 - 3*complex(0,1)*lam7*RA1x1*RA1x3**2*RA3x1 - 3*complex(0,1)*lam3*RA1x1**2*RA1x2*RA3x2 - 3*complex(0,1)*lam4*RA1x1**2*RA1x2*RA3x2 - 3*complex(0,1)*lam5*RA1x1**2*RA1x2*RA3x2 - 3*complex(0,1)*lam2*RA1x2**3*RA3x2 - 3*complex(0,1)*lam8*RA1x2*RA1x3**2*RA3x2 - 3*complex(0,1)*lam7*RA1x1**2*RA1x3*RA3x3 - 3*complex(0,1)*lam8*RA1x2**2*RA1x3*RA3x3 - 3*complex(0,1)*lam6*RA1x3**3*RA3x3',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(complex(0,1)*lam7*RA2x1*RA3x1) - complex(0,1)*lam8*RA2x2*RA3x2 - complex(0,1)*lam6*RA2x3*RA3x3',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '-3*complex(0,1)*lam1*RA1x1**2*RA2x1*RA3x1 - complex(0,1)*lam3*RA1x2**2*RA2x1*RA3x1 - complex(0,1)*lam4*RA1x2**2*RA2x1*RA3x1 - complex(0,1)*lam5*RA1x2**2*RA2x1*RA3x1 - complex(0,1)*lam7*RA1x3**2*RA2x1*RA3x1 - 2*complex(0,1)*lam3*RA1x1*RA1x2*RA2x2*RA3x1 - 2*complex(0,1)*lam4*RA1x1*RA1x2*RA2x2*RA3x1 - 2*complex(0,1)*lam5*RA1x1*RA1x2*RA2x2*RA3x1 - 2*complex(0,1)*lam7*RA1x1*RA1x3*RA2x3*RA3x1 - 2*complex(0,1)*lam3*RA1x1*RA1x2*RA2x1*RA3x2 - 2*complex(0,1)*lam4*RA1x1*RA1x2*RA2x1*RA3x2 - 2*complex(0,1)*lam5*RA1x1*RA1x2*RA2x1*RA3x2 - complex(0,1)*lam3*RA1x1**2*RA2x2*RA3x2 - complex(0,1)*lam4*RA1x1**2*RA2x2*RA3x2 - complex(0,1)*lam5*RA1x1**2*RA2x2*RA3x2 - 3*complex(0,1)*lam2*RA1x2**2*RA2x2*RA3x2 - complex(0,1)*lam8*RA1x3**2*RA2x2*RA3x2 - 2*complex(0,1)*lam8*RA1x2*RA1x3*RA2x3*RA3x2 - 2*complex(0,1)*lam7*RA1x1*RA1x3*RA2x1*RA3x3 - 2*complex(0,1)*lam8*RA1x2*RA1x3*RA2x2*RA3x3 - complex(0,1)*lam7*RA1x1**2*RA2x3*RA3x3 - complex(0,1)*lam8*RA1x2**2*RA2x3*RA3x3 - 3*complex(0,1)*lam6*RA1x3**2*RA2x3*RA3x3',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-3*complex(0,1)*lam1*RA1x1*RA2x1**2*RA3x1 - 2*complex(0,1)*lam3*RA1x2*RA2x1*RA2x2*RA3x1 - 2*complex(0,1)*lam4*RA1x2*RA2x1*RA2x2*RA3x1 - 2*complex(0,1)*lam5*RA1x2*RA2x1*RA2x2*RA3x1 - complex(0,1)*lam3*RA1x1*RA2x2**2*RA3x1 - complex(0,1)*lam4*RA1x1*RA2x2**2*RA3x1 - complex(0,1)*lam5*RA1x1*RA2x2**2*RA3x1 - 2*complex(0,1)*lam7*RA1x3*RA2x1*RA2x3*RA3x1 - complex(0,1)*lam7*RA1x1*RA2x3**2*RA3x1 - complex(0,1)*lam3*RA1x2*RA2x1**2*RA3x2 - complex(0,1)*lam4*RA1x2*RA2x1**2*RA3x2 - complex(0,1)*lam5*RA1x2*RA2x1**2*RA3x2 - 2*complex(0,1)*lam3*RA1x1*RA2x1*RA2x2*RA3x2 - 2*complex(0,1)*lam4*RA1x1*RA2x1*RA2x2*RA3x2 - 2*complex(0,1)*lam5*RA1x1*RA2x1*RA2x2*RA3x2 - 3*complex(0,1)*lam2*RA1x2*RA2x2**2*RA3x2 - 2*complex(0,1)*lam8*RA1x3*RA2x2*RA2x3*RA3x2 - complex(0,1)*lam8*RA1x2*RA2x3**2*RA3x2 - complex(0,1)*lam7*RA1x3*RA2x1**2*RA3x3 - complex(0,1)*lam8*RA1x3*RA2x2**2*RA3x3 - 2*complex(0,1)*lam7*RA1x1*RA2x1*RA2x3*RA3x3 - 2*complex(0,1)*lam8*RA1x2*RA2x2*RA2x3*RA3x3 - 3*complex(0,1)*lam6*RA1x3*RA2x3**2*RA3x3',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '-3*complex(0,1)*lam1*RA2x1**3*RA3x1 - 3*complex(0,1)*lam3*RA2x1*RA2x2**2*RA3x1 - 3*complex(0,1)*lam4*RA2x1*RA2x2**2*RA3x1 - 3*complex(0,1)*lam5*RA2x1*RA2x2**2*RA3x1 - 3*complex(0,1)*lam7*RA2x1*RA2x3**2*RA3x1 - 3*complex(0,1)*lam3*RA2x1**2*RA2x2*RA3x2 - 3*complex(0,1)*lam4*RA2x1**2*RA2x2*RA3x2 - 3*complex(0,1)*lam5*RA2x1**2*RA2x2*RA3x2 - 3*complex(0,1)*lam2*RA2x2**3*RA3x2 - 3*complex(0,1)*lam8*RA2x2*RA2x3**2*RA3x2 - 3*complex(0,1)*lam7*RA2x1**2*RA2x3*RA3x3 - 3*complex(0,1)*lam8*RA2x2**2*RA2x3*RA3x3 - 3*complex(0,1)*lam6*RA2x3**3*RA3x3',
                 order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(complex(0,1)*lam7*RA3x1**2) - complex(0,1)*lam8*RA3x2**2 - complex(0,1)*lam6*RA3x3**2',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '-3*complex(0,1)*lam1*RA1x1**2*RA3x1**2 - complex(0,1)*lam3*RA1x2**2*RA3x1**2 - complex(0,1)*lam4*RA1x2**2*RA3x1**2 - complex(0,1)*lam5*RA1x2**2*RA3x1**2 - complex(0,1)*lam7*RA1x3**2*RA3x1**2 - 4*complex(0,1)*lam3*RA1x1*RA1x2*RA3x1*RA3x2 - 4*complex(0,1)*lam4*RA1x1*RA1x2*RA3x1*RA3x2 - 4*complex(0,1)*lam5*RA1x1*RA1x2*RA3x1*RA3x2 - complex(0,1)*lam3*RA1x1**2*RA3x2**2 - complex(0,1)*lam4*RA1x1**2*RA3x2**2 - complex(0,1)*lam5*RA1x1**2*RA3x2**2 - 3*complex(0,1)*lam2*RA1x2**2*RA3x2**2 - complex(0,1)*lam8*RA1x3**2*RA3x2**2 - 4*complex(0,1)*lam7*RA1x1*RA1x3*RA3x1*RA3x3 - 4*complex(0,1)*lam8*RA1x2*RA1x3*RA3x2*RA3x3 - complex(0,1)*lam7*RA1x1**2*RA3x3**2 - complex(0,1)*lam8*RA1x2**2*RA3x3**2 - 3*complex(0,1)*lam6*RA1x3**2*RA3x3**2',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '-3*complex(0,1)*lam1*RA1x1*RA2x1*RA3x1**2 - complex(0,1)*lam3*RA1x2*RA2x2*RA3x1**2 - complex(0,1)*lam4*RA1x2*RA2x2*RA3x1**2 - complex(0,1)*lam5*RA1x2*RA2x2*RA3x1**2 - complex(0,1)*lam7*RA1x3*RA2x3*RA3x1**2 - 2*complex(0,1)*lam3*RA1x2*RA2x1*RA3x1*RA3x2 - 2*complex(0,1)*lam4*RA1x2*RA2x1*RA3x1*RA3x2 - 2*complex(0,1)*lam5*RA1x2*RA2x1*RA3x1*RA3x2 - 2*complex(0,1)*lam3*RA1x1*RA2x2*RA3x1*RA3x2 - 2*complex(0,1)*lam4*RA1x1*RA2x2*RA3x1*RA3x2 - 2*complex(0,1)*lam5*RA1x1*RA2x2*RA3x1*RA3x2 - complex(0,1)*lam3*RA1x1*RA2x1*RA3x2**2 - complex(0,1)*lam4*RA1x1*RA2x1*RA3x2**2 - complex(0,1)*lam5*RA1x1*RA2x1*RA3x2**2 - 3*complex(0,1)*lam2*RA1x2*RA2x2*RA3x2**2 - complex(0,1)*lam8*RA1x3*RA2x3*RA3x2**2 - 2*complex(0,1)*lam7*RA1x3*RA2x1*RA3x1*RA3x3 - 2*complex(0,1)*lam7*RA1x1*RA2x3*RA3x1*RA3x3 - 2*complex(0,1)*lam8*RA1x3*RA2x2*RA3x2*RA3x3 - 2*complex(0,1)*lam8*RA1x2*RA2x3*RA3x2*RA3x3 - complex(0,1)*lam7*RA1x1*RA2x1*RA3x3**2 - complex(0,1)*lam8*RA1x2*RA2x2*RA3x3**2 - 3*complex(0,1)*lam6*RA1x3*RA2x3*RA3x3**2',
                 order = {'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = '-3*complex(0,1)*lam1*RA2x1**2*RA3x1**2 - complex(0,1)*lam3*RA2x2**2*RA3x1**2 - complex(0,1)*lam4*RA2x2**2*RA3x1**2 - complex(0,1)*lam5*RA2x2**2*RA3x1**2 - complex(0,1)*lam7*RA2x3**2*RA3x1**2 - 4*complex(0,1)*lam3*RA2x1*RA2x2*RA3x1*RA3x2 - 4*complex(0,1)*lam4*RA2x1*RA2x2*RA3x1*RA3x2 - 4*complex(0,1)*lam5*RA2x1*RA2x2*RA3x1*RA3x2 - complex(0,1)*lam3*RA2x1**2*RA3x2**2 - complex(0,1)*lam4*RA2x1**2*RA3x2**2 - complex(0,1)*lam5*RA2x1**2*RA3x2**2 - 3*complex(0,1)*lam2*RA2x2**2*RA3x2**2 - complex(0,1)*lam8*RA2x3**2*RA3x2**2 - 4*complex(0,1)*lam7*RA2x1*RA2x3*RA3x1*RA3x3 - 4*complex(0,1)*lam8*RA2x2*RA2x3*RA3x2*RA3x3 - complex(0,1)*lam7*RA2x1**2*RA3x3**2 - complex(0,1)*lam8*RA2x2**2*RA3x3**2 - 3*complex(0,1)*lam6*RA2x3**2*RA3x3**2',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = '-3*complex(0,1)*lam1*RA1x1*RA3x1**3 - 3*complex(0,1)*lam3*RA1x2*RA3x1**2*RA3x2 - 3*complex(0,1)*lam4*RA1x2*RA3x1**2*RA3x2 - 3*complex(0,1)*lam5*RA1x2*RA3x1**2*RA3x2 - 3*complex(0,1)*lam3*RA1x1*RA3x1*RA3x2**2 - 3*complex(0,1)*lam4*RA1x1*RA3x1*RA3x2**2 - 3*complex(0,1)*lam5*RA1x1*RA3x1*RA3x2**2 - 3*complex(0,1)*lam2*RA1x2*RA3x2**3 - 3*complex(0,1)*lam7*RA1x3*RA3x1**2*RA3x3 - 3*complex(0,1)*lam8*RA1x3*RA3x2**2*RA3x3 - 3*complex(0,1)*lam7*RA1x1*RA3x1*RA3x3**2 - 3*complex(0,1)*lam8*RA1x2*RA3x2*RA3x3**2 - 3*complex(0,1)*lam6*RA1x3*RA3x3**3',
                 order = {'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = '-3*complex(0,1)*lam1*RA2x1*RA3x1**3 - 3*complex(0,1)*lam3*RA2x2*RA3x1**2*RA3x2 - 3*complex(0,1)*lam4*RA2x2*RA3x1**2*RA3x2 - 3*complex(0,1)*lam5*RA2x2*RA3x1**2*RA3x2 - 3*complex(0,1)*lam3*RA2x1*RA3x1*RA3x2**2 - 3*complex(0,1)*lam4*RA2x1*RA3x1*RA3x2**2 - 3*complex(0,1)*lam5*RA2x1*RA3x1*RA3x2**2 - 3*complex(0,1)*lam2*RA2x2*RA3x2**3 - 3*complex(0,1)*lam7*RA2x3*RA3x1**2*RA3x3 - 3*complex(0,1)*lam8*RA2x3*RA3x2**2*RA3x3 - 3*complex(0,1)*lam7*RA2x1*RA3x1*RA3x3**2 - 3*complex(0,1)*lam8*RA2x2*RA3x2*RA3x3**2 - 3*complex(0,1)*lam6*RA2x3*RA3x3**3',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '-3*complex(0,1)*lam1*RA3x1**4 - 6*complex(0,1)*lam3*RA3x1**2*RA3x2**2 - 6*complex(0,1)*lam4*RA3x1**2*RA3x2**2 - 6*complex(0,1)*lam5*RA3x1**2*RA3x2**2 - 3*complex(0,1)*lam2*RA3x2**4 - 6*complex(0,1)*lam7*RA3x1**2*RA3x3**2 - 6*complex(0,1)*lam8*RA3x2**2*RA3x3**2 - 3*complex(0,1)*lam6*RA3x3**4',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = 'complex(0,1)*I1a11*SB',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = 'complex(0,1)*I1a12*SB',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = 'complex(0,1)*I1a13*SB',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = 'complex(0,1)*I1a21*SB',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = 'complex(0,1)*I1a22*SB',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = 'complex(0,1)*I1a23*SB',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = 'complex(0,1)*I1a31*SB',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = 'complex(0,1)*I1a32*SB',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = 'complex(0,1)*I1a33*SB',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = 'complex(0,1)*I2a11*SB',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = 'complex(0,1)*I2a12*SB',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = 'complex(0,1)*I2a13*SB',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = 'complex(0,1)*I2a21*SB',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = 'complex(0,1)*I2a22*SB',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = 'complex(0,1)*I2a23*SB',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = 'complex(0,1)*I2a31*SB',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = 'complex(0,1)*I2a32*SB',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = 'complex(0,1)*I2a33*SB',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = 'complex(0,1)*I3a11*SB',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = 'complex(0,1)*I3a12*SB',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = 'complex(0,1)*I3a13*SB',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = 'complex(0,1)*I3a21*SB',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = 'complex(0,1)*I3a22*SB',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = 'complex(0,1)*I3a23*SB',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = 'complex(0,1)*I3a31*SB',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = 'complex(0,1)*I3a32*SB',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = 'complex(0,1)*I3a33*SB',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = 'complex(0,1)*I4a11*SB',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = 'complex(0,1)*I4a12*SB',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = 'complex(0,1)*I4a13*SB',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = 'complex(0,1)*I4a21*SB',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = 'complex(0,1)*I4a22*SB',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = 'complex(0,1)*I4a23*SB',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = 'complex(0,1)*I4a31*SB',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = 'complex(0,1)*I4a32*SB',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = 'complex(0,1)*I4a33*SB',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = 'CB*complex(0,1)*lam7*SB - CB*complex(0,1)*lam8*SB',
                  order = {'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = '-(CB*ee**2*complex(0,1)*RA1x2)/(2.*CW) + (ee**2*complex(0,1)*RA1x1*SB)/(2.*CW)',
                  order = {'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(CB*ee**2*complex(0,1)*RA1x1)/(2.*CW) - (ee**2*complex(0,1)*RA1x2*SB)/(2.*CW)',
                  order = {'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '-(CB*ee**2*complex(0,1)*RA2x2)/(2.*CW) + (ee**2*complex(0,1)*RA2x1*SB)/(2.*CW)',
                  order = {'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '-(CB*ee**2*complex(0,1)*RA2x1)/(2.*CW) - (ee**2*complex(0,1)*RA2x2*SB)/(2.*CW)',
                  order = {'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = '-(CB*ee**2*complex(0,1)*RA3x2)/(2.*CW) + (ee**2*complex(0,1)*RA3x1*SB)/(2.*CW)',
                  order = {'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(CB*ee**2*complex(0,1)*RA3x1)/(2.*CW) - (ee**2*complex(0,1)*RA3x2*SB)/(2.*CW)',
                  order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '-(CB**2*ee*complex(0,1)) - ee*complex(0,1)*SB**2',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '2*CB**2*ee**2*complex(0,1) + 2*ee**2*complex(0,1)*SB**2',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '-(CB**2*ee**2)/(2.*CW) - (ee**2*SB**2)/(2.*CW)',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '(CB**2*ee**2)/(2.*CW) + (ee**2*SB**2)/(2.*CW)',
                  order = {'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(CB**2*complex(0,1)*lam8) - complex(0,1)*lam7*SB**2',
                  order = {'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '-(CB**2*complex(0,1)*lam7) - complex(0,1)*lam8*SB**2',
                  order = {'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(CB**2*complex(0,1)*lam4*RA1x1*RA1x2) - CB**2*complex(0,1)*lam5*RA1x1*RA1x2 + CB*complex(0,1)*lam1*RA1x1**2*SB - CB*complex(0,1)*lam3*RA1x1**2*SB - CB*complex(0,1)*lam2*RA1x2**2*SB + CB*complex(0,1)*lam3*RA1x2**2*SB + CB*complex(0,1)*lam7*RA1x3**2*SB - CB*complex(0,1)*lam8*RA1x3**2*SB + complex(0,1)*lam4*RA1x1*RA1x2*SB**2 + complex(0,1)*lam5*RA1x1*RA1x2*SB**2',
                  order = {'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '-2*CB**2*complex(0,1)*lam5*RA1x1*RA1x2 + CB*complex(0,1)*lam1*RA1x1**2*SB - CB*complex(0,1)*lam3*RA1x1**2*SB - CB*complex(0,1)*lam4*RA1x1**2*SB + CB*complex(0,1)*lam5*RA1x1**2*SB - CB*complex(0,1)*lam2*RA1x2**2*SB + CB*complex(0,1)*lam3*RA1x2**2*SB + CB*complex(0,1)*lam4*RA1x2**2*SB - CB*complex(0,1)*lam5*RA1x2**2*SB + CB*complex(0,1)*lam7*RA1x3**2*SB - CB*complex(0,1)*lam8*RA1x3**2*SB + 2*complex(0,1)*lam5*RA1x1*RA1x2*SB**2',
                  order = {'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1**2) - CB**2*complex(0,1)*lam2*RA1x2**2 - CB**2*complex(0,1)*lam8*RA1x3**2 + 2*CB*complex(0,1)*lam4*RA1x1*RA1x2*SB + 2*CB*complex(0,1)*lam5*RA1x1*RA1x2*SB - complex(0,1)*lam1*RA1x1**2*SB**2 - complex(0,1)*lam3*RA1x2**2*SB**2 - complex(0,1)*lam7*RA1x3**2*SB**2',
                  order = {'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1**2) - CB**2*complex(0,1)*lam4*RA1x1**2 + CB**2*complex(0,1)*lam5*RA1x1**2 - CB**2*complex(0,1)*lam2*RA1x2**2 - CB**2*complex(0,1)*lam8*RA1x3**2 + 4*CB*complex(0,1)*lam5*RA1x1*RA1x2*SB - complex(0,1)*lam1*RA1x1**2*SB**2 - complex(0,1)*lam3*RA1x2**2*SB**2 - complex(0,1)*lam4*RA1x2**2*SB**2 + complex(0,1)*lam5*RA1x2**2*SB**2 - complex(0,1)*lam7*RA1x3**2*SB**2',
                  order = {'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1**2) - CB**2*complex(0,1)*lam3*RA1x2**2 - CB**2*complex(0,1)*lam7*RA1x3**2 - 2*CB*complex(0,1)*lam4*RA1x1*RA1x2*SB - 2*CB*complex(0,1)*lam5*RA1x1*RA1x2*SB - complex(0,1)*lam3*RA1x1**2*SB**2 - complex(0,1)*lam2*RA1x2**2*SB**2 - complex(0,1)*lam8*RA1x3**2*SB**2',
                  order = {'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1**2) - CB**2*complex(0,1)*lam3*RA1x2**2 - CB**2*complex(0,1)*lam4*RA1x2**2 + CB**2*complex(0,1)*lam5*RA1x2**2 - CB**2*complex(0,1)*lam7*RA1x3**2 - 4*CB*complex(0,1)*lam5*RA1x1*RA1x2*SB - complex(0,1)*lam3*RA1x1**2*SB**2 - complex(0,1)*lam4*RA1x1**2*SB**2 + complex(0,1)*lam5*RA1x1**2*SB**2 - complex(0,1)*lam2*RA1x2**2*SB**2 - complex(0,1)*lam8*RA1x3**2*SB**2',
                  order = {'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '-(CB**2*complex(0,1)*lam4*RA1x2*RA2x1)/2. - (CB**2*complex(0,1)*lam5*RA1x2*RA2x1)/2. - (CB**2*complex(0,1)*lam4*RA1x1*RA2x2)/2. - (CB**2*complex(0,1)*lam5*RA1x1*RA2x2)/2. + CB*complex(0,1)*lam1*RA1x1*RA2x1*SB - CB*complex(0,1)*lam3*RA1x1*RA2x1*SB - CB*complex(0,1)*lam2*RA1x2*RA2x2*SB + CB*complex(0,1)*lam3*RA1x2*RA2x2*SB + CB*complex(0,1)*lam7*RA1x3*RA2x3*SB - CB*complex(0,1)*lam8*RA1x3*RA2x3*SB + (complex(0,1)*lam4*RA1x2*RA2x1*SB**2)/2. + (complex(0,1)*lam5*RA1x2*RA2x1*SB**2)/2. + (complex(0,1)*lam4*RA1x1*RA2x2*SB**2)/2. + (complex(0,1)*lam5*RA1x1*RA2x2*SB**2)/2.',
                  order = {'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(CB**2*complex(0,1)*lam5*RA1x2*RA2x1) - CB**2*complex(0,1)*lam5*RA1x1*RA2x2 + CB*complex(0,1)*lam1*RA1x1*RA2x1*SB - CB*complex(0,1)*lam3*RA1x1*RA2x1*SB - CB*complex(0,1)*lam4*RA1x1*RA2x1*SB + CB*complex(0,1)*lam5*RA1x1*RA2x1*SB - CB*complex(0,1)*lam2*RA1x2*RA2x2*SB + CB*complex(0,1)*lam3*RA1x2*RA2x2*SB + CB*complex(0,1)*lam4*RA1x2*RA2x2*SB - CB*complex(0,1)*lam5*RA1x2*RA2x2*SB + CB*complex(0,1)*lam7*RA1x3*RA2x3*SB - CB*complex(0,1)*lam8*RA1x3*RA2x3*SB + complex(0,1)*lam5*RA1x2*RA2x1*SB**2 + complex(0,1)*lam5*RA1x1*RA2x2*SB**2',
                  order = {'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(CB**2*complex(0,1)*lam4*RA2x1*RA2x2) - CB**2*complex(0,1)*lam5*RA2x1*RA2x2 + CB*complex(0,1)*lam1*RA2x1**2*SB - CB*complex(0,1)*lam3*RA2x1**2*SB - CB*complex(0,1)*lam2*RA2x2**2*SB + CB*complex(0,1)*lam3*RA2x2**2*SB + CB*complex(0,1)*lam7*RA2x3**2*SB - CB*complex(0,1)*lam8*RA2x3**2*SB + complex(0,1)*lam4*RA2x1*RA2x2*SB**2 + complex(0,1)*lam5*RA2x1*RA2x2*SB**2',
                  order = {'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = '-2*CB**2*complex(0,1)*lam5*RA2x1*RA2x2 + CB*complex(0,1)*lam1*RA2x1**2*SB - CB*complex(0,1)*lam3*RA2x1**2*SB - CB*complex(0,1)*lam4*RA2x1**2*SB + CB*complex(0,1)*lam5*RA2x1**2*SB - CB*complex(0,1)*lam2*RA2x2**2*SB + CB*complex(0,1)*lam3*RA2x2**2*SB + CB*complex(0,1)*lam4*RA2x2**2*SB - CB*complex(0,1)*lam5*RA2x2**2*SB + CB*complex(0,1)*lam7*RA2x3**2*SB - CB*complex(0,1)*lam8*RA2x3**2*SB + 2*complex(0,1)*lam5*RA2x1*RA2x2*SB**2',
                  order = {'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1*RA2x1) - CB**2*complex(0,1)*lam2*RA1x2*RA2x2 - CB**2*complex(0,1)*lam8*RA1x3*RA2x3 + CB*complex(0,1)*lam4*RA1x2*RA2x1*SB + CB*complex(0,1)*lam5*RA1x2*RA2x1*SB + CB*complex(0,1)*lam4*RA1x1*RA2x2*SB + CB*complex(0,1)*lam5*RA1x1*RA2x2*SB - complex(0,1)*lam1*RA1x1*RA2x1*SB**2 - complex(0,1)*lam3*RA1x2*RA2x2*SB**2 - complex(0,1)*lam7*RA1x3*RA2x3*SB**2',
                  order = {'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1*RA2x1) - CB**2*complex(0,1)*lam4*RA1x1*RA2x1 + CB**2*complex(0,1)*lam5*RA1x1*RA2x1 - CB**2*complex(0,1)*lam2*RA1x2*RA2x2 - CB**2*complex(0,1)*lam8*RA1x3*RA2x3 + 2*CB*complex(0,1)*lam5*RA1x2*RA2x1*SB + 2*CB*complex(0,1)*lam5*RA1x1*RA2x2*SB - complex(0,1)*lam1*RA1x1*RA2x1*SB**2 - complex(0,1)*lam3*RA1x2*RA2x2*SB**2 - complex(0,1)*lam4*RA1x2*RA2x2*SB**2 + complex(0,1)*lam5*RA1x2*RA2x2*SB**2 - complex(0,1)*lam7*RA1x3*RA2x3*SB**2',
                  order = {'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1*RA2x1) - CB**2*complex(0,1)*lam3*RA1x2*RA2x2 - CB**2*complex(0,1)*lam7*RA1x3*RA2x3 - CB*complex(0,1)*lam4*RA1x2*RA2x1*SB - CB*complex(0,1)*lam5*RA1x2*RA2x1*SB - CB*complex(0,1)*lam4*RA1x1*RA2x2*SB - CB*complex(0,1)*lam5*RA1x1*RA2x2*SB - complex(0,1)*lam3*RA1x1*RA2x1*SB**2 - complex(0,1)*lam2*RA1x2*RA2x2*SB**2 - complex(0,1)*lam8*RA1x3*RA2x3*SB**2',
                  order = {'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1*RA2x1) - CB**2*complex(0,1)*lam3*RA1x2*RA2x2 - CB**2*complex(0,1)*lam4*RA1x2*RA2x2 + CB**2*complex(0,1)*lam5*RA1x2*RA2x2 - CB**2*complex(0,1)*lam7*RA1x3*RA2x3 - 2*CB*complex(0,1)*lam5*RA1x2*RA2x1*SB - 2*CB*complex(0,1)*lam5*RA1x1*RA2x2*SB - complex(0,1)*lam3*RA1x1*RA2x1*SB**2 - complex(0,1)*lam4*RA1x1*RA2x1*SB**2 + complex(0,1)*lam5*RA1x1*RA2x1*SB**2 - complex(0,1)*lam2*RA1x2*RA2x2*SB**2 - complex(0,1)*lam8*RA1x3*RA2x3*SB**2',
                  order = {'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '-(CB**2*complex(0,1)*lam3*RA2x1**2) - CB**2*complex(0,1)*lam2*RA2x2**2 - CB**2*complex(0,1)*lam8*RA2x3**2 + 2*CB*complex(0,1)*lam4*RA2x1*RA2x2*SB + 2*CB*complex(0,1)*lam5*RA2x1*RA2x2*SB - complex(0,1)*lam1*RA2x1**2*SB**2 - complex(0,1)*lam3*RA2x2**2*SB**2 - complex(0,1)*lam7*RA2x3**2*SB**2',
                  order = {'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '-(CB**2*complex(0,1)*lam3*RA2x1**2) - CB**2*complex(0,1)*lam4*RA2x1**2 + CB**2*complex(0,1)*lam5*RA2x1**2 - CB**2*complex(0,1)*lam2*RA2x2**2 - CB**2*complex(0,1)*lam8*RA2x3**2 + 4*CB*complex(0,1)*lam5*RA2x1*RA2x2*SB - complex(0,1)*lam1*RA2x1**2*SB**2 - complex(0,1)*lam3*RA2x2**2*SB**2 - complex(0,1)*lam4*RA2x2**2*SB**2 + complex(0,1)*lam5*RA2x2**2*SB**2 - complex(0,1)*lam7*RA2x3**2*SB**2',
                  order = {'QED':2})

GC_132 = Coupling(name = 'GC_132',
                  value = '-(CB**2*complex(0,1)*lam1*RA2x1**2) - CB**2*complex(0,1)*lam3*RA2x2**2 - CB**2*complex(0,1)*lam7*RA2x3**2 - 2*CB*complex(0,1)*lam4*RA2x1*RA2x2*SB - 2*CB*complex(0,1)*lam5*RA2x1*RA2x2*SB - complex(0,1)*lam3*RA2x1**2*SB**2 - complex(0,1)*lam2*RA2x2**2*SB**2 - complex(0,1)*lam8*RA2x3**2*SB**2',
                  order = {'QED':2})

GC_133 = Coupling(name = 'GC_133',
                  value = '-(CB**2*complex(0,1)*lam1*RA2x1**2) - CB**2*complex(0,1)*lam3*RA2x2**2 - CB**2*complex(0,1)*lam4*RA2x2**2 + CB**2*complex(0,1)*lam5*RA2x2**2 - CB**2*complex(0,1)*lam7*RA2x3**2 - 4*CB*complex(0,1)*lam5*RA2x1*RA2x2*SB - complex(0,1)*lam3*RA2x1**2*SB**2 - complex(0,1)*lam4*RA2x1**2*SB**2 + complex(0,1)*lam5*RA2x1**2*SB**2 - complex(0,1)*lam2*RA2x2**2*SB**2 - complex(0,1)*lam8*RA2x3**2*SB**2',
                  order = {'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = '-(CB**2*complex(0,1)*lam4*RA1x2*RA3x1)/2. - (CB**2*complex(0,1)*lam5*RA1x2*RA3x1)/2. - (CB**2*complex(0,1)*lam4*RA1x1*RA3x2)/2. - (CB**2*complex(0,1)*lam5*RA1x1*RA3x2)/2. + CB*complex(0,1)*lam1*RA1x1*RA3x1*SB - CB*complex(0,1)*lam3*RA1x1*RA3x1*SB - CB*complex(0,1)*lam2*RA1x2*RA3x2*SB + CB*complex(0,1)*lam3*RA1x2*RA3x2*SB + CB*complex(0,1)*lam7*RA1x3*RA3x3*SB - CB*complex(0,1)*lam8*RA1x3*RA3x3*SB + (complex(0,1)*lam4*RA1x2*RA3x1*SB**2)/2. + (complex(0,1)*lam5*RA1x2*RA3x1*SB**2)/2. + (complex(0,1)*lam4*RA1x1*RA3x2*SB**2)/2. + (complex(0,1)*lam5*RA1x1*RA3x2*SB**2)/2.',
                  order = {'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(CB**2*complex(0,1)*lam5*RA1x2*RA3x1) - CB**2*complex(0,1)*lam5*RA1x1*RA3x2 + CB*complex(0,1)*lam1*RA1x1*RA3x1*SB - CB*complex(0,1)*lam3*RA1x1*RA3x1*SB - CB*complex(0,1)*lam4*RA1x1*RA3x1*SB + CB*complex(0,1)*lam5*RA1x1*RA3x1*SB - CB*complex(0,1)*lam2*RA1x2*RA3x2*SB + CB*complex(0,1)*lam3*RA1x2*RA3x2*SB + CB*complex(0,1)*lam4*RA1x2*RA3x2*SB - CB*complex(0,1)*lam5*RA1x2*RA3x2*SB + CB*complex(0,1)*lam7*RA1x3*RA3x3*SB - CB*complex(0,1)*lam8*RA1x3*RA3x3*SB + complex(0,1)*lam5*RA1x2*RA3x1*SB**2 + complex(0,1)*lam5*RA1x1*RA3x2*SB**2',
                  order = {'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '-(CB**2*complex(0,1)*lam4*RA2x2*RA3x1)/2. - (CB**2*complex(0,1)*lam5*RA2x2*RA3x1)/2. - (CB**2*complex(0,1)*lam4*RA2x1*RA3x2)/2. - (CB**2*complex(0,1)*lam5*RA2x1*RA3x2)/2. + CB*complex(0,1)*lam1*RA2x1*RA3x1*SB - CB*complex(0,1)*lam3*RA2x1*RA3x1*SB - CB*complex(0,1)*lam2*RA2x2*RA3x2*SB + CB*complex(0,1)*lam3*RA2x2*RA3x2*SB + CB*complex(0,1)*lam7*RA2x3*RA3x3*SB - CB*complex(0,1)*lam8*RA2x3*RA3x3*SB + (complex(0,1)*lam4*RA2x2*RA3x1*SB**2)/2. + (complex(0,1)*lam5*RA2x2*RA3x1*SB**2)/2. + (complex(0,1)*lam4*RA2x1*RA3x2*SB**2)/2. + (complex(0,1)*lam5*RA2x1*RA3x2*SB**2)/2.',
                  order = {'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '-(CB**2*complex(0,1)*lam5*RA2x2*RA3x1) - CB**2*complex(0,1)*lam5*RA2x1*RA3x2 + CB*complex(0,1)*lam1*RA2x1*RA3x1*SB - CB*complex(0,1)*lam3*RA2x1*RA3x1*SB - CB*complex(0,1)*lam4*RA2x1*RA3x1*SB + CB*complex(0,1)*lam5*RA2x1*RA3x1*SB - CB*complex(0,1)*lam2*RA2x2*RA3x2*SB + CB*complex(0,1)*lam3*RA2x2*RA3x2*SB + CB*complex(0,1)*lam4*RA2x2*RA3x2*SB - CB*complex(0,1)*lam5*RA2x2*RA3x2*SB + CB*complex(0,1)*lam7*RA2x3*RA3x3*SB - CB*complex(0,1)*lam8*RA2x3*RA3x3*SB + complex(0,1)*lam5*RA2x2*RA3x1*SB**2 + complex(0,1)*lam5*RA2x1*RA3x2*SB**2',
                  order = {'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = '-(CB**2*complex(0,1)*lam4*RA3x1*RA3x2) - CB**2*complex(0,1)*lam5*RA3x1*RA3x2 + CB*complex(0,1)*lam1*RA3x1**2*SB - CB*complex(0,1)*lam3*RA3x1**2*SB - CB*complex(0,1)*lam2*RA3x2**2*SB + CB*complex(0,1)*lam3*RA3x2**2*SB + CB*complex(0,1)*lam7*RA3x3**2*SB - CB*complex(0,1)*lam8*RA3x3**2*SB + complex(0,1)*lam4*RA3x1*RA3x2*SB**2 + complex(0,1)*lam5*RA3x1*RA3x2*SB**2',
                  order = {'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = '-2*CB**2*complex(0,1)*lam5*RA3x1*RA3x2 + CB*complex(0,1)*lam1*RA3x1**2*SB - CB*complex(0,1)*lam3*RA3x1**2*SB - CB*complex(0,1)*lam4*RA3x1**2*SB + CB*complex(0,1)*lam5*RA3x1**2*SB - CB*complex(0,1)*lam2*RA3x2**2*SB + CB*complex(0,1)*lam3*RA3x2**2*SB + CB*complex(0,1)*lam4*RA3x2**2*SB - CB*complex(0,1)*lam5*RA3x2**2*SB + CB*complex(0,1)*lam7*RA3x3**2*SB - CB*complex(0,1)*lam8*RA3x3**2*SB + 2*complex(0,1)*lam5*RA3x1*RA3x2*SB**2',
                  order = {'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1*RA3x1) - CB**2*complex(0,1)*lam2*RA1x2*RA3x2 - CB**2*complex(0,1)*lam8*RA1x3*RA3x3 + CB*complex(0,1)*lam4*RA1x2*RA3x1*SB + CB*complex(0,1)*lam5*RA1x2*RA3x1*SB + CB*complex(0,1)*lam4*RA1x1*RA3x2*SB + CB*complex(0,1)*lam5*RA1x1*RA3x2*SB - complex(0,1)*lam1*RA1x1*RA3x1*SB**2 - complex(0,1)*lam3*RA1x2*RA3x2*SB**2 - complex(0,1)*lam7*RA1x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1*RA3x1) - CB**2*complex(0,1)*lam4*RA1x1*RA3x1 + CB**2*complex(0,1)*lam5*RA1x1*RA3x1 - CB**2*complex(0,1)*lam2*RA1x2*RA3x2 - CB**2*complex(0,1)*lam8*RA1x3*RA3x3 + 2*CB*complex(0,1)*lam5*RA1x2*RA3x1*SB + 2*CB*complex(0,1)*lam5*RA1x1*RA3x2*SB - complex(0,1)*lam1*RA1x1*RA3x1*SB**2 - complex(0,1)*lam3*RA1x2*RA3x2*SB**2 - complex(0,1)*lam4*RA1x2*RA3x2*SB**2 + complex(0,1)*lam5*RA1x2*RA3x2*SB**2 - complex(0,1)*lam7*RA1x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1*RA3x1) - CB**2*complex(0,1)*lam3*RA1x2*RA3x2 - CB**2*complex(0,1)*lam7*RA1x3*RA3x3 - CB*complex(0,1)*lam4*RA1x2*RA3x1*SB - CB*complex(0,1)*lam5*RA1x2*RA3x1*SB - CB*complex(0,1)*lam4*RA1x1*RA3x2*SB - CB*complex(0,1)*lam5*RA1x1*RA3x2*SB - complex(0,1)*lam3*RA1x1*RA3x1*SB**2 - complex(0,1)*lam2*RA1x2*RA3x2*SB**2 - complex(0,1)*lam8*RA1x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1*RA3x1) - CB**2*complex(0,1)*lam3*RA1x2*RA3x2 - CB**2*complex(0,1)*lam4*RA1x2*RA3x2 + CB**2*complex(0,1)*lam5*RA1x2*RA3x2 - CB**2*complex(0,1)*lam7*RA1x3*RA3x3 - 2*CB*complex(0,1)*lam5*RA1x2*RA3x1*SB - 2*CB*complex(0,1)*lam5*RA1x1*RA3x2*SB - complex(0,1)*lam3*RA1x1*RA3x1*SB**2 - complex(0,1)*lam4*RA1x1*RA3x1*SB**2 + complex(0,1)*lam5*RA1x1*RA3x1*SB**2 - complex(0,1)*lam2*RA1x2*RA3x2*SB**2 - complex(0,1)*lam8*RA1x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(CB**2*complex(0,1)*lam3*RA2x1*RA3x1) - CB**2*complex(0,1)*lam2*RA2x2*RA3x2 - CB**2*complex(0,1)*lam8*RA2x3*RA3x3 + CB*complex(0,1)*lam4*RA2x2*RA3x1*SB + CB*complex(0,1)*lam5*RA2x2*RA3x1*SB + CB*complex(0,1)*lam4*RA2x1*RA3x2*SB + CB*complex(0,1)*lam5*RA2x1*RA3x2*SB - complex(0,1)*lam1*RA2x1*RA3x1*SB**2 - complex(0,1)*lam3*RA2x2*RA3x2*SB**2 - complex(0,1)*lam7*RA2x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_145 = Coupling(name = 'GC_145',
                  value = '-(CB**2*complex(0,1)*lam3*RA2x1*RA3x1) - CB**2*complex(0,1)*lam4*RA2x1*RA3x1 + CB**2*complex(0,1)*lam5*RA2x1*RA3x1 - CB**2*complex(0,1)*lam2*RA2x2*RA3x2 - CB**2*complex(0,1)*lam8*RA2x3*RA3x3 + 2*CB*complex(0,1)*lam5*RA2x2*RA3x1*SB + 2*CB*complex(0,1)*lam5*RA2x1*RA3x2*SB - complex(0,1)*lam1*RA2x1*RA3x1*SB**2 - complex(0,1)*lam3*RA2x2*RA3x2*SB**2 - complex(0,1)*lam4*RA2x2*RA3x2*SB**2 + complex(0,1)*lam5*RA2x2*RA3x2*SB**2 - complex(0,1)*lam7*RA2x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(CB**2*complex(0,1)*lam1*RA2x1*RA3x1) - CB**2*complex(0,1)*lam3*RA2x2*RA3x2 - CB**2*complex(0,1)*lam7*RA2x3*RA3x3 - CB*complex(0,1)*lam4*RA2x2*RA3x1*SB - CB*complex(0,1)*lam5*RA2x2*RA3x1*SB - CB*complex(0,1)*lam4*RA2x1*RA3x2*SB - CB*complex(0,1)*lam5*RA2x1*RA3x2*SB - complex(0,1)*lam3*RA2x1*RA3x1*SB**2 - complex(0,1)*lam2*RA2x2*RA3x2*SB**2 - complex(0,1)*lam8*RA2x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_147 = Coupling(name = 'GC_147',
                  value = '-(CB**2*complex(0,1)*lam1*RA2x1*RA3x1) - CB**2*complex(0,1)*lam3*RA2x2*RA3x2 - CB**2*complex(0,1)*lam4*RA2x2*RA3x2 + CB**2*complex(0,1)*lam5*RA2x2*RA3x2 - CB**2*complex(0,1)*lam7*RA2x3*RA3x3 - 2*CB*complex(0,1)*lam5*RA2x2*RA3x1*SB - 2*CB*complex(0,1)*lam5*RA2x1*RA3x2*SB - complex(0,1)*lam3*RA2x1*RA3x1*SB**2 - complex(0,1)*lam4*RA2x1*RA3x1*SB**2 + complex(0,1)*lam5*RA2x1*RA3x1*SB**2 - complex(0,1)*lam2*RA2x2*RA3x2*SB**2 - complex(0,1)*lam8*RA2x3*RA3x3*SB**2',
                  order = {'QED':2})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(CB**2*complex(0,1)*lam3*RA3x1**2) - CB**2*complex(0,1)*lam2*RA3x2**2 - CB**2*complex(0,1)*lam8*RA3x3**2 + 2*CB*complex(0,1)*lam4*RA3x1*RA3x2*SB + 2*CB*complex(0,1)*lam5*RA3x1*RA3x2*SB - complex(0,1)*lam1*RA3x1**2*SB**2 - complex(0,1)*lam3*RA3x2**2*SB**2 - complex(0,1)*lam7*RA3x3**2*SB**2',
                  order = {'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = '-(CB**2*complex(0,1)*lam3*RA3x1**2) - CB**2*complex(0,1)*lam4*RA3x1**2 + CB**2*complex(0,1)*lam5*RA3x1**2 - CB**2*complex(0,1)*lam2*RA3x2**2 - CB**2*complex(0,1)*lam8*RA3x3**2 + 4*CB*complex(0,1)*lam5*RA3x1*RA3x2*SB - complex(0,1)*lam1*RA3x1**2*SB**2 - complex(0,1)*lam3*RA3x2**2*SB**2 - complex(0,1)*lam4*RA3x2**2*SB**2 + complex(0,1)*lam5*RA3x2**2*SB**2 - complex(0,1)*lam7*RA3x3**2*SB**2',
                  order = {'QED':2})

GC_150 = Coupling(name = 'GC_150',
                  value = '-(CB**2*complex(0,1)*lam1*RA3x1**2) - CB**2*complex(0,1)*lam3*RA3x2**2 - CB**2*complex(0,1)*lam7*RA3x3**2 - 2*CB*complex(0,1)*lam4*RA3x1*RA3x2*SB - 2*CB*complex(0,1)*lam5*RA3x1*RA3x2*SB - complex(0,1)*lam3*RA3x1**2*SB**2 - complex(0,1)*lam2*RA3x2**2*SB**2 - complex(0,1)*lam8*RA3x3**2*SB**2',
                  order = {'QED':2})

GC_151 = Coupling(name = 'GC_151',
                  value = '-(CB**2*complex(0,1)*lam1*RA3x1**2) - CB**2*complex(0,1)*lam3*RA3x2**2 - CB**2*complex(0,1)*lam4*RA3x2**2 + CB**2*complex(0,1)*lam5*RA3x2**2 - CB**2*complex(0,1)*lam7*RA3x3**2 - 4*CB*complex(0,1)*lam5*RA3x1*RA3x2*SB - complex(0,1)*lam3*RA3x1**2*SB**2 - complex(0,1)*lam4*RA3x1**2*SB**2 + complex(0,1)*lam5*RA3x1**2*SB**2 - complex(0,1)*lam2*RA3x2**2*SB**2 - complex(0,1)*lam8*RA3x3**2*SB**2',
                  order = {'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(CB**3*complex(0,1)*lam2*SB) + CB**3*complex(0,1)*lam3*SB + CB**3*complex(0,1)*lam4*SB + CB**3*complex(0,1)*lam5*SB + CB*complex(0,1)*lam1*SB**3 - CB*complex(0,1)*lam3*SB**3 - CB*complex(0,1)*lam4*SB**3 - CB*complex(0,1)*lam5*SB**3',
                  order = {'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = 'CB**3*complex(0,1)*lam1*SB - CB**3*complex(0,1)*lam3*SB - CB**3*complex(0,1)*lam4*SB - CB**3*complex(0,1)*lam5*SB - CB*complex(0,1)*lam2*SB**3 + CB*complex(0,1)*lam3*SB**3 + CB*complex(0,1)*lam4*SB**3 + CB*complex(0,1)*lam5*SB**3',
                  order = {'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '-2*CB**3*complex(0,1)*lam2*SB + 2*CB**3*complex(0,1)*lam3*SB + 2*CB**3*complex(0,1)*lam4*SB + 2*CB**3*complex(0,1)*lam5*SB + 2*CB*complex(0,1)*lam1*SB**3 - 2*CB*complex(0,1)*lam3*SB**3 - 2*CB*complex(0,1)*lam4*SB**3 - 2*CB*complex(0,1)*lam5*SB**3',
                  order = {'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = '2*CB**3*complex(0,1)*lam1*SB - 2*CB**3*complex(0,1)*lam3*SB - 2*CB**3*complex(0,1)*lam4*SB - 2*CB**3*complex(0,1)*lam5*SB - 2*CB*complex(0,1)*lam2*SB**3 + 2*CB*complex(0,1)*lam3*SB**3 + 2*CB*complex(0,1)*lam4*SB**3 + 2*CB*complex(0,1)*lam5*SB**3',
                  order = {'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '-3*CB**3*complex(0,1)*lam2*SB + 3*CB**3*complex(0,1)*lam3*SB + 3*CB**3*complex(0,1)*lam4*SB + 3*CB**3*complex(0,1)*lam5*SB + 3*CB*complex(0,1)*lam1*SB**3 - 3*CB*complex(0,1)*lam3*SB**3 - 3*CB*complex(0,1)*lam4*SB**3 - 3*CB*complex(0,1)*lam5*SB**3',
                  order = {'QED':2})

GC_157 = Coupling(name = 'GC_157',
                  value = '3*CB**3*complex(0,1)*lam1*SB - 3*CB**3*complex(0,1)*lam3*SB - 3*CB**3*complex(0,1)*lam4*SB - 3*CB**3*complex(0,1)*lam5*SB - 3*CB*complex(0,1)*lam2*SB**3 + 3*CB*complex(0,1)*lam3*SB**3 + 3*CB*complex(0,1)*lam4*SB**3 + 3*CB*complex(0,1)*lam5*SB**3',
                  order = {'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(CB**3*lam4*RA1x2)/2. + (CB**3*lam5*RA1x2)/2. + (CB**2*lam4*RA1x1*SB)/2. - (CB**2*lam5*RA1x1*SB)/2. - (CB*lam4*RA1x2*SB**2)/2. + (CB*lam5*RA1x2*SB**2)/2. + (lam4*RA1x1*SB**3)/2. - (lam5*RA1x1*SB**3)/2.',
                  order = {'QED':2})

GC_159 = Coupling(name = 'GC_159',
                  value = '(CB**3*lam4*RA1x2)/2. - (CB**3*lam5*RA1x2)/2. - (CB**2*lam4*RA1x1*SB)/2. + (CB**2*lam5*RA1x1*SB)/2. + (CB*lam4*RA1x2*SB**2)/2. - (CB*lam5*RA1x2*SB**2)/2. - (lam4*RA1x1*SB**3)/2. + (lam5*RA1x1*SB**3)/2.',
                  order = {'QED':2})

GC_160 = Coupling(name = 'GC_160',
                  value = '(CB**3*lam4*RA1x1)/2. - (CB**3*lam5*RA1x1)/2. + (CB**2*lam4*RA1x2*SB)/2. - (CB**2*lam5*RA1x2*SB)/2. + (CB*lam4*RA1x1*SB**2)/2. - (CB*lam5*RA1x1*SB**2)/2. + (lam4*RA1x2*SB**3)/2. - (lam5*RA1x2*SB**3)/2.',
                  order = {'QED':2})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(CB**3*lam4*RA1x1)/2. + (CB**3*lam5*RA1x1)/2. - (CB**2*lam4*RA1x2*SB)/2. + (CB**2*lam5*RA1x2*SB)/2. - (CB*lam4*RA1x1*SB**2)/2. + (CB*lam5*RA1x1*SB**2)/2. - (lam4*RA1x2*SB**3)/2. + (lam5*RA1x2*SB**3)/2.',
                  order = {'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(CB**3*lam4*RA2x2)/2. + (CB**3*lam5*RA2x2)/2. + (CB**2*lam4*RA2x1*SB)/2. - (CB**2*lam5*RA2x1*SB)/2. - (CB*lam4*RA2x2*SB**2)/2. + (CB*lam5*RA2x2*SB**2)/2. + (lam4*RA2x1*SB**3)/2. - (lam5*RA2x1*SB**3)/2.',
                  order = {'QED':2})

GC_163 = Coupling(name = 'GC_163',
                  value = '(CB**3*lam4*RA2x2)/2. - (CB**3*lam5*RA2x2)/2. - (CB**2*lam4*RA2x1*SB)/2. + (CB**2*lam5*RA2x1*SB)/2. + (CB*lam4*RA2x2*SB**2)/2. - (CB*lam5*RA2x2*SB**2)/2. - (lam4*RA2x1*SB**3)/2. + (lam5*RA2x1*SB**3)/2.',
                  order = {'QED':2})

GC_164 = Coupling(name = 'GC_164',
                  value = '(CB**3*lam4*RA2x1)/2. - (CB**3*lam5*RA2x1)/2. + (CB**2*lam4*RA2x2*SB)/2. - (CB**2*lam5*RA2x2*SB)/2. + (CB*lam4*RA2x1*SB**2)/2. - (CB*lam5*RA2x1*SB**2)/2. + (lam4*RA2x2*SB**3)/2. - (lam5*RA2x2*SB**3)/2.',
                  order = {'QED':2})

GC_165 = Coupling(name = 'GC_165',
                  value = '-(CB**3*lam4*RA2x1)/2. + (CB**3*lam5*RA2x1)/2. - (CB**2*lam4*RA2x2*SB)/2. + (CB**2*lam5*RA2x2*SB)/2. - (CB*lam4*RA2x1*SB**2)/2. + (CB*lam5*RA2x1*SB**2)/2. - (lam4*RA2x2*SB**3)/2. + (lam5*RA2x2*SB**3)/2.',
                  order = {'QED':2})

GC_166 = Coupling(name = 'GC_166',
                  value = '-(CB**3*lam4*RA3x2)/2. + (CB**3*lam5*RA3x2)/2. + (CB**2*lam4*RA3x1*SB)/2. - (CB**2*lam5*RA3x1*SB)/2. - (CB*lam4*RA3x2*SB**2)/2. + (CB*lam5*RA3x2*SB**2)/2. + (lam4*RA3x1*SB**3)/2. - (lam5*RA3x1*SB**3)/2.',
                  order = {'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '(CB**3*lam4*RA3x2)/2. - (CB**3*lam5*RA3x2)/2. - (CB**2*lam4*RA3x1*SB)/2. + (CB**2*lam5*RA3x1*SB)/2. + (CB*lam4*RA3x2*SB**2)/2. - (CB*lam5*RA3x2*SB**2)/2. - (lam4*RA3x1*SB**3)/2. + (lam5*RA3x1*SB**3)/2.',
                  order = {'QED':2})

GC_168 = Coupling(name = 'GC_168',
                  value = '(CB**3*lam4*RA3x1)/2. - (CB**3*lam5*RA3x1)/2. + (CB**2*lam4*RA3x2*SB)/2. - (CB**2*lam5*RA3x2*SB)/2. + (CB*lam4*RA3x1*SB**2)/2. - (CB*lam5*RA3x1*SB**2)/2. + (lam4*RA3x2*SB**3)/2. - (lam5*RA3x2*SB**3)/2.',
                  order = {'QED':2})

GC_169 = Coupling(name = 'GC_169',
                  value = '-(CB**3*lam4*RA3x1)/2. + (CB**3*lam5*RA3x1)/2. - (CB**2*lam4*RA3x2*SB)/2. + (CB**2*lam5*RA3x2*SB)/2. - (CB*lam4*RA3x1*SB**2)/2. + (CB*lam5*RA3x1*SB**2)/2. - (lam4*RA3x2*SB**3)/2. + (lam5*RA3x2*SB**3)/2.',
                  order = {'QED':2})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(CB**4*complex(0,1)*lam2) - 2*CB**2*complex(0,1)*lam3*SB**2 - 2*CB**2*complex(0,1)*lam4*SB**2 - 2*CB**2*complex(0,1)*lam5*SB**2 - complex(0,1)*lam1*SB**4',
                  order = {'QED':2})

GC_171 = Coupling(name = 'GC_171',
                  value = '-2*CB**4*complex(0,1)*lam2 - 4*CB**2*complex(0,1)*lam3*SB**2 - 4*CB**2*complex(0,1)*lam4*SB**2 - 4*CB**2*complex(0,1)*lam5*SB**2 - 2*complex(0,1)*lam1*SB**4',
                  order = {'QED':2})

GC_172 = Coupling(name = 'GC_172',
                  value = '-3*CB**4*complex(0,1)*lam2 - 6*CB**2*complex(0,1)*lam3*SB**2 - 6*CB**2*complex(0,1)*lam4*SB**2 - 6*CB**2*complex(0,1)*lam5*SB**2 - 3*complex(0,1)*lam1*SB**4',
                  order = {'QED':2})

GC_173 = Coupling(name = 'GC_173',
                  value = '-(CB**4*complex(0,1)*lam1) - 2*CB**2*complex(0,1)*lam3*SB**2 - 2*CB**2*complex(0,1)*lam4*SB**2 - 2*CB**2*complex(0,1)*lam5*SB**2 - complex(0,1)*lam2*SB**4',
                  order = {'QED':2})

GC_174 = Coupling(name = 'GC_174',
                  value = '-2*CB**4*complex(0,1)*lam1 - 4*CB**2*complex(0,1)*lam3*SB**2 - 4*CB**2*complex(0,1)*lam4*SB**2 - 4*CB**2*complex(0,1)*lam5*SB**2 - 2*complex(0,1)*lam2*SB**4',
                  order = {'QED':2})

GC_175 = Coupling(name = 'GC_175',
                  value = '-3*CB**4*complex(0,1)*lam1 - 6*CB**2*complex(0,1)*lam3*SB**2 - 6*CB**2*complex(0,1)*lam4*SB**2 - 6*CB**2*complex(0,1)*lam5*SB**2 - 3*complex(0,1)*lam2*SB**4',
                  order = {'QED':2})

GC_176 = Coupling(name = 'GC_176',
                  value = '-(CB**4*complex(0,1)*lam3) - CB**2*complex(0,1)*lam1*SB**2 - CB**2*complex(0,1)*lam2*SB**2 + 2*CB**2*complex(0,1)*lam4*SB**2 + 2*CB**2*complex(0,1)*lam5*SB**2 - complex(0,1)*lam3*SB**4',
                  order = {'QED':2})

GC_177 = Coupling(name = 'GC_177',
                  value = '-(CB**4*complex(0,1)*lam3) - CB**4*complex(0,1)*lam4 - 2*CB**2*complex(0,1)*lam1*SB**2 - 2*CB**2*complex(0,1)*lam2*SB**2 + 2*CB**2*complex(0,1)*lam3*SB**2 + 2*CB**2*complex(0,1)*lam4*SB**2 + 4*CB**2*complex(0,1)*lam5*SB**2 - complex(0,1)*lam3*SB**4 - complex(0,1)*lam4*SB**4',
                  order = {'QED':2})

GC_178 = Coupling(name = 'GC_178',
                  value = '-(CB**4*complex(0,1)*lam4)/2. - (CB**4*complex(0,1)*lam5)/2. - CB**2*complex(0,1)*lam1*SB**2 - CB**2*complex(0,1)*lam2*SB**2 + 2*CB**2*complex(0,1)*lam3*SB**2 + CB**2*complex(0,1)*lam4*SB**2 + CB**2*complex(0,1)*lam5*SB**2 - (complex(0,1)*lam4*SB**4)/2. - (complex(0,1)*lam5*SB**4)/2.',
                  order = {'QED':2})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(CB**4*complex(0,1)*lam3) - CB**4*complex(0,1)*lam4 - CB**4*complex(0,1)*lam5 - 3*CB**2*complex(0,1)*lam1*SB**2 - 3*CB**2*complex(0,1)*lam2*SB**2 + 4*CB**2*complex(0,1)*lam3*SB**2 + 4*CB**2*complex(0,1)*lam4*SB**2 + 4*CB**2*complex(0,1)*lam5*SB**2 - complex(0,1)*lam3*SB**4 - complex(0,1)*lam4*SB**4 - complex(0,1)*lam5*SB**4',
                  order = {'QED':2})

GC_180 = Coupling(name = 'GC_180',
                  value = '-2*CB**4*complex(0,1)*lam5 - 2*CB**2*complex(0,1)*lam1*SB**2 - 2*CB**2*complex(0,1)*lam2*SB**2 + 4*CB**2*complex(0,1)*lam3*SB**2 + 4*CB**2*complex(0,1)*lam4*SB**2 - 2*complex(0,1)*lam5*SB**4',
                  order = {'QED':2})

GC_181 = Coupling(name = 'GC_181',
                  value = '(ee**2*complex(0,1)*RA1x1**2)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x2**2)/(2.*SW**2)',
                  order = {'QED':2})

GC_182 = Coupling(name = 'GC_182',
                  value = '(ee**2*complex(0,1)*RA1x1*RA2x1)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x2*RA2x2)/(2.*SW**2)',
                  order = {'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = '(ee**2*complex(0,1)*RA2x1**2)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x2**2)/(2.*SW**2)',
                  order = {'QED':2})

GC_184 = Coupling(name = 'GC_184',
                  value = '(ee**2*complex(0,1)*RA1x1*RA3x1)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x2*RA3x2)/(2.*SW**2)',
                  order = {'QED':2})

GC_185 = Coupling(name = 'GC_185',
                  value = '(ee**2*complex(0,1)*RA2x1*RA3x1)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x2*RA3x2)/(2.*SW**2)',
                  order = {'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = '(ee**2*complex(0,1)*RA3x1**2)/(2.*SW**2) + (ee**2*complex(0,1)*RA3x2**2)/(2.*SW**2)',
                  order = {'QED':2})

GC_187 = Coupling(name = 'GC_187',
                  value = '(CB**2*ee**2*complex(0,1))/(2.*SW**2) + (ee**2*complex(0,1)*SB**2)/(2.*SW**2)',
                  order = {'QED':2})

GC_188 = Coupling(name = 'GC_188',
                  value = '(CB*ee*complex(0,1)*RA1x2)/(2.*SW) - (ee*complex(0,1)*RA1x1*SB)/(2.*SW)',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-(CB*ee*complex(0,1)*RA1x2)/(2.*SW) + (ee*complex(0,1)*RA1x1*SB)/(2.*SW)',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '(CB*ee**2*complex(0,1)*RA1x2)/(2.*SW) - (ee**2*complex(0,1)*RA1x1*SB)/(2.*SW)',
                  order = {'QED':2})

GC_191 = Coupling(name = 'GC_191',
                  value = '-(CB*ee*complex(0,1)*RA1x1)/(2.*SW) - (ee*complex(0,1)*RA1x2*SB)/(2.*SW)',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '(CB*ee*complex(0,1)*RA1x1)/(2.*SW) + (ee*complex(0,1)*RA1x2*SB)/(2.*SW)',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(CB*ee**2*complex(0,1)*RA1x1)/(2.*SW) + (ee**2*complex(0,1)*RA1x2*SB)/(2.*SW)',
                  order = {'QED':2})

GC_194 = Coupling(name = 'GC_194',
                  value = '(CB*ee*complex(0,1)*RA2x2)/(2.*SW) - (ee*complex(0,1)*RA2x1*SB)/(2.*SW)',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(CB*ee*complex(0,1)*RA2x2)/(2.*SW) + (ee*complex(0,1)*RA2x1*SB)/(2.*SW)',
                  order = {'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '(CB*ee**2*complex(0,1)*RA2x2)/(2.*SW) - (ee**2*complex(0,1)*RA2x1*SB)/(2.*SW)',
                  order = {'QED':2})

GC_197 = Coupling(name = 'GC_197',
                  value = '-(CB*ee*complex(0,1)*RA2x1)/(2.*SW) - (ee*complex(0,1)*RA2x2*SB)/(2.*SW)',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '(CB*ee*complex(0,1)*RA2x1)/(2.*SW) + (ee*complex(0,1)*RA2x2*SB)/(2.*SW)',
                  order = {'QED':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '(CB*ee**2*complex(0,1)*RA2x1)/(2.*SW) + (ee**2*complex(0,1)*RA2x2*SB)/(2.*SW)',
                  order = {'QED':2})

GC_200 = Coupling(name = 'GC_200',
                  value = '(CB*ee*complex(0,1)*RA3x2)/(2.*SW) - (ee*complex(0,1)*RA3x1*SB)/(2.*SW)',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(CB*ee*complex(0,1)*RA3x2)/(2.*SW) + (ee*complex(0,1)*RA3x1*SB)/(2.*SW)',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '(CB*ee**2*complex(0,1)*RA3x2)/(2.*SW) - (ee**2*complex(0,1)*RA3x1*SB)/(2.*SW)',
                  order = {'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = '-(CB*ee*complex(0,1)*RA3x1)/(2.*SW) - (ee*complex(0,1)*RA3x2*SB)/(2.*SW)',
                  order = {'QED':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '(CB*ee*complex(0,1)*RA3x1)/(2.*SW) + (ee*complex(0,1)*RA3x2*SB)/(2.*SW)',
                  order = {'QED':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '(CB*ee**2*complex(0,1)*RA3x1)/(2.*SW) + (ee**2*complex(0,1)*RA3x2*SB)/(2.*SW)',
                  order = {'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = '(CB**2*ee)/(2.*SW) + (ee*SB**2)/(2.*SW)',
                  order = {'QED':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-(CB**2*ee**2)/(2.*SW) - (ee**2*SB**2)/(2.*SW)',
                  order = {'QED':2})

GC_208 = Coupling(name = 'GC_208',
                  value = '(CB**2*ee**2)/(2.*SW) + (ee**2*SB**2)/(2.*SW)',
                  order = {'QED':2})

GC_209 = Coupling(name = 'GC_209',
                  value = '-((ee**2*complex(0,1))/SW**2)',
                  order = {'QED':2})

GC_210 = Coupling(name = 'GC_210',
                  value = '(CW**2*ee**2*complex(0,1))/SW**2',
                  order = {'QED':2})

GC_211 = Coupling(name = 'GC_211',
                  value = '(-2*CW**2*ee**2*complex(0,1))/SW**2',
                  order = {'QED':2})

GC_212 = Coupling(name = 'GC_212',
                  value = '(ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '(CKM1x1*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '(CKM1x2*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '(CKM1x3*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '(CKM2x1*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(CKM2x2*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '(CKM2x3*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '(CKM3x1*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '(CKM3x2*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '(CKM3x3*ee*complex(0,1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '-((CW*ee*complex(0,1))/SW)',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '(CW*ee*complex(0,1))/SW',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '(CW*ee**2*complex(0,1))/SW',
                  order = {'QED':2})

GC_225 = Coupling(name = 'GC_225',
                  value = '(-2*CW*ee**2*complex(0,1))/SW',
                  order = {'QED':2})

GC_226 = Coupling(name = 'GC_226',
                  value = '(ee*complex(0,1)*SW)/(3.*CW)',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '(-2*ee*complex(0,1)*SW)/(3.*CW)',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '(ee*complex(0,1)*SW)/CW',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '-(CW*ee*complex(0,1))/(2.*SW) - (ee*complex(0,1)*SW)/(6.*CW)',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '(CW*ee*complex(0,1))/(2.*SW) - (ee*complex(0,1)*SW)/(6.*CW)',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '-(CW*ee*complex(0,1))/(2.*SW) + (ee*complex(0,1)*SW)/(2.*CW)',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(CW*ee*complex(0,1))/(2.*SW) + (ee*complex(0,1)*SW)/(2.*CW)',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '-(CB*CW*ee*RA1x2)/(2.*SW) + (CW*ee*RA1x1*SB)/(2.*SW) - (CB*ee*RA1x2*SW)/(2.*CW) + (ee*RA1x1*SB*SW)/(2.*CW)',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '-(CB*CW*ee*RA1x1)/(2.*SW) - (CW*ee*RA1x2*SB)/(2.*SW) - (CB*ee*RA1x1*SW)/(2.*CW) - (ee*RA1x2*SB*SW)/(2.*CW)',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-(CB*CW*ee*RA2x2)/(2.*SW) + (CW*ee*RA2x1*SB)/(2.*SW) - (CB*ee*RA2x2*SW)/(2.*CW) + (ee*RA2x1*SB*SW)/(2.*CW)',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '-(CB*CW*ee*RA2x1)/(2.*SW) - (CW*ee*RA2x2*SB)/(2.*SW) - (CB*ee*RA2x1*SW)/(2.*CW) - (ee*RA2x2*SB*SW)/(2.*CW)',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '-(CB*CW*ee*RA3x2)/(2.*SW) + (CW*ee*RA3x1*SB)/(2.*SW) - (CB*ee*RA3x2*SW)/(2.*CW) + (ee*RA3x1*SB*SW)/(2.*CW)',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '-(CB*CW*ee*RA3x1)/(2.*SW) - (CW*ee*RA3x2*SB)/(2.*SW) - (CB*ee*RA3x1*SW)/(2.*CW) - (ee*RA3x2*SB*SW)/(2.*CW)',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '-(CB**2*CW*ee*complex(0,1))/(2.*SW) - (CW*ee*complex(0,1)*SB**2)/(2.*SW) + (CB**2*ee*complex(0,1)*SW)/(2.*CW) + (ee*complex(0,1)*SB**2*SW)/(2.*CW)',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '(CB**2*CW*ee**2*complex(0,1))/SW + (CW*ee**2*complex(0,1)*SB**2)/SW - (CB**2*ee**2*complex(0,1)*SW)/CW - (ee**2*complex(0,1)*SB**2*SW)/CW',
                  order = {'QED':2})

GC_241 = Coupling(name = 'GC_241',
                  value = 'ee**2*complex(0,1)*RA1x1**2 + ee**2*complex(0,1)*RA1x2**2 + (CW**2*ee**2*complex(0,1)*RA1x1**2)/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*RA1x2**2)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x1**2*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*RA1x2**2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_242 = Coupling(name = 'GC_242',
                  value = 'ee**2*complex(0,1)*RA1x1*RA2x1 + ee**2*complex(0,1)*RA1x2*RA2x2 + (CW**2*ee**2*complex(0,1)*RA1x1*RA2x1)/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*RA1x2*RA2x2)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x1*RA2x1*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*RA1x2*RA2x2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_243 = Coupling(name = 'GC_243',
                  value = 'ee**2*complex(0,1)*RA2x1**2 + ee**2*complex(0,1)*RA2x2**2 + (CW**2*ee**2*complex(0,1)*RA2x1**2)/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*RA2x2**2)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x1**2*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*RA2x2**2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_244 = Coupling(name = 'GC_244',
                  value = 'ee**2*complex(0,1)*RA1x1*RA3x1 + ee**2*complex(0,1)*RA1x2*RA3x2 + (CW**2*ee**2*complex(0,1)*RA1x1*RA3x1)/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*RA1x2*RA3x2)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x1*RA3x1*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*RA1x2*RA3x2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_245 = Coupling(name = 'GC_245',
                  value = 'ee**2*complex(0,1)*RA2x1*RA3x1 + ee**2*complex(0,1)*RA2x2*RA3x2 + (CW**2*ee**2*complex(0,1)*RA2x1*RA3x1)/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*RA2x2*RA3x2)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x1*RA3x1*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*RA2x2*RA3x2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_246 = Coupling(name = 'GC_246',
                  value = 'ee**2*complex(0,1)*RA3x1**2 + ee**2*complex(0,1)*RA3x2**2 + (CW**2*ee**2*complex(0,1)*RA3x1**2)/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*RA3x2**2)/(2.*SW**2) + (ee**2*complex(0,1)*RA3x1**2*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*RA3x2**2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_247 = Coupling(name = 'GC_247',
                  value = '-(CB**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*SB**2 + (CB**2*CW**2*ee**2*complex(0,1))/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*SB**2)/(2.*SW**2) + (CB**2*ee**2*complex(0,1)*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*SB**2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_248 = Coupling(name = 'GC_248',
                  value = 'CB**2*ee**2*complex(0,1) + ee**2*complex(0,1)*SB**2 + (CB**2*CW**2*ee**2*complex(0,1))/(2.*SW**2) + (CW**2*ee**2*complex(0,1)*SB**2)/(2.*SW**2) + (CB**2*ee**2*complex(0,1)*SW**2)/(2.*CW**2) + (ee**2*complex(0,1)*SB**2*SW**2)/(2.*CW**2)',
                  order = {'QED':2})

GC_249 = Coupling(name = 'GC_249',
                  value = '(ee**2*complex(0,1)*SB*v1)/(2.*CW) - (CB*ee**2*complex(0,1)*v2)/(2.*CW)',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-(CB*ee**2*complex(0,1)*v1)/(2.*CW) - (ee**2*complex(0,1)*SB*v2)/(2.*CW)',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '-(CB**2*lam4*SB*v1)/2. + (CB**2*lam5*SB*v1)/2. - (lam4*SB**3*v1)/2. + (lam5*SB**3*v1)/2. + (CB**3*lam4*v2)/2. - (CB**3*lam5*v2)/2. + (CB*lam4*SB**2*v2)/2. - (CB*lam5*SB**2*v2)/2.',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '(CB**2*lam4*SB*v1)/2. - (CB**2*lam5*SB*v1)/2. + (lam4*SB**3*v1)/2. - (lam5*SB**3*v1)/2. - (CB**3*lam4*v2)/2. + (CB**3*lam5*v2)/2. - (CB*lam4*SB**2*v2)/2. + (CB*lam5*SB**2*v2)/2.',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(CB**3*lam4*v1)/2. - (CB**3*lam5*v1)/2. + (CB*lam4*SB**2*v1)/2. - (CB*lam5*SB**2*v1)/2. + (CB**2*lam4*SB*v2)/2. - (CB**2*lam5*SB*v2)/2. + (lam4*SB**3*v2)/2. - (lam5*SB**3*v2)/2.',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '-(CB**3*lam4*v1)/2. + (CB**3*lam5*v1)/2. - (CB*lam4*SB**2*v1)/2. + (CB*lam5*SB**2*v1)/2. - (CB**2*lam4*SB*v2)/2. + (CB**2*lam5*SB*v2)/2. - (lam4*SB**3*v2)/2. + (lam5*SB**3*v2)/2.',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(ee**2*complex(0,1)*RA1x1*v1)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x2*v2)/(2.*SW**2)',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(ee**2*complex(0,1)*RA2x1*v1)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x2*v2)/(2.*SW**2)',
                  order = {'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(ee**2*complex(0,1)*RA3x1*v1)/(2.*SW**2) + (ee**2*complex(0,1)*RA3x2*v2)/(2.*SW**2)',
                  order = {'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '-(ee**2*complex(0,1)*SB*v1)/(2.*SW) + (CB*ee**2*complex(0,1)*v2)/(2.*SW)',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(CB*ee**2*complex(0,1)*v1)/(2.*SW) + (ee**2*complex(0,1)*SB*v2)/(2.*SW)',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = 'ee**2*complex(0,1)*RA1x1*v1 + (CW**2*ee**2*complex(0,1)*RA1x1*v1)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x1*SW**2*v1)/(2.*CW**2) + ee**2*complex(0,1)*RA1x2*v2 + (CW**2*ee**2*complex(0,1)*RA1x2*v2)/(2.*SW**2) + (ee**2*complex(0,1)*RA1x2*SW**2*v2)/(2.*CW**2)',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = 'ee**2*complex(0,1)*RA2x1*v1 + (CW**2*ee**2*complex(0,1)*RA2x1*v1)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x1*SW**2*v1)/(2.*CW**2) + ee**2*complex(0,1)*RA2x2*v2 + (CW**2*ee**2*complex(0,1)*RA2x2*v2)/(2.*SW**2) + (ee**2*complex(0,1)*RA2x2*SW**2*v2)/(2.*CW**2)',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = 'ee**2*complex(0,1)*RA3x1*v1 + (CW**2*ee**2*complex(0,1)*RA3x1*v1)/(2.*SW**2) + (ee**2*complex(0,1)*RA3x1*SW**2*v1)/(2.*CW**2) + ee**2*complex(0,1)*RA3x2*v2 + (CW**2*ee**2*complex(0,1)*RA3x2*v2)/(2.*SW**2) + (ee**2*complex(0,1)*RA3x2*SW**2*v2)/(2.*CW**2)',
                  order = {'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '-(ee**2*vH)/(2.*SW)',
                  order = {'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '(ee**2*vH)/(2.*SW)',
                  order = {'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '-(ee**2*vH)/(4.*CW) - (CW*ee**2*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '(ee**2*vH)/(4.*CW) - (CW*ee**2*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '-(ee**2*vH)/(4.*CW) + (CW*ee**2*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(ee**2*vH)/(4.*CW) + (CW*ee**2*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '-(CB*ee**2*complex(0,1)*RA1x1*vH)/(4.*SW**2) - (ee**2*complex(0,1)*RA1x2*SB*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '-(CB*ee**2*complex(0,1)*RA2x1*vH)/(4.*SW**2) - (ee**2*complex(0,1)*RA2x2*SB*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-(CB*ee**2*complex(0,1)*RA3x1*vH)/(4.*SW**2) - (ee**2*complex(0,1)*RA3x2*SB*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '-(CB**2*ee**2*vH)/(4.*SW**2) - (ee**2*SB**2*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(CB**2*ee**2*vH)/(4.*SW**2) + (ee**2*SB**2*vH)/(4.*SW**2)',
                  order = {'QED':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-(CB*ee**2*complex(0,1)*RA1x1*vH)/2. - (ee**2*complex(0,1)*RA1x2*SB*vH)/2. - (CB*CW**2*ee**2*complex(0,1)*RA1x1*vH)/(4.*SW**2) - (CW**2*ee**2*complex(0,1)*RA1x2*SB*vH)/(4.*SW**2) - (CB*ee**2*complex(0,1)*RA1x1*SW**2*vH)/(4.*CW**2) - (ee**2*complex(0,1)*RA1x2*SB*SW**2*vH)/(4.*CW**2)',
                  order = {'QED':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '-(CB*ee**2*complex(0,1)*RA2x1*vH)/2. - (ee**2*complex(0,1)*RA2x2*SB*vH)/2. - (CB*CW**2*ee**2*complex(0,1)*RA2x1*vH)/(4.*SW**2) - (CW**2*ee**2*complex(0,1)*RA2x2*SB*vH)/(4.*SW**2) - (CB*ee**2*complex(0,1)*RA2x1*SW**2*vH)/(4.*CW**2) - (ee**2*complex(0,1)*RA2x2*SB*SW**2*vH)/(4.*CW**2)',
                  order = {'QED':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '-(CB*ee**2*complex(0,1)*RA3x1*vH)/2. - (ee**2*complex(0,1)*RA3x2*SB*vH)/2. - (CB*CW**2*ee**2*complex(0,1)*RA3x1*vH)/(4.*SW**2) - (CW**2*ee**2*complex(0,1)*RA3x2*SB*vH)/(4.*SW**2) - (CB*ee**2*complex(0,1)*RA3x1*SW**2*vH)/(4.*CW**2) - (ee**2*complex(0,1)*RA3x2*SB*SW**2*vH)/(4.*CW**2)',
                  order = {'QED':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '-(complex(0,1)*lam7*RA1x1*v1) - complex(0,1)*lam8*RA1x2*v2 - complex(0,1)*lam6*RA1x3*vS',
                  order = {'QED':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '-3*complex(0,1)*lam1*RA1x1**3*v1 - 3*complex(0,1)*lam3*RA1x1*RA1x2**2*v1 - 3*complex(0,1)*lam4*RA1x1*RA1x2**2*v1 - 3*complex(0,1)*lam5*RA1x1*RA1x2**2*v1 - 3*complex(0,1)*lam7*RA1x1*RA1x3**2*v1 - 3*complex(0,1)*lam3*RA1x1**2*RA1x2*v2 - 3*complex(0,1)*lam4*RA1x1**2*RA1x2*v2 - 3*complex(0,1)*lam5*RA1x1**2*RA1x2*v2 - 3*complex(0,1)*lam2*RA1x2**3*v2 - 3*complex(0,1)*lam8*RA1x2*RA1x3**2*v2 - 3*complex(0,1)*lam7*RA1x1**2*RA1x3*vS - 3*complex(0,1)*lam8*RA1x2**2*RA1x3*vS - 3*complex(0,1)*lam6*RA1x3**3*vS',
                  order = {'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '-(complex(0,1)*lam7*RA2x1*v1) - complex(0,1)*lam8*RA2x2*v2 - complex(0,1)*lam6*RA2x3*vS',
                  order = {'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '-3*complex(0,1)*lam1*RA1x1**2*RA2x1*v1 - complex(0,1)*lam3*RA1x2**2*RA2x1*v1 - complex(0,1)*lam4*RA1x2**2*RA2x1*v1 - complex(0,1)*lam5*RA1x2**2*RA2x1*v1 - complex(0,1)*lam7*RA1x3**2*RA2x1*v1 - 2*complex(0,1)*lam3*RA1x1*RA1x2*RA2x2*v1 - 2*complex(0,1)*lam4*RA1x1*RA1x2*RA2x2*v1 - 2*complex(0,1)*lam5*RA1x1*RA1x2*RA2x2*v1 - 2*complex(0,1)*lam7*RA1x1*RA1x3*RA2x3*v1 - 2*complex(0,1)*lam3*RA1x1*RA1x2*RA2x1*v2 - 2*complex(0,1)*lam4*RA1x1*RA1x2*RA2x1*v2 - 2*complex(0,1)*lam5*RA1x1*RA1x2*RA2x1*v2 - complex(0,1)*lam3*RA1x1**2*RA2x2*v2 - complex(0,1)*lam4*RA1x1**2*RA2x2*v2 - complex(0,1)*lam5*RA1x1**2*RA2x2*v2 - 3*complex(0,1)*lam2*RA1x2**2*RA2x2*v2 - complex(0,1)*lam8*RA1x3**2*RA2x2*v2 - 2*complex(0,1)*lam8*RA1x2*RA1x3*RA2x3*v2 - 2*complex(0,1)*lam7*RA1x1*RA1x3*RA2x1*vS - 2*complex(0,1)*lam8*RA1x2*RA1x3*RA2x2*vS - complex(0,1)*lam7*RA1x1**2*RA2x3*vS - complex(0,1)*lam8*RA1x2**2*RA2x3*vS - 3*complex(0,1)*lam6*RA1x3**2*RA2x3*vS',
                  order = {'QED':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '-3*complex(0,1)*lam1*RA1x1*RA2x1**2*v1 - 2*complex(0,1)*lam3*RA1x2*RA2x1*RA2x2*v1 - 2*complex(0,1)*lam4*RA1x2*RA2x1*RA2x2*v1 - 2*complex(0,1)*lam5*RA1x2*RA2x1*RA2x2*v1 - complex(0,1)*lam3*RA1x1*RA2x2**2*v1 - complex(0,1)*lam4*RA1x1*RA2x2**2*v1 - complex(0,1)*lam5*RA1x1*RA2x2**2*v1 - 2*complex(0,1)*lam7*RA1x3*RA2x1*RA2x3*v1 - complex(0,1)*lam7*RA1x1*RA2x3**2*v1 - complex(0,1)*lam3*RA1x2*RA2x1**2*v2 - complex(0,1)*lam4*RA1x2*RA2x1**2*v2 - complex(0,1)*lam5*RA1x2*RA2x1**2*v2 - 2*complex(0,1)*lam3*RA1x1*RA2x1*RA2x2*v2 - 2*complex(0,1)*lam4*RA1x1*RA2x1*RA2x2*v2 - 2*complex(0,1)*lam5*RA1x1*RA2x1*RA2x2*v2 - 3*complex(0,1)*lam2*RA1x2*RA2x2**2*v2 - 2*complex(0,1)*lam8*RA1x3*RA2x2*RA2x3*v2 - complex(0,1)*lam8*RA1x2*RA2x3**2*v2 - complex(0,1)*lam7*RA1x3*RA2x1**2*vS - complex(0,1)*lam8*RA1x3*RA2x2**2*vS - 2*complex(0,1)*lam7*RA1x1*RA2x1*RA2x3*vS - 2*complex(0,1)*lam8*RA1x2*RA2x2*RA2x3*vS - 3*complex(0,1)*lam6*RA1x3*RA2x3**2*vS',
                  order = {'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '-3*complex(0,1)*lam1*RA2x1**3*v1 - 3*complex(0,1)*lam3*RA2x1*RA2x2**2*v1 - 3*complex(0,1)*lam4*RA2x1*RA2x2**2*v1 - 3*complex(0,1)*lam5*RA2x1*RA2x2**2*v1 - 3*complex(0,1)*lam7*RA2x1*RA2x3**2*v1 - 3*complex(0,1)*lam3*RA2x1**2*RA2x2*v2 - 3*complex(0,1)*lam4*RA2x1**2*RA2x2*v2 - 3*complex(0,1)*lam5*RA2x1**2*RA2x2*v2 - 3*complex(0,1)*lam2*RA2x2**3*v2 - 3*complex(0,1)*lam8*RA2x2*RA2x3**2*v2 - 3*complex(0,1)*lam7*RA2x1**2*RA2x3*vS - 3*complex(0,1)*lam8*RA2x2**2*RA2x3*vS - 3*complex(0,1)*lam6*RA2x3**3*vS',
                  order = {'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-(complex(0,1)*lam7*RA3x1*v1) - complex(0,1)*lam8*RA3x2*v2 - complex(0,1)*lam6*RA3x3*vS',
                  order = {'QED':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '-3*complex(0,1)*lam1*RA1x1**2*RA3x1*v1 - complex(0,1)*lam3*RA1x2**2*RA3x1*v1 - complex(0,1)*lam4*RA1x2**2*RA3x1*v1 - complex(0,1)*lam5*RA1x2**2*RA3x1*v1 - complex(0,1)*lam7*RA1x3**2*RA3x1*v1 - 2*complex(0,1)*lam3*RA1x1*RA1x2*RA3x2*v1 - 2*complex(0,1)*lam4*RA1x1*RA1x2*RA3x2*v1 - 2*complex(0,1)*lam5*RA1x1*RA1x2*RA3x2*v1 - 2*complex(0,1)*lam7*RA1x1*RA1x3*RA3x3*v1 - 2*complex(0,1)*lam3*RA1x1*RA1x2*RA3x1*v2 - 2*complex(0,1)*lam4*RA1x1*RA1x2*RA3x1*v2 - 2*complex(0,1)*lam5*RA1x1*RA1x2*RA3x1*v2 - complex(0,1)*lam3*RA1x1**2*RA3x2*v2 - complex(0,1)*lam4*RA1x1**2*RA3x2*v2 - complex(0,1)*lam5*RA1x1**2*RA3x2*v2 - 3*complex(0,1)*lam2*RA1x2**2*RA3x2*v2 - complex(0,1)*lam8*RA1x3**2*RA3x2*v2 - 2*complex(0,1)*lam8*RA1x2*RA1x3*RA3x3*v2 - 2*complex(0,1)*lam7*RA1x1*RA1x3*RA3x1*vS - 2*complex(0,1)*lam8*RA1x2*RA1x3*RA3x2*vS - complex(0,1)*lam7*RA1x1**2*RA3x3*vS - complex(0,1)*lam8*RA1x2**2*RA3x3*vS - 3*complex(0,1)*lam6*RA1x3**2*RA3x3*vS',
                  order = {'QED':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '-3*complex(0,1)*lam1*RA1x1*RA2x1*RA3x1*v1 - complex(0,1)*lam3*RA1x2*RA2x2*RA3x1*v1 - complex(0,1)*lam4*RA1x2*RA2x2*RA3x1*v1 - complex(0,1)*lam5*RA1x2*RA2x2*RA3x1*v1 - complex(0,1)*lam7*RA1x3*RA2x3*RA3x1*v1 - complex(0,1)*lam3*RA1x2*RA2x1*RA3x2*v1 - complex(0,1)*lam4*RA1x2*RA2x1*RA3x2*v1 - complex(0,1)*lam5*RA1x2*RA2x1*RA3x2*v1 - complex(0,1)*lam3*RA1x1*RA2x2*RA3x2*v1 - complex(0,1)*lam4*RA1x1*RA2x2*RA3x2*v1 - complex(0,1)*lam5*RA1x1*RA2x2*RA3x2*v1 - complex(0,1)*lam7*RA1x3*RA2x1*RA3x3*v1 - complex(0,1)*lam7*RA1x1*RA2x3*RA3x3*v1 - complex(0,1)*lam3*RA1x2*RA2x1*RA3x1*v2 - complex(0,1)*lam4*RA1x2*RA2x1*RA3x1*v2 - complex(0,1)*lam5*RA1x2*RA2x1*RA3x1*v2 - complex(0,1)*lam3*RA1x1*RA2x2*RA3x1*v2 - complex(0,1)*lam4*RA1x1*RA2x2*RA3x1*v2 - complex(0,1)*lam5*RA1x1*RA2x2*RA3x1*v2 - complex(0,1)*lam3*RA1x1*RA2x1*RA3x2*v2 - complex(0,1)*lam4*RA1x1*RA2x1*RA3x2*v2 - complex(0,1)*lam5*RA1x1*RA2x1*RA3x2*v2 - 3*complex(0,1)*lam2*RA1x2*RA2x2*RA3x2*v2 - complex(0,1)*lam8*RA1x3*RA2x3*RA3x2*v2 - complex(0,1)*lam8*RA1x3*RA2x2*RA3x3*v2 - complex(0,1)*lam8*RA1x2*RA2x3*RA3x3*v2 - complex(0,1)*lam7*RA1x3*RA2x1*RA3x1*vS - complex(0,1)*lam7*RA1x1*RA2x3*RA3x1*vS - complex(0,1)*lam8*RA1x3*RA2x2*RA3x2*vS - complex(0,1)*lam8*RA1x2*RA2x3*RA3x2*vS - complex(0,1)*lam7*RA1x1*RA2x1*RA3x3*vS - complex(0,1)*lam8*RA1x2*RA2x2*RA3x3*vS - 3*complex(0,1)*lam6*RA1x3*RA2x3*RA3x3*vS',
                  order = {'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '-3*complex(0,1)*lam1*RA2x1**2*RA3x1*v1 - complex(0,1)*lam3*RA2x2**2*RA3x1*v1 - complex(0,1)*lam4*RA2x2**2*RA3x1*v1 - complex(0,1)*lam5*RA2x2**2*RA3x1*v1 - complex(0,1)*lam7*RA2x3**2*RA3x1*v1 - 2*complex(0,1)*lam3*RA2x1*RA2x2*RA3x2*v1 - 2*complex(0,1)*lam4*RA2x1*RA2x2*RA3x2*v1 - 2*complex(0,1)*lam5*RA2x1*RA2x2*RA3x2*v1 - 2*complex(0,1)*lam7*RA2x1*RA2x3*RA3x3*v1 - 2*complex(0,1)*lam3*RA2x1*RA2x2*RA3x1*v2 - 2*complex(0,1)*lam4*RA2x1*RA2x2*RA3x1*v2 - 2*complex(0,1)*lam5*RA2x1*RA2x2*RA3x1*v2 - complex(0,1)*lam3*RA2x1**2*RA3x2*v2 - complex(0,1)*lam4*RA2x1**2*RA3x2*v2 - complex(0,1)*lam5*RA2x1**2*RA3x2*v2 - 3*complex(0,1)*lam2*RA2x2**2*RA3x2*v2 - complex(0,1)*lam8*RA2x3**2*RA3x2*v2 - 2*complex(0,1)*lam8*RA2x2*RA2x3*RA3x3*v2 - 2*complex(0,1)*lam7*RA2x1*RA2x3*RA3x1*vS - 2*complex(0,1)*lam8*RA2x2*RA2x3*RA3x2*vS - complex(0,1)*lam7*RA2x1**2*RA3x3*vS - complex(0,1)*lam8*RA2x2**2*RA3x3*vS - 3*complex(0,1)*lam6*RA2x3**2*RA3x3*vS',
                  order = {'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '-3*complex(0,1)*lam1*RA1x1*RA3x1**2*v1 - 2*complex(0,1)*lam3*RA1x2*RA3x1*RA3x2*v1 - 2*complex(0,1)*lam4*RA1x2*RA3x1*RA3x2*v1 - 2*complex(0,1)*lam5*RA1x2*RA3x1*RA3x2*v1 - complex(0,1)*lam3*RA1x1*RA3x2**2*v1 - complex(0,1)*lam4*RA1x1*RA3x2**2*v1 - complex(0,1)*lam5*RA1x1*RA3x2**2*v1 - 2*complex(0,1)*lam7*RA1x3*RA3x1*RA3x3*v1 - complex(0,1)*lam7*RA1x1*RA3x3**2*v1 - complex(0,1)*lam3*RA1x2*RA3x1**2*v2 - complex(0,1)*lam4*RA1x2*RA3x1**2*v2 - complex(0,1)*lam5*RA1x2*RA3x1**2*v2 - 2*complex(0,1)*lam3*RA1x1*RA3x1*RA3x2*v2 - 2*complex(0,1)*lam4*RA1x1*RA3x1*RA3x2*v2 - 2*complex(0,1)*lam5*RA1x1*RA3x1*RA3x2*v2 - 3*complex(0,1)*lam2*RA1x2*RA3x2**2*v2 - 2*complex(0,1)*lam8*RA1x3*RA3x2*RA3x3*v2 - complex(0,1)*lam8*RA1x2*RA3x3**2*v2 - complex(0,1)*lam7*RA1x3*RA3x1**2*vS - complex(0,1)*lam8*RA1x3*RA3x2**2*vS - 2*complex(0,1)*lam7*RA1x1*RA3x1*RA3x3*vS - 2*complex(0,1)*lam8*RA1x2*RA3x2*RA3x3*vS - 3*complex(0,1)*lam6*RA1x3*RA3x3**2*vS',
                  order = {'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '-3*complex(0,1)*lam1*RA2x1*RA3x1**2*v1 - 2*complex(0,1)*lam3*RA2x2*RA3x1*RA3x2*v1 - 2*complex(0,1)*lam4*RA2x2*RA3x1*RA3x2*v1 - 2*complex(0,1)*lam5*RA2x2*RA3x1*RA3x2*v1 - complex(0,1)*lam3*RA2x1*RA3x2**2*v1 - complex(0,1)*lam4*RA2x1*RA3x2**2*v1 - complex(0,1)*lam5*RA2x1*RA3x2**2*v1 - 2*complex(0,1)*lam7*RA2x3*RA3x1*RA3x3*v1 - complex(0,1)*lam7*RA2x1*RA3x3**2*v1 - complex(0,1)*lam3*RA2x2*RA3x1**2*v2 - complex(0,1)*lam4*RA2x2*RA3x1**2*v2 - complex(0,1)*lam5*RA2x2*RA3x1**2*v2 - 2*complex(0,1)*lam3*RA2x1*RA3x1*RA3x2*v2 - 2*complex(0,1)*lam4*RA2x1*RA3x1*RA3x2*v2 - 2*complex(0,1)*lam5*RA2x1*RA3x1*RA3x2*v2 - 3*complex(0,1)*lam2*RA2x2*RA3x2**2*v2 - 2*complex(0,1)*lam8*RA2x3*RA3x2*RA3x3*v2 - complex(0,1)*lam8*RA2x2*RA3x3**2*v2 - complex(0,1)*lam7*RA2x3*RA3x1**2*vS - complex(0,1)*lam8*RA2x3*RA3x2**2*vS - 2*complex(0,1)*lam7*RA2x1*RA3x1*RA3x3*vS - 2*complex(0,1)*lam8*RA2x2*RA3x2*RA3x3*vS - 3*complex(0,1)*lam6*RA2x3*RA3x3**2*vS',
                  order = {'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '-3*complex(0,1)*lam1*RA3x1**3*v1 - 3*complex(0,1)*lam3*RA3x1*RA3x2**2*v1 - 3*complex(0,1)*lam4*RA3x1*RA3x2**2*v1 - 3*complex(0,1)*lam5*RA3x1*RA3x2**2*v1 - 3*complex(0,1)*lam7*RA3x1*RA3x3**2*v1 - 3*complex(0,1)*lam3*RA3x1**2*RA3x2*v2 - 3*complex(0,1)*lam4*RA3x1**2*RA3x2*v2 - 3*complex(0,1)*lam5*RA3x1**2*RA3x2*v2 - 3*complex(0,1)*lam2*RA3x2**3*v2 - 3*complex(0,1)*lam8*RA3x2*RA3x3**2*v2 - 3*complex(0,1)*lam7*RA3x1**2*RA3x3*vS - 3*complex(0,1)*lam8*RA3x2**2*RA3x3*vS - 3*complex(0,1)*lam6*RA3x3**3*vS',
                  order = {'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '-(CB**2*complex(0,1)*lam4*RA1x2*v1)/2. - (CB**2*complex(0,1)*lam5*RA1x2*v1)/2. + CB*complex(0,1)*lam1*RA1x1*SB*v1 - CB*complex(0,1)*lam3*RA1x1*SB*v1 + (complex(0,1)*lam4*RA1x2*SB**2*v1)/2. + (complex(0,1)*lam5*RA1x2*SB**2*v1)/2. - (CB**2*complex(0,1)*lam4*RA1x1*v2)/2. - (CB**2*complex(0,1)*lam5*RA1x1*v2)/2. - CB*complex(0,1)*lam2*RA1x2*SB*v2 + CB*complex(0,1)*lam3*RA1x2*SB*v2 + (complex(0,1)*lam4*RA1x1*SB**2*v2)/2. + (complex(0,1)*lam5*RA1x1*SB**2*v2)/2. + CB*complex(0,1)*lam7*RA1x3*SB*vS - CB*complex(0,1)*lam8*RA1x3*SB*vS',
                  order = {'QED':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '-(CB**2*complex(0,1)*lam5*RA1x2*v1) + CB*complex(0,1)*lam1*RA1x1*SB*v1 - CB*complex(0,1)*lam3*RA1x1*SB*v1 - CB*complex(0,1)*lam4*RA1x1*SB*v1 + CB*complex(0,1)*lam5*RA1x1*SB*v1 + complex(0,1)*lam5*RA1x2*SB**2*v1 - CB**2*complex(0,1)*lam5*RA1x1*v2 - CB*complex(0,1)*lam2*RA1x2*SB*v2 + CB*complex(0,1)*lam3*RA1x2*SB*v2 + CB*complex(0,1)*lam4*RA1x2*SB*v2 - CB*complex(0,1)*lam5*RA1x2*SB*v2 + complex(0,1)*lam5*RA1x1*SB**2*v2 + CB*complex(0,1)*lam7*RA1x3*SB*vS - CB*complex(0,1)*lam8*RA1x3*SB*vS',
                  order = {'QED':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '-(CB**2*complex(0,1)*lam4*RA2x2*v1)/2. - (CB**2*complex(0,1)*lam5*RA2x2*v1)/2. + CB*complex(0,1)*lam1*RA2x1*SB*v1 - CB*complex(0,1)*lam3*RA2x1*SB*v1 + (complex(0,1)*lam4*RA2x2*SB**2*v1)/2. + (complex(0,1)*lam5*RA2x2*SB**2*v1)/2. - (CB**2*complex(0,1)*lam4*RA2x1*v2)/2. - (CB**2*complex(0,1)*lam5*RA2x1*v2)/2. - CB*complex(0,1)*lam2*RA2x2*SB*v2 + CB*complex(0,1)*lam3*RA2x2*SB*v2 + (complex(0,1)*lam4*RA2x1*SB**2*v2)/2. + (complex(0,1)*lam5*RA2x1*SB**2*v2)/2. + CB*complex(0,1)*lam7*RA2x3*SB*vS - CB*complex(0,1)*lam8*RA2x3*SB*vS',
                  order = {'QED':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '-(CB**2*complex(0,1)*lam5*RA2x2*v1) + CB*complex(0,1)*lam1*RA2x1*SB*v1 - CB*complex(0,1)*lam3*RA2x1*SB*v1 - CB*complex(0,1)*lam4*RA2x1*SB*v1 + CB*complex(0,1)*lam5*RA2x1*SB*v1 + complex(0,1)*lam5*RA2x2*SB**2*v1 - CB**2*complex(0,1)*lam5*RA2x1*v2 - CB*complex(0,1)*lam2*RA2x2*SB*v2 + CB*complex(0,1)*lam3*RA2x2*SB*v2 + CB*complex(0,1)*lam4*RA2x2*SB*v2 - CB*complex(0,1)*lam5*RA2x2*SB*v2 + complex(0,1)*lam5*RA2x1*SB**2*v2 + CB*complex(0,1)*lam7*RA2x3*SB*vS - CB*complex(0,1)*lam8*RA2x3*SB*vS',
                  order = {'QED':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '-(CB**2*complex(0,1)*lam4*RA3x2*v1)/2. - (CB**2*complex(0,1)*lam5*RA3x2*v1)/2. + CB*complex(0,1)*lam1*RA3x1*SB*v1 - CB*complex(0,1)*lam3*RA3x1*SB*v1 + (complex(0,1)*lam4*RA3x2*SB**2*v1)/2. + (complex(0,1)*lam5*RA3x2*SB**2*v1)/2. - (CB**2*complex(0,1)*lam4*RA3x1*v2)/2. - (CB**2*complex(0,1)*lam5*RA3x1*v2)/2. - CB*complex(0,1)*lam2*RA3x2*SB*v2 + CB*complex(0,1)*lam3*RA3x2*SB*v2 + (complex(0,1)*lam4*RA3x1*SB**2*v2)/2. + (complex(0,1)*lam5*RA3x1*SB**2*v2)/2. + CB*complex(0,1)*lam7*RA3x3*SB*vS - CB*complex(0,1)*lam8*RA3x3*SB*vS',
                  order = {'QED':1})

GC_295 = Coupling(name = 'GC_295',
                  value = '-(CB**2*complex(0,1)*lam5*RA3x2*v1) + CB*complex(0,1)*lam1*RA3x1*SB*v1 - CB*complex(0,1)*lam3*RA3x1*SB*v1 - CB*complex(0,1)*lam4*RA3x1*SB*v1 + CB*complex(0,1)*lam5*RA3x1*SB*v1 + complex(0,1)*lam5*RA3x2*SB**2*v1 - CB**2*complex(0,1)*lam5*RA3x1*v2 - CB*complex(0,1)*lam2*RA3x2*SB*v2 + CB*complex(0,1)*lam3*RA3x2*SB*v2 + CB*complex(0,1)*lam4*RA3x2*SB*v2 - CB*complex(0,1)*lam5*RA3x2*SB*v2 + complex(0,1)*lam5*RA3x1*SB**2*v2 + CB*complex(0,1)*lam7*RA3x3*SB*vS - CB*complex(0,1)*lam8*RA3x3*SB*vS',
                  order = {'QED':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1*v1) + CB*complex(0,1)*lam4*RA1x2*SB*v1 + CB*complex(0,1)*lam5*RA1x2*SB*v1 - complex(0,1)*lam1*RA1x1*SB**2*v1 - CB**2*complex(0,1)*lam2*RA1x2*v2 + CB*complex(0,1)*lam4*RA1x1*SB*v2 + CB*complex(0,1)*lam5*RA1x1*SB*v2 - complex(0,1)*lam3*RA1x2*SB**2*v2 - CB**2*complex(0,1)*lam8*RA1x3*vS - complex(0,1)*lam7*RA1x3*SB**2*vS',
                  order = {'QED':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '-(CB**2*complex(0,1)*lam3*RA1x1*v1) - CB**2*complex(0,1)*lam4*RA1x1*v1 + CB**2*complex(0,1)*lam5*RA1x1*v1 + 2*CB*complex(0,1)*lam5*RA1x2*SB*v1 - complex(0,1)*lam1*RA1x1*SB**2*v1 - CB**2*complex(0,1)*lam2*RA1x2*v2 + 2*CB*complex(0,1)*lam5*RA1x1*SB*v2 - complex(0,1)*lam3*RA1x2*SB**2*v2 - complex(0,1)*lam4*RA1x2*SB**2*v2 + complex(0,1)*lam5*RA1x2*SB**2*v2 - CB**2*complex(0,1)*lam8*RA1x3*vS - complex(0,1)*lam7*RA1x3*SB**2*vS',
                  order = {'QED':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1*v1) - CB*complex(0,1)*lam4*RA1x2*SB*v1 - CB*complex(0,1)*lam5*RA1x2*SB*v1 - complex(0,1)*lam3*RA1x1*SB**2*v1 - CB**2*complex(0,1)*lam3*RA1x2*v2 - CB*complex(0,1)*lam4*RA1x1*SB*v2 - CB*complex(0,1)*lam5*RA1x1*SB*v2 - complex(0,1)*lam2*RA1x2*SB**2*v2 - CB**2*complex(0,1)*lam7*RA1x3*vS - complex(0,1)*lam8*RA1x3*SB**2*vS',
                  order = {'QED':1})

GC_299 = Coupling(name = 'GC_299',
                  value = '-(CB**2*complex(0,1)*lam1*RA1x1*v1) - 2*CB*complex(0,1)*lam5*RA1x2*SB*v1 - complex(0,1)*lam3*RA1x1*SB**2*v1 - complex(0,1)*lam4*RA1x1*SB**2*v1 + complex(0,1)*lam5*RA1x1*SB**2*v1 - CB**2*complex(0,1)*lam3*RA1x2*v2 - CB**2*complex(0,1)*lam4*RA1x2*v2 + CB**2*complex(0,1)*lam5*RA1x2*v2 - 2*CB*complex(0,1)*lam5*RA1x1*SB*v2 - complex(0,1)*lam2*RA1x2*SB**2*v2 - CB**2*complex(0,1)*lam7*RA1x3*vS - complex(0,1)*lam8*RA1x3*SB**2*vS',
                  order = {'QED':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '-(CB**2*complex(0,1)*lam3*RA2x1*v1) + CB*complex(0,1)*lam4*RA2x2*SB*v1 + CB*complex(0,1)*lam5*RA2x2*SB*v1 - complex(0,1)*lam1*RA2x1*SB**2*v1 - CB**2*complex(0,1)*lam2*RA2x2*v2 + CB*complex(0,1)*lam4*RA2x1*SB*v2 + CB*complex(0,1)*lam5*RA2x1*SB*v2 - complex(0,1)*lam3*RA2x2*SB**2*v2 - CB**2*complex(0,1)*lam8*RA2x3*vS - complex(0,1)*lam7*RA2x3*SB**2*vS',
                  order = {'QED':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '-(CB**2*complex(0,1)*lam3*RA2x1*v1) - CB**2*complex(0,1)*lam4*RA2x1*v1 + CB**2*complex(0,1)*lam5*RA2x1*v1 + 2*CB*complex(0,1)*lam5*RA2x2*SB*v1 - complex(0,1)*lam1*RA2x1*SB**2*v1 - CB**2*complex(0,1)*lam2*RA2x2*v2 + 2*CB*complex(0,1)*lam5*RA2x1*SB*v2 - complex(0,1)*lam3*RA2x2*SB**2*v2 - complex(0,1)*lam4*RA2x2*SB**2*v2 + complex(0,1)*lam5*RA2x2*SB**2*v2 - CB**2*complex(0,1)*lam8*RA2x3*vS - complex(0,1)*lam7*RA2x3*SB**2*vS',
                  order = {'QED':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '-(CB**2*complex(0,1)*lam1*RA2x1*v1) - CB*complex(0,1)*lam4*RA2x2*SB*v1 - CB*complex(0,1)*lam5*RA2x2*SB*v1 - complex(0,1)*lam3*RA2x1*SB**2*v1 - CB**2*complex(0,1)*lam3*RA2x2*v2 - CB*complex(0,1)*lam4*RA2x1*SB*v2 - CB*complex(0,1)*lam5*RA2x1*SB*v2 - complex(0,1)*lam2*RA2x2*SB**2*v2 - CB**2*complex(0,1)*lam7*RA2x3*vS - complex(0,1)*lam8*RA2x3*SB**2*vS',
                  order = {'QED':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '-(CB**2*complex(0,1)*lam1*RA2x1*v1) - 2*CB*complex(0,1)*lam5*RA2x2*SB*v1 - complex(0,1)*lam3*RA2x1*SB**2*v1 - complex(0,1)*lam4*RA2x1*SB**2*v1 + complex(0,1)*lam5*RA2x1*SB**2*v1 - CB**2*complex(0,1)*lam3*RA2x2*v2 - CB**2*complex(0,1)*lam4*RA2x2*v2 + CB**2*complex(0,1)*lam5*RA2x2*v2 - 2*CB*complex(0,1)*lam5*RA2x1*SB*v2 - complex(0,1)*lam2*RA2x2*SB**2*v2 - CB**2*complex(0,1)*lam7*RA2x3*vS - complex(0,1)*lam8*RA2x3*SB**2*vS',
                  order = {'QED':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '-(CB**2*complex(0,1)*lam3*RA3x1*v1) + CB*complex(0,1)*lam4*RA3x2*SB*v1 + CB*complex(0,1)*lam5*RA3x2*SB*v1 - complex(0,1)*lam1*RA3x1*SB**2*v1 - CB**2*complex(0,1)*lam2*RA3x2*v2 + CB*complex(0,1)*lam4*RA3x1*SB*v2 + CB*complex(0,1)*lam5*RA3x1*SB*v2 - complex(0,1)*lam3*RA3x2*SB**2*v2 - CB**2*complex(0,1)*lam8*RA3x3*vS - complex(0,1)*lam7*RA3x3*SB**2*vS',
                  order = {'QED':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '-(CB**2*complex(0,1)*lam3*RA3x1*v1) - CB**2*complex(0,1)*lam4*RA3x1*v1 + CB**2*complex(0,1)*lam5*RA3x1*v1 + 2*CB*complex(0,1)*lam5*RA3x2*SB*v1 - complex(0,1)*lam1*RA3x1*SB**2*v1 - CB**2*complex(0,1)*lam2*RA3x2*v2 + 2*CB*complex(0,1)*lam5*RA3x1*SB*v2 - complex(0,1)*lam3*RA3x2*SB**2*v2 - complex(0,1)*lam4*RA3x2*SB**2*v2 + complex(0,1)*lam5*RA3x2*SB**2*v2 - CB**2*complex(0,1)*lam8*RA3x3*vS - complex(0,1)*lam7*RA3x3*SB**2*vS',
                  order = {'QED':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '-(CB**2*complex(0,1)*lam1*RA3x1*v1) - CB*complex(0,1)*lam4*RA3x2*SB*v1 - CB*complex(0,1)*lam5*RA3x2*SB*v1 - complex(0,1)*lam3*RA3x1*SB**2*v1 - CB**2*complex(0,1)*lam3*RA3x2*v2 - CB*complex(0,1)*lam4*RA3x1*SB*v2 - CB*complex(0,1)*lam5*RA3x1*SB*v2 - complex(0,1)*lam2*RA3x2*SB**2*v2 - CB**2*complex(0,1)*lam7*RA3x3*vS - complex(0,1)*lam8*RA3x3*SB**2*vS',
                  order = {'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '-(CB**2*complex(0,1)*lam1*RA3x1*v1) - 2*CB*complex(0,1)*lam5*RA3x2*SB*v1 - complex(0,1)*lam3*RA3x1*SB**2*v1 - complex(0,1)*lam4*RA3x1*SB**2*v1 + complex(0,1)*lam5*RA3x1*SB**2*v1 - CB**2*complex(0,1)*lam3*RA3x2*v2 - CB**2*complex(0,1)*lam4*RA3x2*v2 + CB**2*complex(0,1)*lam5*RA3x2*v2 - 2*CB*complex(0,1)*lam5*RA3x1*SB*v2 - complex(0,1)*lam2*RA3x2*SB**2*v2 - CB**2*complex(0,1)*lam7*RA3x3*vS - complex(0,1)*lam8*RA3x3*SB**2*vS',
                  order = {'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '-((CB*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '(CB*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '-((complex(0,1)*RA1x1*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '-((complex(0,1)*RA2x1*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '-((complex(0,1)*RA3x1*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '-((SB*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '(SB*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '-((CB*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '(CB*yc)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '-((complex(0,1)*RA1x2*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '-((complex(0,1)*RA2x2*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '-((complex(0,1)*RA3x2*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '-((SB*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '(SB*yc)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '-((CB*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_323 = Coupling(name = 'GC_323',
                  value = '(CB*ydo)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-((complex(0,1)*RA1x1*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '-((complex(0,1)*RA2x1*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '-((complex(0,1)*RA3x1*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '-((SB*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '(SB*ydo)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '-(CB*complex(0,1)*ye)',
                  order = {'QED':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '-((CB*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '(CB*ye)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '-((complex(0,1)*RA1x1*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '-((complex(0,1)*RA2x1*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '-((complex(0,1)*RA3x1*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = 'complex(0,1)*SB*ye',
                  order = {'QED':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '-((SB*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '(SB*ye)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '-(CB*complex(0,1)*ym)',
                  order = {'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '-((CB*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '(CB*ym)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '-((complex(0,1)*RA1x1*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '-((complex(0,1)*RA2x1*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '-((complex(0,1)*RA3x1*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_344 = Coupling(name = 'GC_344',
                  value = 'complex(0,1)*SB*ym',
                  order = {'QED':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '-((SB*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '(SB*ym)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '-((CB*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(CB*ys)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '-((complex(0,1)*RA1x1*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '-((complex(0,1)*RA2x1*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '-((complex(0,1)*RA3x1*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '-((SB*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(SB*ys)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '-((CB*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(CB*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '-((complex(0,1)*RA1x2*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '-((complex(0,1)*RA2x2*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '-((complex(0,1)*RA3x2*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '-((SB*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(SB*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '-(CB*complex(0,1)*ytau)',
                  order = {'QED':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '-((CB*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(CB*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '-((complex(0,1)*RA1x1*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '-((complex(0,1)*RA2x1*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '-((complex(0,1)*RA3x1*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = 'complex(0,1)*SB*ytau',
                  order = {'QED':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '-((SB*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(SB*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '-((CB*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '(CB*yup)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '-((complex(0,1)*RA1x2*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '-((complex(0,1)*RA2x2*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '-((complex(0,1)*RA3x2*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '-((SB*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_376 = Coupling(name = 'GC_376',
                  value = '(SB*yup)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(SW*cmath.sqrt(2))',
                  order = {'QED':1})

