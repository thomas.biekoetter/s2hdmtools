# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Fri 18 Jun 2021 18:05:56


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



