import numpy as np
import timeit
from tqdm import tqdm
import random
from deap import base
from deap import creator
from deap import tools
from itertools import repeat
try:
    from collections.abc import Sequence
except ImportError:
    from collections import Sequence
import multiprocessing as mp

from s2hdmTools.paramPoint import ParamPoint
from s2hdmTools.constraints.higgsius.higgsBS \
    import HiggsBS
from s2hdmTools.constraints.higgsius.standardModel.higgs import BR


# How many points desired
NUMPOINT = 400

# How many CPUs
CPU = 46

# Setup S2HDM

TYPE = 2

TB = (1.5, 10.0)
mH1 = 96.0
mH2 = 125.09
mXi = (40.0, 80.0)
vS = (40.0, 1000.0)
M = (500.0, 1000.0)
maxDiffM = 200.0 # Max. difference of mA/mH3/mHp from M

hbhs = HiggsBS()


# Setup experimental input

mu_lep_exp = 0.117
mu_lep_unc = 0.05
mu_cms_exp = 0.6
mu_cms_unc = 0.2

br = BR()
brSMbb = br.get_bb(mH1)
brSMyy = br.get_yy(mH1)


# Setup DEAP

NN = 100000 # Initial population size
CXPB = 0.8 # Probability for love
MUTPB = 0.2 # Probability for mutation
EUTPB = 0.1 # Probability for euta

MAXGEN = 30 # Maximum number of generations
MAXDURA = 180 # Maximum time in s for eval. of one gener.
CHISQTHRESH = 110.0 # Chisq threshold for best point

creator.create(
    "FitnessMin",
    base.Fitness,
    weights=(-1.0, ))

creator.create(
    "Individual",
    list,
    fitness=creator.FitnessMin)

toolbox = base.Toolbox()

toolbox.register(
    "attr_float",
    random.uniform, 1e-6, 1e0)

toolbox.register(
    "individual",
    tools.initRepeat,
    creator.Individual,
    toolbox.attr_float,
    11)

toolbox.register(
    "population",
    tools.initRepeat,
    list,
    toolbox.individual)

def mutUniformFloat(individual, low, up, indpb):

    size = len(individual)
    if not isinstance(low, Sequence):
        low = repeat(low, size)
    elif len(low) < size:
        raise IndexError("low must be at least the size of individual: %d < %d" % (len(low), size))
    if not isinstance(up, Sequence):
        up = repeat(up, size)
    elif len(up) < size:
        raise IndexError("up must be at least the size of individual: %d < %d" % (len(up), size))

    for i, xl, xu in zip(range(size), low, up):
        if random.random() < indpb:
            individual[i] = individual[i] * random.uniform(xl, xu)

    return individual,

def eval_point(individual):

    paras = construct_dict(individual)
    pt = ParamPoint(paras)

    if pt.check_boundedness() and pt.check_tree_pert_uni() \
        and pt.check_ewpo() and pt.R[1, 2]**2 < 0.2 and \
            pt.R[2, 2]**2 < 0.1 and (pt.R[1, 1] / pt.sb)**2 > 0.8:
        chisq = run_higgssignals(pt) # / 84.
        if pt.HB['result'] == 0:
            chisq += pt.HB['obsratio'] * 1e2
        chisq += 1e1 * mu_lep(pt)
        chisq += 1e1 * mu_cms(pt)
    else:
        chisq = 1e10

    return chisq,

toolbox.register("evaluate", eval_point)
toolbox.register("select", tools.selTournament, tournsize=3)
toolbox.register("mate", tools.cxUniform, indpb=0.2)
toolbox.register("mutate", mutUniformFloat, low=0.8, up=1.2, indpb=0.1)

def run_higgssignals(pt):
    hbhs.check_point(pt)
    return pt.HS['Chisq']

def mu_lep(pt):
    ch1vv = pt.effCpls['HVV'][0]
    bh1bb = pt.b_H1['bb']
    mu = ch1vv**2 * bh1bb / brSMbb
    chisq = (mu - mu_lep_exp)**2 / mu_lep_unc**2
    return chisq

def mu_cms(pt):
    ch1tt = pt.effCpls['Huu'][0]
    bh1yy = pt.b_H1['yy']
    mu = ch1tt**2 * bh1yy / brSMyy
    chisq = (mu - mu_cms_exp)**2 / mu_cms_unc**2
    return chisq

def inds_to_file(inds):
    paras = [construct_dict(ind) for ind in inds]
    fits = [ind.fitness.values[0] for ind in inds]
    name = 'anomaliesAccommodator_' + \
        str(np.round(min(fits), 3)) + '.csv'
    with open(name, 'w') as f:
        for pr, fit in zip(paras, fits):
            pr['fitness'] = fit
            f.write(str(pr))
            f.write('\n')

def construct_dict(ind):

    xind = [x if x < 1.0 else 1.0 for x in ind]

    tb = TB[0] + (TB[1] - TB[0]) * xind[3]

    be = np.arctan(tb)
    sb = np.sin(be)
    cb = np.cos(be)

    m = M[0] + (M[1] - M[0]) * xind[10]

    al1 = -np.pi / 2.0 + np.pi * xind[0]
    al2 = -np.pi / 2.0 + np.pi * xind[1]
    al3 = -np.pi / 2.0 + np.pi * xind[2]

    mh3 = m - maxDiffM + 2.0 * maxDiffM * xind[4]
    mxi = mXi[0] + (mXi[1] - mXi[0]) * xind[5]
    ma = m - maxDiffM + 2.0 * maxDiffM * xind[6]
    mhp = m - maxDiffM + 2.0 * maxDiffM * xind[7]

    vs = vS[0] + (vS[1] - vS[0]) * xind[8]

    m12sq = m**2 * sb * cb * (0.75 + 0.5 * xind[9])

    paras = {
        'type': TYPE,
        'tb': tb,
        'al1': al1,
        'al2': al2,
        'al3': al3,
        'mH1': mH1,
        'mH2': mH2,
        'mH3': min(mh3, 1e3),
        'mXi': mxi,
        'mA': min(ma, 1e3),
        'mHp': max(550.0, min(mhp, 1e3)),
        'vs': vs,
        'm12sq': m12sq}

    return paras

def gen_point():

        pop = toolbox.population(n=NN)

        fitnesses = list(map(toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit

        generations = 0
        fits = [x[0] for x in fitnesses]
        minfit = min(fits)
        best = None
        dura = 0.0

        while minfit > CHISQTHRESH and (generations < MAXGEN) and (dura < MAXDURA):

            generations += 1


            offspring = toolbox.select(pop, len(pop))
            offspring = list(map(toolbox.clone, offspring))

            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.uniform(0, 1) < CXPB:
                    toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values

            for mutant in offspring:
                if random.uniform(0, 1) < MUTPB:
                    toolbox.mutate(mutant)
                    del mutant.fitness.values

            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            start_time = timeit.default_timer()
            fitnesses = map(toolbox.evaluate, invalid_ind)
            dura = timeit.default_timer() - start_time
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            offfits = []
            for ind in offspring:
                offfits.append(ind.fitness.values[0])
            allmin = min(offfits + fits)

            if len(pop) > NN:
                euta = []
                for i, child in enumerate(offspring):
                    if child.fitness.values[0] > 1e5:
                        if random.uniform(0, 1) < EUTPB:
                            euta.append(i)
                for i in reversed(euta):
                    del offspring[i]

            elites = []
            for ind in offspring + pop:
                fit = ind.fitness.values[0]
                if (fit <= allmin): #  and (fit < 1e4):
                    best = ind
                    elites.append(ind)

            pop[:] = offspring
            for el in elites:
                pop.append(el)

            fits = [ind.fitness.values[0] for ind in pop]
            minfit = min(fits)

        print('Found point has a fitness of: ')
        print('\t' + str(best.fitness.values[0]))

        return best


def main():

    besties = [0] * NUMPOINT

    p = mp.Pool(CPU)

    for i in tqdm(range(0, NUMPOINT)):
        besties[i] = p.apply_async(gen_point)
        # besties[i] = gen_point()

    p.close()
    p.join()
    besties = [x.get() for x in besties]

    inds_to_file(besties)


main()
