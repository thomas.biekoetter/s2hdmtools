# Parameter scans using genetic algorithms

In this folder we collect scripts to sample
the parameter space of the S2HDM utilizing
genetic algorithms. The scripts rely on the
package `DEAP` for the implementation of
the genetic algorithms. The following scripts
are available:

## anomaliesAccommodator.py

This script aims towards the location of
parameter space regions in which the
96GeV excesses can be accommodated in
combination with the indirect DM
anomalies.

The script minimizes a Chi-squared
taking into account only the observables
related to the Higgs phenomenology, whereas
regarding the DM candidate only a suitable
mass range is fixed.
The precise DM properties for the
parameter points that have been obtained
have to be checked afterwards.

The output file `anomaliesAccommodator.csv`
contains the input parameters (angle input
format) for the parameter points with the
lowest Chi-squared values.

To run the script, just do:

```
python anomaliesAccommodator.py
```

To adjust parameter ranges to be sampled,
or to adjust the properties of the genetic
algorithm, modify the script as you wish.
