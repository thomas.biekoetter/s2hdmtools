# s2hdmTools

A python package for the exploration of
the singlet-extended 2 Higgs doublet
model (S2HDM).
The package can be used to make
theoretical predictions
for the collider phenomenology and the
dark matter phenomenology of the model.
Based on these predictions, one can then
verify whether a parameter point is excluded
or not. In addition to the experimental
constraints, also theoretical constraints
can be applied in order to constrain
the parameter space.

The documentation of `s2hdmTools` can be
found [here].

## Authors

`s2hdmTools` is developed by [María Olalla Olea Romacho]
and [Thomas Biekötter].

## Citation guide

If you use this code for a scientific
publication, please cite the following
papers:

* [arXiv:2108.10864]: Thomas Biekoetter, Maria
    Olalla Olea, Reconciling Higgs physics and
    pseudo-Nambu-Goldstone dark matter
    in the S2HDM using a genetic algorithm,
    [J. High Energ. Phys. 2021, 215 (2021)]

* [arXiv:2207.04973]: Thomas Biekötter, María
    Olalla Olea, Pedro Gabriel and Rui Santos,
    Direct detection of pseudo-Nambu-Goldstone dark matter
    in a two Higgs doublet plus singlet extension of the SM,
    [J. High Energ. Phys. 2022, 126 (2022)]

Please also see the citation guides of
the external programs that are interfaced
in `s2hdmTools`.

## Installation

The package and all its dependencies
are installed by executing:

```
make all PC2=/home/.../python2 F2PC2=/home/.../f2py2
```

The external program `MadDM` is
only available for version 2 of python.
To be more precise, `MadDM` requires
`python2` and a Fortran-to-python compiler
`f2py2`. The latter is shipped with `numpy`.
We prefer to leave it to the user
to provide which compiler should be used
for `MadDM`, such that the paths have to
be given as arguments to the Makefile.
This can be done as shown above, or
one can set `PC2` and `F2PC2`
directly in the Makefile.

In order to install `s2hdmTools` without
the MadDM interface it is sufficient to do:

```
make all
```

The installation will take several minutes to
complete. Most time is spent on the download
and installation of the external programs:

* [AnyHdecay]: Calculation of decay widths
* [HiggsBounds]: Test against collider searches
* [HiggsSignals]: Test against Higgs-boson cross-section measurements
* [Hom4PS2]: Solving stationary conditions of the potential
* [MicrOmegas]: Calculating relic abundance of dark matter
* [MadDM]: Test against dark-matter indirect-detection constraints
* [HiggsTools]: Test against collider constraints
* [dds2hdm]: Calculating dark-matter direct-detection cross sections

**Note:** The new code HiggsTools replaces the former codes
HiggsBounds and HiggsSignals, which are no longer maintained.
From here on, one should use the HiggsTools interface in order
to check against collider constraints. The interfaces to
HiggsBounds and HiggsSignals have been deprecated and are kept only
for backwards compatibility and for cross checks.


<!-- Links -->
[María Olalla Olea Romacho]: mailto:maria.olalla.olea.romacho@desy.de
[Thomas Biekötter]: mailto:thomas.biekoetter@desy.de
[AnyHdecay]: https://gitlab.com/jonaswittbrodt/anyhdecay
[HiggsBounds]: https://gitlab.com/higgsbounds/higgsbounds
[HiggsSignals]: https://gitlab.com/higgsbounds/higgssinals
[Hom4PS2]: http://www.math.nsysu.edu.tw/~leetsung/works
[MicrOmegas]: http://lapth.cnrs.fr/micromegas
[MadDM]: https://launchpad.net/maddm
[HiggsTools]: https://gitlab.com/higgsbounds/higgstools
[dds2hdm]: https://gitlab.com/thomas.biekoetter/dds2hdm
[arXiv:2108.10864]: https://arxiv.org/abs/2108.10864
[arXiv:2207.04973]: https://arxiv.org/abs/2207.04973
[here]: https://www.desy.de/~biek/s2hdmtoolsdocu/site
[J. High Energ. Phys. 2021, 215 (2021)]: https://link.springer.com/article/10.1007%2FJHEP10%282021%29215
[J. High Energ. Phys. 2022, 126 (2022)]: https://link.springer.com/article/10.1007/JHEP10(2022)126
