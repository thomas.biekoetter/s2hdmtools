from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name = "s2hdmTools",
    version = "1.0.0",
    description = "A python package for the S2HDM.",
    long_description = long_description,
    packages = ["s2hdmTools"],
    author = ["Olalla Olea", "Thomas Biekotter"],
    author_email = [
        "maria.olalla.olea.romacho@desy.de",
        "thomas.biekoetter@desy.de"],
    url = "https://gitlab.com/thomas.biekoetter/s2hdmtools",
    include_package_data = True,
    package_data = {"s2hdmTools": ["*"]},
    install_requires = ["numpy", "scipy"],
    python_requires = '>=3.6',
)
