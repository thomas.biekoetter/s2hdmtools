The user interface is given in terms of
the `ParamPoint` class. For each parameter
point that one wishes to analyze, an instance
of this class has to be created, giving the
values of the free parameters as input.

## ParamPoint class

The `ParamPoint` class contains the functions
to perform the possible computations, such
as the RGE running, the calculation of the
branching ratios, or to confront the parameter
point with the various theoretical
and experimental constraints. The functions
are documented in detail below.

# ::: s2hdmTools.paramPoint.ParamPoint
    handler: python
    selections:
        members:
            - __init__
            - calculate_branching_ratios
    rendering:
        show_root_heading: true
        heading_level: 4

