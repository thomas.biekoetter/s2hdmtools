The following script shows how to initialize
a parameter point and how to confront it
with some theoretical and experimental
constraints:

```
import numpy as np
from s2hdmTools.paramPoint import ParamPoint


TB = 2.
BE = np.arctan(TB)
paras = {
    'type': 2,
    'tb': TB,
    'al1': BE,
    'al2': 0.01,
    'al3': 1.4,
    'mH1': 125.,
    'mH2': 225.,
    'mH3': 450.,
    'mXi': 265.,
    'mA': 600.,
    'mHp': 650.,
    'vs': 1000.,
    'm12sq': 400.**2 * np.sin(BE) * np.cos(BE)}


def main():
    pt, IsBounded, IsPertUni, IsEWPO, IsGlobal = analize_point(paras)
    print_results(pt, IsBounded, IsPertUni, IsEWPO, IsGlobal)


def analize_point(paras):
    pt = ParamPoint(paras)
    IsBounded = pt.check_boundedness()
    IsPertUni = pt.check_tree_pert_uni()
    IsEWPO = pt.check_ewpo()
    IsGlobal = pt.check_metastability()
    pt.check_collider_constraints()
    pt.eval_darkmatter(mode='micro')
    pt.eval_darkmatter(mode='direct')
    return pt, IsBounded, IsPertUni, IsEWPO, IsGlobal


def print_results(pt, IsBounded, IsPertUni, IsEWPO, IsGlobal):
    print("Scalar potential is bounded from below: " + str(IsBounded))
    print("Perturbative unitarity counstraints are respected: " + str(IsPertUni))
    print("Constraints from EWPO are respected: " + str(IsEWPO))
    print("EW minimum is the global minimum of the potential: " + str(IsGlobal))
    print()
    print("Results from the HiggsBounds analysis: ")
    print(pt.HT['HB'])
    print()
    print("Results from the HiggsSignals analysis: ")
    print(pt.HT['HS'])
    print()
    print("Predicted dark-matter relic abundance: " + str(pt.Micromegas['relic']['Omegahsq']))
    print("Dark-matter proton scattering cross section: " + str(pt.directDetection['sigma_proton']))


main()
```
