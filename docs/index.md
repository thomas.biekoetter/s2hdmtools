# Welcome to s2hdmTools

This is the documentation of the
package `s2hdmTools`, a tool for the
for the phenomenological exploration
of the S2HDM.

## Citation guide

If you use this code for a scientific
publication, please cite the following
papers:

* [arXiv:2108.10864]: Thomas Biekoetter, Maria
    Olalla Olea, Reconciling Higgs physics and
    pseudo-Nambu-Goldstone dark matter
    in the S2HDM using a genetic algorithm,
    [J. High Energ. Phys. 2021, 215 (2021)]

* [arXiv:2207.04973]: Thomas Biekötter, María
    Olalla Olea, Pedro Gabriel and Rui Santos,
    Direct detection of pseudo-Nambu-Goldstone dark matter
    in a two Higgs doublet plus singlet extension of the SM,
    [J. High Energ. Phys. 2022, 126 (2022)]


### External software

`s2hdmTools` makes use of external software
to perform some of the computations. Please
also cite the relevant publications for
the following software if it is relevant
for your work:

* Vacuum stability: [Hom4PS2]
* Collider constraints: [AnyHdecay], [HiggsTools]
* Dark-matter constraints: [MicrOmegas], [MadDM], [dds2hdm]

## Installation

`s2hdmTools` can be installed by
typing the following command
from within the main directory:

```
make all PC2=/home/.../python2 F2PC2=/home/.../f2py2
```

The makefile uses `pip` to install
the package to your python environment.

`MadDM` requires
`python2` and a Fortran-to-python compiler
`f2py2`. The latter is shipped with `numpy`.
We prefer to leave it to the user
to provide which compiler should be used
for `MadDM`, such that the paths have to
be given as arguments to the Makefile.
This can be done as shown above, or
one can set `PC2` and `F2PC2`
directly in the Makefile.

In order to install `s2hdmTools` without
the MadDM interface it is sufficient to do:

```
make all
```

## User instructions

The user interface of the package
is explained [here](https://www.desy.de/~biek/s2hdmtoolsdocu/site/guide/).
You can also find an example script
under [this link](https://www.desy.de/~biek/s2hdmtoolsdocu/site/example/),
which demonstrates how to initialize
a parameter point and how to confront
it with the various constraints.

## Issues

In case you notice any bug or
otherwise undesired behaviour,
please raise an issue in the
[gitlab repository](https://gitlab.com/thomas.biekoetter/s2hdmtools).



[arXiv:2108.10864]: https://arxiv.org/abs/2108.10864
[arXiv:2207.04973]: https://arxiv.org/abs/2207.04973
[AnyHdecay]: https://gitlab.com/jonaswittbrodt/anyhdecay
[HiggsTools]: https://gitlab.com/higgsbounds/higgstools
[Hom4PS2]: http://www.math.nsysu.edu.tw/~leetsung/works
[MicrOmegas]: http://lapth.cnrs.fr/micromegas
[MadDM]: https://launchpad.net/maddm
[dds2hdm]: https://gitlab.com/thomas.biekoetter/dds2hdm
[J. High Energ. Phys. 2021, 215 (2021)]: https://link.springer.com/article/10.1007%2FJHEP10%282021%29215
[J. High Energ. Phys. 2022, 126 (2022)]: https://link.springer.com/article/10.1007/JHEP10(2022)126
